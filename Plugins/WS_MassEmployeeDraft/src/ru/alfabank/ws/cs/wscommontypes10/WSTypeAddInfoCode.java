
package ru.alfabank.ws.cs.wscommontypes10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSTypeAddInfoCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSTypeAddInfoCode">
 *   &lt;complexContent>
 *     &lt;extension base="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeAddInfo">
 *       &lt;sequence>
 *         &lt;element name="adt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSTypeAddInfoCode", propOrder = {
    "adt"
})
public class WSTypeAddInfoCode
    extends WSTypeAddInfo
{

    @XmlElement(required = true)
    protected String adt;

    /**
     * Gets the value of the adt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdt() {
        return adt;
    }

    /**
     * Sets the value of the adt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdt(String value) {
        this.adt = value;
    }

}
