
package ru.alfabank.ws.cs.wscommontypes10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSTypeAddInfoDate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSTypeAddInfoDate">
 *   &lt;complexContent>
 *     &lt;extension base="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeAddInfo">
 *       &lt;sequence>
 *         &lt;element name="adt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSTypeAddInfoDate", propOrder = {
    "adt"
})
public class WSTypeAddInfoDate
    extends WSTypeAddInfo
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar adt;

    /**
     * Gets the value of the adt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAdt() {
        return adt;
    }

    /**
     * Sets the value of the adt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAdt(XMLGregorianCalendar value) {
        this.adt = value;
    }

}
