
package ru.alfabank.ws.cs.wscommontypes10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSTypeAddInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSTypeAddInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSTypeAddInfo", propOrder = {
    "anm"
})
@XmlSeeAlso({
    WSTypeAddInfoAmount.class,
    WSTypeAddInfoAlphanumeric.class,
    WSTypeAddInfoCode.class,
    WSTypeAddInfoNumeric.class,
    WSTypeAddInfoDate.class,
    WSTypeAddInfoExchangeRate.class,
    WSTypeAddInfoInterestRate.class
})
public abstract class WSTypeAddInfo {

    @XmlElement(required = true)
    protected String anm;

    /**
     * Gets the value of the anm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnm() {
        return anm;
    }

    /**
     * Sets the value of the anm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnm(String value) {
        this.anm = value;
    }

}
