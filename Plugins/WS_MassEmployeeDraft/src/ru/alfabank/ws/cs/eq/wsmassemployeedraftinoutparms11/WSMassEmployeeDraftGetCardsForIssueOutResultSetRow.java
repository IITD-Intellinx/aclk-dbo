
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetCardsForIssueOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetCardsForIssueOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="scrd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scrdn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="rqid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="dat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="ser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="num" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fio" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar105" minOccurs="0"/>
 *         &lt;element name="acc1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="err" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar132" minOccurs="0"/>
 *         &lt;element name="desc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetCardsForIssueOutResultSetRow", propOrder = {
    "orid",
    "cus",
    "clc",
    "scrd",
    "scrdn",
    "rqid",
    "dat",
    "ser",
    "num",
    "fio",
    "acc1",
    "err",
    "desc",
    "dfid"
})
public class WSMassEmployeeDraftGetCardsForIssueOutResultSetRow {

    protected String orid;
    protected String cus;
    protected String clc;
    protected String scrd;
    protected String scrdn;
    protected String rqid;
    protected XMLGregorianCalendar dat;
    protected String ser;
    protected String num;
    protected String fio;
    protected String acc1;
    protected String err;
    protected String desc;
    protected String dfid;

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the scrd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd() {
        return scrd;
    }

    /**
     * Sets the value of the scrd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd(String value) {
        this.scrd = value;
    }

    /**
     * Gets the value of the scrdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrdn() {
        return scrdn;
    }

    /**
     * Sets the value of the scrdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrdn(String value) {
        this.scrdn = value;
    }

    /**
     * Gets the value of the rqid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRqid() {
        return rqid;
    }

    /**
     * Sets the value of the rqid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRqid(String value) {
        this.rqid = value;
    }

    /**
     * Gets the value of the dat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDat() {
        return dat;
    }

    /**
     * Sets the value of the dat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDat(XMLGregorianCalendar value) {
        this.dat = value;
    }

    /**
     * Gets the value of the ser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSer() {
        return ser;
    }

    /**
     * Sets the value of the ser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSer(String value) {
        this.ser = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

    /**
     * Gets the value of the fio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFio() {
        return fio;
    }

    /**
     * Sets the value of the fio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFio(String value) {
        this.fio = value;
    }

    /**
     * Gets the value of the acc1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcc1() {
        return acc1;
    }

    /**
     * Sets the value of the acc1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcc1(String value) {
        this.acc1 = value;
    }

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the desc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the value of the desc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

}
