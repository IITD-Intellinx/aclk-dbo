
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftDeleteDraftInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftDeleteDraftInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="tme" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDateTime" minOccurs="0"/>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="pkid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rcus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="rclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fnm1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cnal" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnap" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="inn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar12" minOccurs="0"/>
 *         &lt;element name="tab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dlgn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="work" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="wdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="emb" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wadr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="phor" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="qni" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ddte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="new" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="acus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ocr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="crpr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="crlim" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="brnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="fphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="wphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="mpho" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="mail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wmail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="prp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="ean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ccy" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="min" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="max" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="sfrur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sfusd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sfeur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="comm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1000" minOccurs="0"/>
 *         &lt;element name="usid2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="er370" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar370" minOccurs="0"/>
 *         &lt;element name="brne" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="phid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fxmid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fnam" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftDeleteDraftInParms", propOrder = {
    "dfid",
    "orid",
    "cus",
    "clc",
    "sts",
    "tme",
    "usid",
    "pkid",
    "rcus",
    "rclc",
    "fnm1",
    "fnm2",
    "fnm3",
    "dtbr",
    "mtbr",
    "sex",
    "cnal",
    "cnap",
    "inn",
    "tab",
    "dlgn",
    "work",
    "wdte",
    "emb",
    "wadr",
    "phor",
    "qni",
    "ddte",
    "_new",
    "acus",
    "aclc",
    "ocr",
    "crpr",
    "crlim",
    "brnm",
    "fphn",
    "wphn",
    "mpho",
    "mail",
    "wmail",
    "prp",
    "scon",
    "ean",
    "ccy",
    "min",
    "max",
    "sfrur",
    "sfusd",
    "sfeur",
    "comm",
    "usid2",
    "er370",
    "brne",
    "phid",
    "fxmid",
    "fnam"
})
public class WSMassEmployeeDraftDeleteDraftInParms {

    protected String dfid;
    protected String orid;
    protected String cus;
    protected String clc;
    protected String sts;
    protected XMLGregorianCalendar tme;
    protected String usid;
    protected String pkid;
    protected String rcus;
    protected String rclc;
    protected String fnm1;
    protected String fnm2;
    protected String fnm3;
    protected XMLGregorianCalendar dtbr;
    protected String mtbr;
    protected String sex;
    protected String cnal;
    protected String cnap;
    protected String inn;
    protected String tab;
    protected String dlgn;
    protected String work;
    protected XMLGregorianCalendar wdte;
    protected String emb;
    protected String wadr;
    protected String phor;
    protected String qni;
    protected XMLGregorianCalendar ddte;
    @XmlElement(name = "new")
    protected String _new;
    protected String acus;
    protected String aclc;
    protected String ocr;
    protected String crpr;
    protected BigDecimal crlim;
    protected String brnm;
    protected String fphn;
    protected String wphn;
    protected String mpho;
    protected String mail;
    protected String wmail;
    protected String prp;
    protected String scon;
    protected String ean;
    protected String ccy;
    protected BigDecimal min;
    protected BigDecimal max;
    protected String sfrur;
    protected String sfusd;
    protected String sfeur;
    protected String comm;
    protected String usid2;
    protected String er370;
    protected String brne;
    protected String phid;
    protected String fxmid;
    protected String fnam;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the tme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTme() {
        return tme;
    }

    /**
     * Sets the value of the tme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTme(XMLGregorianCalendar value) {
        this.tme = value;
    }

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the pkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * Sets the value of the pkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkid(String value) {
        this.pkid = value;
    }

    /**
     * Gets the value of the rcus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcus() {
        return rcus;
    }

    /**
     * Sets the value of the rcus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcus(String value) {
        this.rcus = value;
    }

    /**
     * Gets the value of the rclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRclc() {
        return rclc;
    }

    /**
     * Sets the value of the rclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRclc(String value) {
        this.rclc = value;
    }

    /**
     * Gets the value of the fnm1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm1() {
        return fnm1;
    }

    /**
     * Sets the value of the fnm1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm1(String value) {
        this.fnm1 = value;
    }

    /**
     * Gets the value of the fnm2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm2() {
        return fnm2;
    }

    /**
     * Sets the value of the fnm2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm2(String value) {
        this.fnm2 = value;
    }

    /**
     * Gets the value of the fnm3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm3() {
        return fnm3;
    }

    /**
     * Sets the value of the fnm3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm3(String value) {
        this.fnm3 = value;
    }

    /**
     * Gets the value of the dtbr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtbr() {
        return dtbr;
    }

    /**
     * Sets the value of the dtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtbr(XMLGregorianCalendar value) {
        this.dtbr = value;
    }

    /**
     * Gets the value of the mtbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMtbr() {
        return mtbr;
    }

    /**
     * Sets the value of the mtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMtbr(String value) {
        this.mtbr = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Gets the value of the cnal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnal() {
        return cnal;
    }

    /**
     * Sets the value of the cnal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnal(String value) {
        this.cnal = value;
    }

    /**
     * Gets the value of the cnap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnap() {
        return cnap;
    }

    /**
     * Sets the value of the cnap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnap(String value) {
        this.cnap = value;
    }

    /**
     * Gets the value of the inn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInn() {
        return inn;
    }

    /**
     * Sets the value of the inn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInn(String value) {
        this.inn = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTab() {
        return tab;
    }

    /**
     * Sets the value of the tab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTab(String value) {
        this.tab = value;
    }

    /**
     * Gets the value of the dlgn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlgn() {
        return dlgn;
    }

    /**
     * Sets the value of the dlgn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlgn(String value) {
        this.dlgn = value;
    }

    /**
     * Gets the value of the work property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWork() {
        return work;
    }

    /**
     * Sets the value of the work property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWork(String value) {
        this.work = value;
    }

    /**
     * Gets the value of the wdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWdte() {
        return wdte;
    }

    /**
     * Sets the value of the wdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWdte(XMLGregorianCalendar value) {
        this.wdte = value;
    }

    /**
     * Gets the value of the emb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmb() {
        return emb;
    }

    /**
     * Sets the value of the emb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmb(String value) {
        this.emb = value;
    }

    /**
     * Gets the value of the wadr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWadr() {
        return wadr;
    }

    /**
     * Sets the value of the wadr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWadr(String value) {
        this.wadr = value;
    }

    /**
     * Gets the value of the phor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhor() {
        return phor;
    }

    /**
     * Sets the value of the phor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhor(String value) {
        this.phor = value;
    }

    /**
     * Gets the value of the qni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQni() {
        return qni;
    }

    /**
     * Sets the value of the qni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQni(String value) {
        this.qni = value;
    }

    /**
     * Gets the value of the ddte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDdte() {
        return ddte;
    }

    /**
     * Sets the value of the ddte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDdte(XMLGregorianCalendar value) {
        this.ddte = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNew(String value) {
        this._new = value;
    }

    /**
     * Gets the value of the acus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcus() {
        return acus;
    }

    /**
     * Sets the value of the acus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcus(String value) {
        this.acus = value;
    }

    /**
     * Gets the value of the aclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclc() {
        return aclc;
    }

    /**
     * Sets the value of the aclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclc(String value) {
        this.aclc = value;
    }

    /**
     * Gets the value of the ocr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcr() {
        return ocr;
    }

    /**
     * Sets the value of the ocr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcr(String value) {
        this.ocr = value;
    }

    /**
     * Gets the value of the crpr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrpr() {
        return crpr;
    }

    /**
     * Sets the value of the crpr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrpr(String value) {
        this.crpr = value;
    }

    /**
     * Gets the value of the crlim property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCrlim() {
        return crlim;
    }

    /**
     * Sets the value of the crlim property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCrlim(BigDecimal value) {
        this.crlim = value;
    }

    /**
     * Gets the value of the brnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnm() {
        return brnm;
    }

    /**
     * Sets the value of the brnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnm(String value) {
        this.brnm = value;
    }

    /**
     * Gets the value of the fphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFphn() {
        return fphn;
    }

    /**
     * Sets the value of the fphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFphn(String value) {
        this.fphn = value;
    }

    /**
     * Gets the value of the wphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWphn() {
        return wphn;
    }

    /**
     * Sets the value of the wphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWphn(String value) {
        this.wphn = value;
    }

    /**
     * Gets the value of the mpho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpho() {
        return mpho;
    }

    /**
     * Sets the value of the mpho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpho(String value) {
        this.mpho = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the wmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWmail() {
        return wmail;
    }

    /**
     * Sets the value of the wmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWmail(String value) {
        this.wmail = value;
    }

    /**
     * Gets the value of the prp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrp() {
        return prp;
    }

    /**
     * Sets the value of the prp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrp(String value) {
        this.prp = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMin(BigDecimal value) {
        this.min = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMax(BigDecimal value) {
        this.max = value;
    }

    /**
     * Gets the value of the sfrur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfrur() {
        return sfrur;
    }

    /**
     * Sets the value of the sfrur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfrur(String value) {
        this.sfrur = value;
    }

    /**
     * Gets the value of the sfusd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfusd() {
        return sfusd;
    }

    /**
     * Sets the value of the sfusd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfusd(String value) {
        this.sfusd = value;
    }

    /**
     * Gets the value of the sfeur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfeur() {
        return sfeur;
    }

    /**
     * Sets the value of the sfeur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfeur(String value) {
        this.sfeur = value;
    }

    /**
     * Gets the value of the comm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComm() {
        return comm;
    }

    /**
     * Sets the value of the comm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComm(String value) {
        this.comm = value;
    }

    /**
     * Gets the value of the usid2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid2() {
        return usid2;
    }

    /**
     * Sets the value of the usid2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid2(String value) {
        this.usid2 = value;
    }

    /**
     * Gets the value of the er370 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEr370() {
        return er370;
    }

    /**
     * Sets the value of the er370 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEr370(String value) {
        this.er370 = value;
    }

    /**
     * Gets the value of the brne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrne() {
        return brne;
    }

    /**
     * Sets the value of the brne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrne(String value) {
        this.brne = value;
    }

    /**
     * Gets the value of the phid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhid() {
        return phid;
    }

    /**
     * Sets the value of the phid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhid(String value) {
        this.phid = value;
    }

    /**
     * Gets the value of the fxmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFxmid() {
        return fxmid;
    }

    /**
     * Sets the value of the fxmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFxmid(String value) {
        this.fxmid = value;
    }

    /**
     * Gets the value of the fnam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnam() {
        return fnam;
    }

    /**
     * Sets the value of the fnam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnam(String value) {
        this.fnam = value;
    }

}
