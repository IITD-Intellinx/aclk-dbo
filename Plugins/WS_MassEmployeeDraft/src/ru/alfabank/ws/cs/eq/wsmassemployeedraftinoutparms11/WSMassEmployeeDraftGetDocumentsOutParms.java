
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftGetDocumentsOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetDocumentsOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultSet" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetDocumentsOutResultSet"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetDocumentsOutParms", propOrder = {
    "resultSet"
})
public class WSMassEmployeeDraftGetDocumentsOutParms {

    @XmlElement(required = true)
    protected WSMassEmployeeDraftGetDocumentsOutResultSet resultSet;

    /**
     * Gets the value of the resultSet property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftGetDocumentsOutResultSet }
     *     
     */
    public WSMassEmployeeDraftGetDocumentsOutResultSet getResultSet() {
        return resultSet;
    }

    /**
     * Sets the value of the resultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftGetDocumentsOutResultSet }
     *     
     */
    public void setResultSet(WSMassEmployeeDraftGetDocumentsOutResultSet value) {
        this.resultSet = value;
    }

}
