
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftAddOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftAddOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="ean1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ean2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ean3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="err" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1000" minOccurs="0"/>
 *         &lt;element name="odfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftAddOutParms", propOrder = {
    "idfid",
    "ean1",
    "ean2",
    "ean3",
    "err",
    "odfid"
})
public class WSMassEmployeeDraftAddOutParms {

    protected String idfid;
    protected String ean1;
    protected String ean2;
    protected String ean3;
    protected String err;
    protected String odfid;

    /**
     * Gets the value of the idfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfid() {
        return idfid;
    }

    /**
     * Sets the value of the idfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfid(String value) {
        this.idfid = value;
    }

    /**
     * Gets the value of the ean1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan1() {
        return ean1;
    }

    /**
     * Sets the value of the ean1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan1(String value) {
        this.ean1 = value;
    }

    /**
     * Gets the value of the ean2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan2() {
        return ean2;
    }

    /**
     * Sets the value of the ean2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan2(String value) {
        this.ean2 = value;
    }

    /**
     * Gets the value of the ean3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan3() {
        return ean3;
    }

    /**
     * Sets the value of the ean3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan3(String value) {
        this.ean3 = value;
    }

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the odfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdfid() {
        return odfid;
    }

    /**
     * Sets the value of the odfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdfid(String value) {
        this.odfid = value;
    }

}
