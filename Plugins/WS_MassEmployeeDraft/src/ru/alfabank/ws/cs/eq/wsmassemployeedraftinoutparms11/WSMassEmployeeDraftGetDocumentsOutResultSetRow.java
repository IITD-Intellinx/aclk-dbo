
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetDocumentsOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetDocumentsOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dct" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="dctnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="osn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ucd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ucdnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="ser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="num" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="or" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="cod" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar7" minOccurs="0"/>
 *         &lt;element name="opn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="sdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="edte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetDocumentsOutResultSetRow", propOrder = {
    "dfid",
    "dct",
    "dctnm",
    "osn",
    "ucd",
    "ucdnm",
    "ser",
    "num",
    "or",
    "cod",
    "opn",
    "sdte",
    "edte"
})
public class WSMassEmployeeDraftGetDocumentsOutResultSetRow {

    protected String dfid;
    protected String dct;
    protected String dctnm;
    protected String osn;
    protected String ucd;
    protected String ucdnm;
    protected String ser;
    protected String num;
    protected String or;
    protected String cod;
    protected XMLGregorianCalendar opn;
    protected XMLGregorianCalendar sdte;
    protected XMLGregorianCalendar edte;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the dct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDct() {
        return dct;
    }

    /**
     * Sets the value of the dct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDct(String value) {
        this.dct = value;
    }

    /**
     * Gets the value of the dctnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDctnm() {
        return dctnm;
    }

    /**
     * Sets the value of the dctnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDctnm(String value) {
        this.dctnm = value;
    }

    /**
     * Gets the value of the osn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOsn() {
        return osn;
    }

    /**
     * Sets the value of the osn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOsn(String value) {
        this.osn = value;
    }

    /**
     * Gets the value of the ucd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcd() {
        return ucd;
    }

    /**
     * Sets the value of the ucd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcd(String value) {
        this.ucd = value;
    }

    /**
     * Gets the value of the ucdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcdnm() {
        return ucdnm;
    }

    /**
     * Sets the value of the ucdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcdnm(String value) {
        this.ucdnm = value;
    }

    /**
     * Gets the value of the ser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSer() {
        return ser;
    }

    /**
     * Sets the value of the ser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSer(String value) {
        this.ser = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

    /**
     * Gets the value of the or property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOr() {
        return or;
    }

    /**
     * Sets the value of the or property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOr(String value) {
        this.or = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCod() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCod(String value) {
        this.cod = value;
    }

    /**
     * Gets the value of the opn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpn() {
        return opn;
    }

    /**
     * Sets the value of the opn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpn(XMLGregorianCalendar value) {
        this.opn = value;
    }

    /**
     * Gets the value of the sdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSdte() {
        return sdte;
    }

    /**
     * Sets the value of the sdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSdte(XMLGregorianCalendar value) {
        this.sdte = value;
    }

    /**
     * Gets the value of the edte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdte() {
        return edte;
    }

    /**
     * Sets the value of the edte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdte(XMLGregorianCalendar value) {
        this.edte = value;
    }

}
