
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetConvAccParmsOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetConvAccParmsOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="seq" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal20" minOccurs="0"/>
 *         &lt;element name="ccy" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ama" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="rat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal117" minOccurs="0"/>
 *         &lt;element name="ean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="act" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="dle" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="drt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetConvAccParmsOutResultSetRow", propOrder = {
    "seq",
    "ccy",
    "ama",
    "rat",
    "ean",
    "act",
    "dle",
    "drt"
})
public class WSMassEmployeeDraftGetConvAccParmsOutResultSetRow {

    protected BigDecimal seq;
    protected String ccy;
    protected BigDecimal ama;
    protected BigDecimal rat;
    protected String ean;
    protected String act;
    protected XMLGregorianCalendar dle;
    protected XMLGregorianCalendar drt;

    /**
     * Gets the value of the seq property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSeq() {
        return seq;
    }

    /**
     * Sets the value of the seq property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSeq(BigDecimal value) {
        this.seq = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the ama property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAma() {
        return ama;
    }

    /**
     * Sets the value of the ama property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAma(BigDecimal value) {
        this.ama = value;
    }

    /**
     * Gets the value of the rat property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRat() {
        return rat;
    }

    /**
     * Sets the value of the rat property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRat(BigDecimal value) {
        this.rat = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the act property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAct() {
        return act;
    }

    /**
     * Sets the value of the act property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAct(String value) {
        this.act = value;
    }

    /**
     * Gets the value of the dle property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDle() {
        return dle;
    }

    /**
     * Sets the value of the dle property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDle(XMLGregorianCalendar value) {
        this.dle = value;
    }

    /**
     * Gets the value of the drt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDrt() {
        return drt;
    }

    /**
     * Sets the value of the drt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDrt(XMLGregorianCalendar value) {
        this.drt = value;
    }

}
