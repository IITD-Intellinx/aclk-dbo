
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftAddDraftResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftAddDraftResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftAddDraftResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftAddDraftResponse", propOrder = {
    "response"
})
public class WSMassEmployeeDraftAddDraftResponse {

    @XmlElement(required = true)
    protected WSMassEmployeeDraftAddDraftResponseType response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftAddDraftResponseType }
     *     
     */
    public WSMassEmployeeDraftAddDraftResponseType getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftAddDraftResponseType }
     *     
     */
    public void setResponse(WSMassEmployeeDraftAddDraftResponseType value) {
        this.response = value;
    }

}
