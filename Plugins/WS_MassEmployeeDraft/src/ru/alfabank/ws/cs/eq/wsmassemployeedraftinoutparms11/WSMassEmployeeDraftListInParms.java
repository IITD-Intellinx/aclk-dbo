
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftListInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftListInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="emtyp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar15" minOccurs="0"/>
 *         &lt;element name="cdts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="cdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="edtes" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="edtee" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="pkid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="prp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="acus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fio" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar105" minOccurs="0"/>
 *         &lt;element name="dtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="qni" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="new" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="arc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="obt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="dulser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dulnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="orinn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="orinf" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="orname" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="newacc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sgn1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="usidsgn1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="sgn2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="usidsgn2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dsts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="uusid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="udt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="ufid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="bnn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="par1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftListInParms", propOrder = {
    "usid",
    "emtyp",
    "dfid",
    "orid",
    "cus",
    "clc",
    "sts",
    "cdts",
    "cdte",
    "edtes",
    "edtee",
    "pkid",
    "prp",
    "acus",
    "aclc",
    "fio",
    "dtbr",
    "qni",
    "_new",
    "arc",
    "obt",
    "scon",
    "dulser",
    "dulnum",
    "orinn",
    "orinf",
    "orname",
    "newacc",
    "sgn1",
    "usidsgn1",
    "sgn2",
    "usidsgn2",
    "dsts",
    "uusid",
    "udt",
    "ufid",
    "bnn",
    "par1"
})
public class WSMassEmployeeDraftListInParms {

    protected String usid;
    protected String emtyp;
    protected String dfid;
    protected String orid;
    protected String cus;
    protected String clc;
    protected String sts;
    protected XMLGregorianCalendar cdts;
    protected XMLGregorianCalendar cdte;
    protected XMLGregorianCalendar edtes;
    protected XMLGregorianCalendar edtee;
    protected String pkid;
    protected String prp;
    protected String acus;
    protected String aclc;
    protected String fio;
    protected XMLGregorianCalendar dtbr;
    protected String qni;
    @XmlElement(name = "new")
    protected String _new;
    protected String arc;
    protected String obt;
    protected String scon;
    protected String dulser;
    protected String dulnum;
    protected String orinn;
    protected String orinf;
    protected String orname;
    protected String newacc;
    protected String sgn1;
    protected String usidsgn1;
    protected String sgn2;
    protected String usidsgn2;
    protected String dsts;
    protected String uusid;
    protected XMLGregorianCalendar udt;
    protected String ufid;
    protected String bnn;
    protected String par1;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the emtyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmtyp() {
        return emtyp;
    }

    /**
     * Sets the value of the emtyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmtyp(String value) {
        this.emtyp = value;
    }

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the cdts property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCdts() {
        return cdts;
    }

    /**
     * Sets the value of the cdts property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCdts(XMLGregorianCalendar value) {
        this.cdts = value;
    }

    /**
     * Gets the value of the cdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCdte() {
        return cdte;
    }

    /**
     * Sets the value of the cdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCdte(XMLGregorianCalendar value) {
        this.cdte = value;
    }

    /**
     * Gets the value of the edtes property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdtes() {
        return edtes;
    }

    /**
     * Sets the value of the edtes property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdtes(XMLGregorianCalendar value) {
        this.edtes = value;
    }

    /**
     * Gets the value of the edtee property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdtee() {
        return edtee;
    }

    /**
     * Sets the value of the edtee property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdtee(XMLGregorianCalendar value) {
        this.edtee = value;
    }

    /**
     * Gets the value of the pkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * Sets the value of the pkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkid(String value) {
        this.pkid = value;
    }

    /**
     * Gets the value of the prp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrp() {
        return prp;
    }

    /**
     * Sets the value of the prp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrp(String value) {
        this.prp = value;
    }

    /**
     * Gets the value of the acus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcus() {
        return acus;
    }

    /**
     * Sets the value of the acus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcus(String value) {
        this.acus = value;
    }

    /**
     * Gets the value of the aclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclc() {
        return aclc;
    }

    /**
     * Sets the value of the aclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclc(String value) {
        this.aclc = value;
    }

    /**
     * Gets the value of the fio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFio() {
        return fio;
    }

    /**
     * Sets the value of the fio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFio(String value) {
        this.fio = value;
    }

    /**
     * Gets the value of the dtbr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtbr() {
        return dtbr;
    }

    /**
     * Sets the value of the dtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtbr(XMLGregorianCalendar value) {
        this.dtbr = value;
    }

    /**
     * Gets the value of the qni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQni() {
        return qni;
    }

    /**
     * Sets the value of the qni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQni(String value) {
        this.qni = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNew(String value) {
        this._new = value;
    }

    /**
     * Gets the value of the arc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArc() {
        return arc;
    }

    /**
     * Sets the value of the arc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArc(String value) {
        this.arc = value;
    }

    /**
     * Gets the value of the obt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObt() {
        return obt;
    }

    /**
     * Sets the value of the obt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObt(String value) {
        this.obt = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the dulser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulser() {
        return dulser;
    }

    /**
     * Sets the value of the dulser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulser(String value) {
        this.dulser = value;
    }

    /**
     * Gets the value of the dulnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulnum() {
        return dulnum;
    }

    /**
     * Sets the value of the dulnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulnum(String value) {
        this.dulnum = value;
    }

    /**
     * Gets the value of the orinn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrinn() {
        return orinn;
    }

    /**
     * Sets the value of the orinn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrinn(String value) {
        this.orinn = value;
    }

    /**
     * Gets the value of the orinf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrinf() {
        return orinf;
    }

    /**
     * Sets the value of the orinf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrinf(String value) {
        this.orinf = value;
    }

    /**
     * Gets the value of the orname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrname() {
        return orname;
    }

    /**
     * Sets the value of the orname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrname(String value) {
        this.orname = value;
    }

    /**
     * Gets the value of the newacc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewacc() {
        return newacc;
    }

    /**
     * Sets the value of the newacc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewacc(String value) {
        this.newacc = value;
    }

    /**
     * Gets the value of the sgn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgn1() {
        return sgn1;
    }

    /**
     * Sets the value of the sgn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgn1(String value) {
        this.sgn1 = value;
    }

    /**
     * Gets the value of the usidsgn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsidsgn1() {
        return usidsgn1;
    }

    /**
     * Sets the value of the usidsgn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsidsgn1(String value) {
        this.usidsgn1 = value;
    }

    /**
     * Gets the value of the sgn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSgn2() {
        return sgn2;
    }

    /**
     * Sets the value of the sgn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSgn2(String value) {
        this.sgn2 = value;
    }

    /**
     * Gets the value of the usidsgn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsidsgn2() {
        return usidsgn2;
    }

    /**
     * Sets the value of the usidsgn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsidsgn2(String value) {
        this.usidsgn2 = value;
    }

    /**
     * Gets the value of the dsts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDsts() {
        return dsts;
    }

    /**
     * Sets the value of the dsts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDsts(String value) {
        this.dsts = value;
    }

    /**
     * Gets the value of the uusid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUusid() {
        return uusid;
    }

    /**
     * Sets the value of the uusid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUusid(String value) {
        this.uusid = value;
    }

    /**
     * Gets the value of the udt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUdt() {
        return udt;
    }

    /**
     * Sets the value of the udt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUdt(XMLGregorianCalendar value) {
        this.udt = value;
    }

    /**
     * Gets the value of the ufid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfid() {
        return ufid;
    }

    /**
     * Sets the value of the ufid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfid(String value) {
        this.ufid = value;
    }

    /**
     * Gets the value of the bnn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBnn() {
        return bnn;
    }

    /**
     * Sets the value of the bnn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBnn(String value) {
        this.bnn = value;
    }

    /**
     * Gets the value of the par1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPar1() {
        return par1;
    }

    /**
     * Sets the value of the par1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPar1(String value) {
        this.par1 = value;
    }

}
