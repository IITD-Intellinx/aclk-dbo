
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftGetHistoryInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;


/**
 * <p>Java class for WSMassEmployeeDraftGetHistory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetHistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="inCommonParms" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSCommonParms"/>
 *         &lt;element name="inParms" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetHistoryInParms"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetHistory", propOrder = {
    "inCommonParms",
    "inParms"
})
public class WSMassEmployeeDraftGetHistory {

    @XmlElement(required = true)
    protected WSCommonParms inCommonParms;
    @XmlElement(required = true)
    protected WSMassEmployeeDraftGetHistoryInParms inParms;

    /**
     * Gets the value of the inCommonParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSCommonParms }
     *     
     */
    public WSCommonParms getInCommonParms() {
        return inCommonParms;
    }

    /**
     * Sets the value of the inCommonParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCommonParms }
     *     
     */
    public void setInCommonParms(WSCommonParms value) {
        this.inCommonParms = value;
    }

    /**
     * Gets the value of the inParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftGetHistoryInParms }
     *     
     */
    public WSMassEmployeeDraftGetHistoryInParms getInParms() {
        return inParms;
    }

    /**
     * Sets the value of the inParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftGetHistoryInParms }
     *     
     */
    public void setInParms(WSMassEmployeeDraftGetHistoryInParms value) {
        this.inParms = value;
    }

}
