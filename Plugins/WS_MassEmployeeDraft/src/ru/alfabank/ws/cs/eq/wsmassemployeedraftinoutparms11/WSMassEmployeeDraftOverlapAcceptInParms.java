
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftOverlapAcceptInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftOverlapAcceptInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rno" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal30" minOccurs="0"/>
 *         &lt;element name="acpt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ccy" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="acc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="res" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="qni" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="rcus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="rclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ccy1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ean1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ccy2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ean2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="wmsg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar300" minOccurs="0"/>
 *         &lt;element name="comm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1000" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftOverlapAcceptInParms", propOrder = {
    "usid",
    "dfid",
    "rno",
    "acpt",
    "ccy",
    "acc",
    "res",
    "sts",
    "qni",
    "cus",
    "clc",
    "rcus",
    "rclc",
    "ean",
    "ccy1",
    "ean1",
    "ccy2",
    "ean2",
    "wmsg",
    "comm"
})
public class WSMassEmployeeDraftOverlapAcceptInParms {

    protected String usid;
    protected String dfid;
    protected BigDecimal rno;
    protected String acpt;
    protected String ccy;
    protected String acc;
    protected String res;
    protected String sts;
    protected String qni;
    protected String cus;
    protected String clc;
    protected String rcus;
    protected String rclc;
    protected String ean;
    protected String ccy1;
    protected String ean1;
    protected String ccy2;
    protected String ean2;
    protected String wmsg;
    protected String comm;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the rno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRno() {
        return rno;
    }

    /**
     * Sets the value of the rno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRno(BigDecimal value) {
        this.rno = value;
    }

    /**
     * Gets the value of the acpt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcpt() {
        return acpt;
    }

    /**
     * Sets the value of the acpt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcpt(String value) {
        this.acpt = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the acc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcc() {
        return acc;
    }

    /**
     * Sets the value of the acc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcc(String value) {
        this.acc = value;
    }

    /**
     * Gets the value of the res property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRes() {
        return res;
    }

    /**
     * Sets the value of the res property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRes(String value) {
        this.res = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the qni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQni() {
        return qni;
    }

    /**
     * Sets the value of the qni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQni(String value) {
        this.qni = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the rcus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcus() {
        return rcus;
    }

    /**
     * Sets the value of the rcus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcus(String value) {
        this.rcus = value;
    }

    /**
     * Gets the value of the rclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRclc() {
        return rclc;
    }

    /**
     * Sets the value of the rclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRclc(String value) {
        this.rclc = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the ccy1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * Sets the value of the ccy1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy1(String value) {
        this.ccy1 = value;
    }

    /**
     * Gets the value of the ean1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan1() {
        return ean1;
    }

    /**
     * Sets the value of the ean1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan1(String value) {
        this.ean1 = value;
    }

    /**
     * Gets the value of the ccy2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * Sets the value of the ccy2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy2(String value) {
        this.ccy2 = value;
    }

    /**
     * Gets the value of the ean2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan2() {
        return ean2;
    }

    /**
     * Sets the value of the ean2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan2(String value) {
        this.ean2 = value;
    }

    /**
     * Gets the value of the wmsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWmsg() {
        return wmsg;
    }

    /**
     * Sets the value of the wmsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWmsg(String value) {
        this.wmsg = value;
    }

    /**
     * Gets the value of the comm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComm() {
        return comm;
    }

    /**
     * Sets the value of the comm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComm(String value) {
        this.comm = value;
    }

}
