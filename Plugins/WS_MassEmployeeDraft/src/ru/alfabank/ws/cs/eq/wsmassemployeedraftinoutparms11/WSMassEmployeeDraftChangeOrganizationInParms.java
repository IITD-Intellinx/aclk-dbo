
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftChangeOrganizationInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftChangeOrganizationInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orido" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="oridn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="dte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="uid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftChangeOrganizationInParms", propOrder = {
    "orido",
    "cus",
    "clc",
    "oridn",
    "dte",
    "uid"
})
public class WSMassEmployeeDraftChangeOrganizationInParms {

    protected String orido;
    protected String cus;
    protected String clc;
    protected String oridn;
    protected XMLGregorianCalendar dte;
    protected String uid;

    /**
     * Gets the value of the orido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrido() {
        return orido;
    }

    /**
     * Sets the value of the orido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrido(String value) {
        this.orido = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the oridn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOridn() {
        return oridn;
    }

    /**
     * Sets the value of the oridn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOridn(String value) {
        this.oridn = value;
    }

    /**
     * Gets the value of the dte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDte() {
        return dte;
    }

    /**
     * Sets the value of the dte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDte(XMLGregorianCalendar value) {
        this.dte = value;
    }

    /**
     * Gets the value of the uid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUid() {
        return uid;
    }

    /**
     * Sets the value of the uid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUid(String value) {
        this.uid = value;
    }

}
