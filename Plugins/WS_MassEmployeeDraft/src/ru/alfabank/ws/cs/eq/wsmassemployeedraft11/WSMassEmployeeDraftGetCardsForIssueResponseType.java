
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftGetCardsForIssueOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;


/**
 * <p>Java class for WSMassEmployeeDraftGetCardsForIssueResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetCardsForIssueResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outCommonParms" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSCommonOutParms"/>
 *         &lt;element name="outParms" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetCardsForIssueOutParms"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetCardsForIssueResponseType", propOrder = {
    "outCommonParms",
    "outParms"
})
public class WSMassEmployeeDraftGetCardsForIssueResponseType {

    @XmlElement(required = true)
    protected WSCommonOutParms outCommonParms;
    @XmlElement(required = true)
    protected WSMassEmployeeDraftGetCardsForIssueOutParms outParms;

    /**
     * Gets the value of the outCommonParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSCommonOutParms }
     *     
     */
    public WSCommonOutParms getOutCommonParms() {
        return outCommonParms;
    }

    /**
     * Sets the value of the outCommonParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCommonOutParms }
     *     
     */
    public void setOutCommonParms(WSCommonOutParms value) {
        this.outCommonParms = value;
    }

    /**
     * Gets the value of the outParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftGetCardsForIssueOutParms }
     *     
     */
    public WSMassEmployeeDraftGetCardsForIssueOutParms getOutParms() {
        return outParms;
    }

    /**
     * Sets the value of the outParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftGetCardsForIssueOutParms }
     *     
     */
    public void setOutParms(WSMassEmployeeDraftGetCardsForIssueOutParms value) {
        this.outParms = value;
    }

}
