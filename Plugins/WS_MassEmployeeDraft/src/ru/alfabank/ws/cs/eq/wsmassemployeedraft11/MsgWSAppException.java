
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSAppException;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebFault(name = "WSAppException", targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru")
public class MsgWSAppException
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private WSAppException faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public MsgWSAppException(String message, WSAppException faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public MsgWSAppException(String message, WSAppException faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: ru.alfabank.ws.cs.wscommontypes10.WSAppException
     */
    public WSAppException getFaultInfo() {
        return faultInfo;
    }

}
