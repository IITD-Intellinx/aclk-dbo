
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetCardParmsOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetCardParmsOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scrd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scrdn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sconn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="brnd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="brndn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="sms" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="fict" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="inbo" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="onum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="odte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="pnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="pdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="rqid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="dat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="err" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar132" minOccurs="0"/>
 *         &lt;element name="crd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar24" minOccurs="0"/>
 *         &lt;element name="od" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="desc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="aifp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="mifare" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2048" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetCardParmsOutResultSetRow", propOrder = {
    "scrd",
    "scrdn",
    "scon",
    "sconn",
    "brnd",
    "brndn",
    "sms",
    "fict",
    "inbo",
    "onum",
    "odte",
    "pnum",
    "pdte",
    "rqid",
    "dat",
    "err",
    "crd",
    "od",
    "desc",
    "aifp",
    "mifare"
})
public class WSMassEmployeeDraftGetCardParmsOutResultSetRow {

    protected String scrd;
    protected String scrdn;
    protected String scon;
    protected String sconn;
    protected String brnd;
    protected String brndn;
    protected String sms;
    protected String fict;
    protected String inbo;
    protected String onum;
    protected XMLGregorianCalendar odte;
    protected String pnum;
    protected XMLGregorianCalendar pdte;
    protected String rqid;
    protected XMLGregorianCalendar dat;
    protected String err;
    protected String crd;
    protected XMLGregorianCalendar od;
    protected String desc;
    protected String aifp;
    protected String mifare;

    /**
     * Gets the value of the scrd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd() {
        return scrd;
    }

    /**
     * Sets the value of the scrd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd(String value) {
        this.scrd = value;
    }

    /**
     * Gets the value of the scrdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrdn() {
        return scrdn;
    }

    /**
     * Sets the value of the scrdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrdn(String value) {
        this.scrdn = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the sconn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSconn() {
        return sconn;
    }

    /**
     * Sets the value of the sconn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSconn(String value) {
        this.sconn = value;
    }

    /**
     * Gets the value of the brnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnd() {
        return brnd;
    }

    /**
     * Sets the value of the brnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnd(String value) {
        this.brnd = value;
    }

    /**
     * Gets the value of the brndn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrndn() {
        return brndn;
    }

    /**
     * Sets the value of the brndn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrndn(String value) {
        this.brndn = value;
    }

    /**
     * Gets the value of the sms property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSms() {
        return sms;
    }

    /**
     * Sets the value of the sms property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSms(String value) {
        this.sms = value;
    }

    /**
     * Gets the value of the fict property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFict() {
        return fict;
    }

    /**
     * Sets the value of the fict property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFict(String value) {
        this.fict = value;
    }

    /**
     * Gets the value of the inbo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInbo() {
        return inbo;
    }

    /**
     * Sets the value of the inbo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInbo(String value) {
        this.inbo = value;
    }

    /**
     * Gets the value of the onum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnum() {
        return onum;
    }

    /**
     * Sets the value of the onum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnum(String value) {
        this.onum = value;
    }

    /**
     * Gets the value of the odte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOdte() {
        return odte;
    }

    /**
     * Sets the value of the odte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOdte(XMLGregorianCalendar value) {
        this.odte = value;
    }

    /**
     * Gets the value of the pnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnum() {
        return pnum;
    }

    /**
     * Sets the value of the pnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnum(String value) {
        this.pnum = value;
    }

    /**
     * Gets the value of the pdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPdte() {
        return pdte;
    }

    /**
     * Sets the value of the pdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPdte(XMLGregorianCalendar value) {
        this.pdte = value;
    }

    /**
     * Gets the value of the rqid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRqid() {
        return rqid;
    }

    /**
     * Sets the value of the rqid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRqid(String value) {
        this.rqid = value;
    }

    /**
     * Gets the value of the dat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDat() {
        return dat;
    }

    /**
     * Sets the value of the dat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDat(XMLGregorianCalendar value) {
        this.dat = value;
    }

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the crd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrd() {
        return crd;
    }

    /**
     * Sets the value of the crd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrd(String value) {
        this.crd = value;
    }

    /**
     * Gets the value of the od property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOd() {
        return od;
    }

    /**
     * Sets the value of the od property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOd(XMLGregorianCalendar value) {
        this.od = value;
    }

    /**
     * Gets the value of the desc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Sets the value of the desc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesc(String value) {
        this.desc = value;
    }

    /**
     * Gets the value of the aifp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAifp() {
        return aifp;
    }

    /**
     * Sets the value of the aifp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAifp(String value) {
        this.aifp = value;
    }

    /**
     * Gets the value of the mifare property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMifare() {
        return mifare;
    }

    /**
     * Sets the value of the mifare property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMifare(String value) {
        this.mifare = value;
    }

}
