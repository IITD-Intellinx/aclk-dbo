
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftMaintainEmployeeInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftMaintainEmployeeInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="tab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="work" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="wadr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="chg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="scrd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="brnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="ab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="an" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="as" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="f17" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="min" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="max" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="cdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="acus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="crpr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="crlim" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="crdoc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="crdt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="regdt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="obt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="arc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sfrur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sfusd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sfeur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="aab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="aan" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aas" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="af17" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="aean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="drt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="brne" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="phid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fxmid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ufid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftMaintainEmployeeInParms", propOrder = {
    "cus",
    "clc",
    "orid",
    "tab",
    "work",
    "wadr",
    "chg",
    "scrd",
    "scon",
    "brnm",
    "ab",
    "an",
    "as",
    "f17",
    "ean",
    "min",
    "max",
    "cdte",
    "acus",
    "aclc",
    "sts",
    "crpr",
    "crlim",
    "crdoc",
    "crdt",
    "regdt",
    "obt",
    "arc",
    "sfrur",
    "sfusd",
    "sfeur",
    "aab",
    "aan",
    "aas",
    "af17",
    "aean",
    "drt",
    "usid",
    "brne",
    "phid",
    "fxmid",
    "ufid"
})
public class WSMassEmployeeDraftMaintainEmployeeInParms {

    protected String cus;
    protected String clc;
    protected String orid;
    protected String tab;
    protected String work;
    protected String wadr;
    protected String chg;
    protected String scrd;
    protected String scon;
    protected String brnm;
    protected String ab;
    protected String an;
    protected String as;
    protected String f17;
    protected String ean;
    protected BigDecimal min;
    protected BigDecimal max;
    protected XMLGregorianCalendar cdte;
    protected String acus;
    protected String aclc;
    protected String sts;
    protected String crpr;
    protected BigDecimal crlim;
    protected String crdoc;
    protected XMLGregorianCalendar crdt;
    protected XMLGregorianCalendar regdt;
    protected String obt;
    protected String arc;
    protected String sfrur;
    protected String sfusd;
    protected String sfeur;
    protected String aab;
    protected String aan;
    protected String aas;
    protected String af17;
    protected String aean;
    protected XMLGregorianCalendar drt;
    protected String usid;
    protected String brne;
    protected String phid;
    protected String fxmid;
    protected String ufid;

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTab() {
        return tab;
    }

    /**
     * Sets the value of the tab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTab(String value) {
        this.tab = value;
    }

    /**
     * Gets the value of the work property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWork() {
        return work;
    }

    /**
     * Sets the value of the work property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWork(String value) {
        this.work = value;
    }

    /**
     * Gets the value of the wadr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWadr() {
        return wadr;
    }

    /**
     * Sets the value of the wadr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWadr(String value) {
        this.wadr = value;
    }

    /**
     * Gets the value of the chg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChg() {
        return chg;
    }

    /**
     * Sets the value of the chg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChg(String value) {
        this.chg = value;
    }

    /**
     * Gets the value of the scrd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd() {
        return scrd;
    }

    /**
     * Sets the value of the scrd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd(String value) {
        this.scrd = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the brnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnm() {
        return brnm;
    }

    /**
     * Sets the value of the brnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnm(String value) {
        this.brnm = value;
    }

    /**
     * Gets the value of the ab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAb() {
        return ab;
    }

    /**
     * Sets the value of the ab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAb(String value) {
        this.ab = value;
    }

    /**
     * Gets the value of the an property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAn() {
        return an;
    }

    /**
     * Sets the value of the an property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAn(String value) {
        this.an = value;
    }

    /**
     * Gets the value of the as property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAs() {
        return as;
    }

    /**
     * Sets the value of the as property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAs(String value) {
        this.as = value;
    }

    /**
     * Gets the value of the f17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getF17() {
        return f17;
    }

    /**
     * Sets the value of the f17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setF17(String value) {
        this.f17 = value;
    }

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMin(BigDecimal value) {
        this.min = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMax(BigDecimal value) {
        this.max = value;
    }

    /**
     * Gets the value of the cdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCdte() {
        return cdte;
    }

    /**
     * Sets the value of the cdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCdte(XMLGregorianCalendar value) {
        this.cdte = value;
    }

    /**
     * Gets the value of the acus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcus() {
        return acus;
    }

    /**
     * Sets the value of the acus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcus(String value) {
        this.acus = value;
    }

    /**
     * Gets the value of the aclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclc() {
        return aclc;
    }

    /**
     * Sets the value of the aclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclc(String value) {
        this.aclc = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the crpr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrpr() {
        return crpr;
    }

    /**
     * Sets the value of the crpr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrpr(String value) {
        this.crpr = value;
    }

    /**
     * Gets the value of the crlim property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCrlim() {
        return crlim;
    }

    /**
     * Sets the value of the crlim property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCrlim(BigDecimal value) {
        this.crlim = value;
    }

    /**
     * Gets the value of the crdoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrdoc() {
        return crdoc;
    }

    /**
     * Sets the value of the crdoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrdoc(String value) {
        this.crdoc = value;
    }

    /**
     * Gets the value of the crdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCrdt() {
        return crdt;
    }

    /**
     * Sets the value of the crdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCrdt(XMLGregorianCalendar value) {
        this.crdt = value;
    }

    /**
     * Gets the value of the regdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegdt() {
        return regdt;
    }

    /**
     * Sets the value of the regdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegdt(XMLGregorianCalendar value) {
        this.regdt = value;
    }

    /**
     * Gets the value of the obt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObt() {
        return obt;
    }

    /**
     * Sets the value of the obt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObt(String value) {
        this.obt = value;
    }

    /**
     * Gets the value of the arc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArc() {
        return arc;
    }

    /**
     * Sets the value of the arc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArc(String value) {
        this.arc = value;
    }

    /**
     * Gets the value of the sfrur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfrur() {
        return sfrur;
    }

    /**
     * Sets the value of the sfrur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfrur(String value) {
        this.sfrur = value;
    }

    /**
     * Gets the value of the sfusd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfusd() {
        return sfusd;
    }

    /**
     * Sets the value of the sfusd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfusd(String value) {
        this.sfusd = value;
    }

    /**
     * Gets the value of the sfeur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSfeur() {
        return sfeur;
    }

    /**
     * Sets the value of the sfeur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSfeur(String value) {
        this.sfeur = value;
    }

    /**
     * Gets the value of the aab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAab() {
        return aab;
    }

    /**
     * Sets the value of the aab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAab(String value) {
        this.aab = value;
    }

    /**
     * Gets the value of the aan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAan() {
        return aan;
    }

    /**
     * Sets the value of the aan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAan(String value) {
        this.aan = value;
    }

    /**
     * Gets the value of the aas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAas() {
        return aas;
    }

    /**
     * Sets the value of the aas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAas(String value) {
        this.aas = value;
    }

    /**
     * Gets the value of the af17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAf17() {
        return af17;
    }

    /**
     * Sets the value of the af17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAf17(String value) {
        this.af17 = value;
    }

    /**
     * Gets the value of the aean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAean() {
        return aean;
    }

    /**
     * Sets the value of the aean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAean(String value) {
        this.aean = value;
    }

    /**
     * Gets the value of the drt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDrt() {
        return drt;
    }

    /**
     * Sets the value of the drt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDrt(XMLGregorianCalendar value) {
        this.drt = value;
    }

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the brne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrne() {
        return brne;
    }

    /**
     * Sets the value of the brne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrne(String value) {
        this.brne = value;
    }

    /**
     * Gets the value of the phid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhid() {
        return phid;
    }

    /**
     * Sets the value of the phid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhid(String value) {
        this.phid = value;
    }

    /**
     * Gets the value of the fxmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFxmid() {
        return fxmid;
    }

    /**
     * Sets the value of the fxmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFxmid(String value) {
        this.fxmid = value;
    }

    /**
     * Gets the value of the ufid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfid() {
        return ufid;
    }

    /**
     * Sets the value of the ufid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfid(String value) {
        this.ufid = value;
    }

}
