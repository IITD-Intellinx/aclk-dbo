
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftGetCardParmsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetCardParmsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetCardParmsResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetCardParmsResponse", propOrder = {
    "response"
})
public class WSMassEmployeeDraftGetCardParmsResponse {

    @XmlElement(required = true)
    protected WSMassEmployeeDraftGetCardParmsResponseType response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftGetCardParmsResponseType }
     *     
     */
    public WSMassEmployeeDraftGetCardParmsResponseType getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftGetCardParmsResponseType }
     *     
     */
    public void setResponse(WSMassEmployeeDraftGetCardParmsResponseType value) {
        this.response = value;
    }

}
