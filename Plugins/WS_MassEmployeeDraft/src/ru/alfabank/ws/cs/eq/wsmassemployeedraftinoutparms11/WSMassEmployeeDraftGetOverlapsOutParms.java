
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftGetOverlapsOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetOverlapsOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultSet" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetOverlapsOutResultSet"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetOverlapsOutParms", propOrder = {
    "resultSet"
})
public class WSMassEmployeeDraftGetOverlapsOutParms {

    @XmlElement(required = true)
    protected WSMassEmployeeDraftGetOverlapsOutResultSet resultSet;

    /**
     * Gets the value of the resultSet property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftGetOverlapsOutResultSet }
     *     
     */
    public WSMassEmployeeDraftGetOverlapsOutResultSet getResultSet() {
        return resultSet;
    }

    /**
     * Sets the value of the resultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftGetOverlapsOutResultSet }
     *     
     */
    public void setResultSet(WSMassEmployeeDraftGetOverlapsOutResultSet value) {
        this.resultSet = value;
    }

}
