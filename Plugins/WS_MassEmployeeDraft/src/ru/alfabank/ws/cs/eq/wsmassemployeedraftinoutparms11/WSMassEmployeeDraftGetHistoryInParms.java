
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetHistoryInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetHistoryInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sdat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="edat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="chusid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="bsdat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="bedat" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetHistoryInParms", propOrder = {
    "usid",
    "dfid",
    "sts",
    "sdat",
    "edat",
    "chusid",
    "orid",
    "cus",
    "clc",
    "bsdat",
    "bedat"
})
public class WSMassEmployeeDraftGetHistoryInParms {

    protected String usid;
    protected String dfid;
    protected String sts;
    protected XMLGregorianCalendar sdat;
    protected XMLGregorianCalendar edat;
    protected String chusid;
    protected String orid;
    protected String cus;
    protected String clc;
    protected XMLGregorianCalendar bsdat;
    protected XMLGregorianCalendar bedat;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the sdat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSdat() {
        return sdat;
    }

    /**
     * Sets the value of the sdat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSdat(XMLGregorianCalendar value) {
        this.sdat = value;
    }

    /**
     * Gets the value of the edat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdat() {
        return edat;
    }

    /**
     * Sets the value of the edat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdat(XMLGregorianCalendar value) {
        this.edat = value;
    }

    /**
     * Gets the value of the chusid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChusid() {
        return chusid;
    }

    /**
     * Sets the value of the chusid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChusid(String value) {
        this.chusid = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the bsdat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBsdat() {
        return bsdat;
    }

    /**
     * Sets the value of the bsdat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBsdat(XMLGregorianCalendar value) {
        this.bsdat = value;
    }

    /**
     * Gets the value of the bedat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBedat() {
        return bedat;
    }

    /**
     * Sets the value of the bedat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBedat(XMLGregorianCalendar value) {
        this.bedat = value;
    }

}
