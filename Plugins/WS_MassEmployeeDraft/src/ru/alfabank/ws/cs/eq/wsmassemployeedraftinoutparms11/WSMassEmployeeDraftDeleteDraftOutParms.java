
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftDeleteDraftOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftDeleteDraftOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="er370" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar370" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftDeleteDraftOutParms", propOrder = {
    "dfid",
    "er370"
})
public class WSMassEmployeeDraftDeleteDraftOutParms {

    protected String dfid;
    protected String er370;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the er370 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEr370() {
        return er370;
    }

    /**
     * Sets the value of the er370 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEr370(String value) {
        this.er370 = value;
    }

}
