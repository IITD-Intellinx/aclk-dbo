
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alfabank.ws.cs.eq.wsmassemployeedraft11 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSMassEmployeeDraftAddDraft_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftAddDraft");
    private final static QName _WSMassEmployeeDraftOverlapAccept_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftOverlapAccept");
    private final static QName _WSMassEmployeeDraftGetOverlaps_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetOverlaps");
    private final static QName _WSMassEmployeeDraftGetCardsForIssue_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetCardsForIssue");
    private final static QName _WSMassEmployeeDraftListResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftListResponse");
    private final static QName _WSMassEmployeeDraftGetConvAccParms_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetConvAccParms");
    private final static QName _WSMassEmployeeDraftGetHistoryResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetHistoryResponse");
    private final static QName _WSMassEmployeeDraftGetHistory_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetHistory");
    private final static QName _WSMassEmployeeDraftGetAddresses_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetAddresses");
    private final static QName _WSMassEmployeeDraftGetDocumentsResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetDocumentsResponse");
    private final static QName _WSMassEmployeeDraftMaintainEmployeeResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftMaintainEmployeeResponse");
    private final static QName _WSMassEmployeeDraftList_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftList");
    private final static QName _WSMassEmployeeDraftMaintainDraft_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftMaintainDraft");
    private final static QName _WSMassEmployeeDraftDeleteDismiss_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftDeleteDismiss");
    private final static QName _WSMassEmployeeDraftAddDraftResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftAddDraftResponse");
    private final static QName _WSMassEmployeeDraftMaintainEmployee_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftMaintainEmployee");
    private final static QName _WSMassEmployeeDraftCardOrderIssue_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftCardOrderIssue");
    private final static QName _WSMassEmployeeDraftChangeOrganizationResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftChangeOrganizationResponse");
    private final static QName _WSMassEmployeeDraftChangeOrganization_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftChangeOrganization");
    private final static QName _WSMassEmployeeDraftDeleteDraft_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftDeleteDraft");
    private final static QName _WSMassEmployeeDraftGetOverlapsResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetOverlapsResponse");
    private final static QName _WSMassEmployeeDraftDeleteResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftDeleteResponse");
    private final static QName _WSMassEmployeeDraftMaintainDraftResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftMaintainDraftResponse");
    private final static QName _WSMassEmployeeDraftGetAddressesResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetAddressesResponse");
    private final static QName _WSMassEmployeeDraftMaintainDismiss_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftMaintainDismiss");
    private final static QName _WSMassEmployeeDraftGetConvAccParmsResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetConvAccParmsResponse");
    private final static QName _WSMassEmployeeDraftResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftResponse");
    private final static QName _WSMassEmployeeDraftOverlapAcceptResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftOverlapAcceptResponse");
    private final static QName _WSMassEmployeeDraftGetCardParms_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetCardParms");
    private final static QName _WSMassEmployeeDraftDeleteDraftResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftDeleteDraftResponse");
    private final static QName _WSMassEmployeeDraftDelete_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftDelete");
    private final static QName _WSMassEmployeeDraftGetCardParmsResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetCardParmsResponse");
    private final static QName _WSMassEmployeeDraftGetDocuments_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetDocuments");
    private final static QName _WSMassEmployeeDraftGetCardsForIssueResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftGetCardsForIssueResponse");
    private final static QName _WSMassEmployeeDraftAdd_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftAdd");
    private final static QName _WSMassEmployeeDraftAddResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftAddResponse");
    private final static QName _WSMassEmployeeDraftAddDismiss_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftAddDismiss");
    private final static QName _WSMassEmployeeDraftCardOrderIssueResponse_QNAME = new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraftCardOrderIssueResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alfabank.ws.cs.eq.wsmassemployeedraft11
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlaps }
     * 
     */
    public WSMassEmployeeDraftGetOverlaps createWSMassEmployeeDraftGetOverlaps() {
        return new WSMassEmployeeDraftGetOverlaps();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueResponse }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueResponse createWSMassEmployeeDraftGetCardsForIssueResponse() {
        return new WSMassEmployeeDraftGetCardsForIssueResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistory }
     * 
     */
    public WSMassEmployeeDraftGetHistory createWSMassEmployeeDraftGetHistory() {
        return new WSMassEmployeeDraftGetHistory();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftResponseType }
     * 
     */
    public WSMassEmployeeDraftResponseType createWSMassEmployeeDraftResponseType() {
        return new WSMassEmployeeDraftResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListResponseType }
     * 
     */
    public WSMassEmployeeDraftListResponseType createWSMassEmployeeDraftListResponseType() {
        return new WSMassEmployeeDraftListResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsResponseType }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsResponseType createWSMassEmployeeDraftGetOverlapsResponseType() {
        return new WSMassEmployeeDraftGetOverlapsResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDraftResponse }
     * 
     */
    public WSMassEmployeeDraftMaintainDraftResponse createWSMassEmployeeDraftMaintainDraftResponse() {
        return new WSMassEmployeeDraftMaintainDraftResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryResponse }
     * 
     */
    public WSMassEmployeeDraftGetHistoryResponse createWSMassEmployeeDraftGetHistoryResponse() {
        return new WSMassEmployeeDraftGetHistoryResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssue }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssue createWSMassEmployeeDraftGetCardsForIssue() {
        return new WSMassEmployeeDraftGetCardsForIssue();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDismiss }
     * 
     */
    public WSMassEmployeeDraftDeleteDismiss createWSMassEmployeeDraftDeleteDismiss() {
        return new WSMassEmployeeDraftDeleteDismiss();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryResponseType }
     * 
     */
    public WSMassEmployeeDraftGetHistoryResponseType createWSMassEmployeeDraftGetHistoryResponseType() {
        return new WSMassEmployeeDraftGetHistoryResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftOverlapAcceptResponseType }
     * 
     */
    public WSMassEmployeeDraftOverlapAcceptResponseType createWSMassEmployeeDraftOverlapAcceptResponseType() {
        return new WSMassEmployeeDraftOverlapAcceptResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueResponseType }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueResponseType createWSMassEmployeeDraftGetCardsForIssueResponseType() {
        return new WSMassEmployeeDraftGetCardsForIssueResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDraft }
     * 
     */
    public WSMassEmployeeDraftMaintainDraft createWSMassEmployeeDraftMaintainDraft() {
        return new WSMassEmployeeDraftMaintainDraft();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDraftResponse }
     * 
     */
    public WSMassEmployeeDraftDeleteDraftResponse createWSMassEmployeeDraftDeleteDraftResponse() {
        return new WSMassEmployeeDraftDeleteDraftResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftOverlapAccept }
     * 
     */
    public WSMassEmployeeDraftOverlapAccept createWSMassEmployeeDraftOverlapAccept() {
        return new WSMassEmployeeDraftOverlapAccept();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDismiss }
     * 
     */
    public WSMassEmployeeDraftAddDismiss createWSMassEmployeeDraftAddDismiss() {
        return new WSMassEmployeeDraftAddDismiss();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftResponse }
     * 
     */
    public WSMassEmployeeDraftResponse createWSMassEmployeeDraftResponse() {
        return new WSMassEmployeeDraftResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainEmployeeResponse }
     * 
     */
    public WSMassEmployeeDraftMaintainEmployeeResponse createWSMassEmployeeDraftMaintainEmployeeResponse() {
        return new WSMassEmployeeDraftMaintainEmployeeResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainEmployeeResponseType }
     * 
     */
    public WSMassEmployeeDraftMaintainEmployeeResponseType createWSMassEmployeeDraftMaintainEmployeeResponseType() {
        return new WSMassEmployeeDraftMaintainEmployeeResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsResponse }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsResponse createWSMassEmployeeDraftGetDocumentsResponse() {
        return new WSMassEmployeeDraftGetDocumentsResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDraftResponseType }
     * 
     */
    public WSMassEmployeeDraftDeleteDraftResponseType createWSMassEmployeeDraftDeleteDraftResponseType() {
        return new WSMassEmployeeDraftDeleteDraftResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDelete }
     * 
     */
    public WSMassEmployeeDraftDelete createWSMassEmployeeDraftDelete() {
        return new WSMassEmployeeDraftDelete();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteResponse }
     * 
     */
    public WSMassEmployeeDraftDeleteResponse createWSMassEmployeeDraftDeleteResponse() {
        return new WSMassEmployeeDraftDeleteResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDraft }
     * 
     */
    public WSMassEmployeeDraftDeleteDraft createWSMassEmployeeDraftDeleteDraft() {
        return new WSMassEmployeeDraftDeleteDraft();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAdd }
     * 
     */
    public WSMassEmployeeDraftAdd createWSMassEmployeeDraftAdd() {
        return new WSMassEmployeeDraftAdd();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesResponse }
     * 
     */
    public WSMassEmployeeDraftGetAddressesResponse createWSMassEmployeeDraftGetAddressesResponse() {
        return new WSMassEmployeeDraftGetAddressesResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsResponseType }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsResponseType createWSMassEmployeeDraftGetConvAccParmsResponseType() {
        return new WSMassEmployeeDraftGetConvAccParmsResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftOverlapAcceptResponse }
     * 
     */
    public WSMassEmployeeDraftOverlapAcceptResponse createWSMassEmployeeDraftOverlapAcceptResponse() {
        return new WSMassEmployeeDraftOverlapAcceptResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsResponse }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsResponse createWSMassEmployeeDraftGetOverlapsResponse() {
        return new WSMassEmployeeDraftGetOverlapsResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddresses }
     * 
     */
    public WSMassEmployeeDraftGetAddresses createWSMassEmployeeDraftGetAddresses() {
        return new WSMassEmployeeDraftGetAddresses();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsResponse }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsResponse createWSMassEmployeeDraftGetConvAccParmsResponse() {
        return new WSMassEmployeeDraftGetConvAccParmsResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteResponseType }
     * 
     */
    public WSMassEmployeeDraftDeleteResponseType createWSMassEmployeeDraftDeleteResponseType() {
        return new WSMassEmployeeDraftDeleteResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftList }
     * 
     */
    public WSMassEmployeeDraftList createWSMassEmployeeDraftList() {
        return new WSMassEmployeeDraftList();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDraft }
     * 
     */
    public WSMassEmployeeDraftAddDraft createWSMassEmployeeDraftAddDraft() {
        return new WSMassEmployeeDraftAddDraft();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDraftResponse }
     * 
     */
    public WSMassEmployeeDraftAddDraftResponse createWSMassEmployeeDraftAddDraftResponse() {
        return new WSMassEmployeeDraftAddDraftResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddResponseType }
     * 
     */
    public WSMassEmployeeDraftAddResponseType createWSMassEmployeeDraftAddResponseType() {
        return new WSMassEmployeeDraftAddResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsResponseType }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsResponseType createWSMassEmployeeDraftGetCardParmsResponseType() {
        return new WSMassEmployeeDraftGetCardParmsResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainEmployee }
     * 
     */
    public WSMassEmployeeDraftMaintainEmployee createWSMassEmployeeDraftMaintainEmployee() {
        return new WSMassEmployeeDraftMaintainEmployee();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftCardOrderIssueResponseType }
     * 
     */
    public WSMassEmployeeDraftCardOrderIssueResponseType createWSMassEmployeeDraftCardOrderIssueResponseType() {
        return new WSMassEmployeeDraftCardOrderIssueResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftCardOrderIssueResponse }
     * 
     */
    public WSMassEmployeeDraftCardOrderIssueResponse createWSMassEmployeeDraftCardOrderIssueResponse() {
        return new WSMassEmployeeDraftCardOrderIssueResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListResponse }
     * 
     */
    public WSMassEmployeeDraftListResponse createWSMassEmployeeDraftListResponse() {
        return new WSMassEmployeeDraftListResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesResponseType }
     * 
     */
    public WSMassEmployeeDraftGetAddressesResponseType createWSMassEmployeeDraftGetAddressesResponseType() {
        return new WSMassEmployeeDraftGetAddressesResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParms }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParms createWSMassEmployeeDraftGetConvAccParms() {
        return new WSMassEmployeeDraftGetConvAccParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParms }
     * 
     */
    public WSMassEmployeeDraftGetCardParms createWSMassEmployeeDraftGetCardParms() {
        return new WSMassEmployeeDraftGetCardParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDismiss }
     * 
     */
    public WSMassEmployeeDraftMaintainDismiss createWSMassEmployeeDraftMaintainDismiss() {
        return new WSMassEmployeeDraftMaintainDismiss();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocuments }
     * 
     */
    public WSMassEmployeeDraftGetDocuments createWSMassEmployeeDraftGetDocuments() {
        return new WSMassEmployeeDraftGetDocuments();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftChangeOrganizationResponseType }
     * 
     */
    public WSMassEmployeeDraftChangeOrganizationResponseType createWSMassEmployeeDraftChangeOrganizationResponseType() {
        return new WSMassEmployeeDraftChangeOrganizationResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftChangeOrganizationResponse }
     * 
     */
    public WSMassEmployeeDraftChangeOrganizationResponse createWSMassEmployeeDraftChangeOrganizationResponse() {
        return new WSMassEmployeeDraftChangeOrganizationResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDraftResponseType }
     * 
     */
    public WSMassEmployeeDraftAddDraftResponseType createWSMassEmployeeDraftAddDraftResponseType() {
        return new WSMassEmployeeDraftAddDraftResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftChangeOrganization }
     * 
     */
    public WSMassEmployeeDraftChangeOrganization createWSMassEmployeeDraftChangeOrganization() {
        return new WSMassEmployeeDraftChangeOrganization();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddResponse }
     * 
     */
    public WSMassEmployeeDraftAddResponse createWSMassEmployeeDraftAddResponse() {
        return new WSMassEmployeeDraftAddResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDraftResponseType }
     * 
     */
    public WSMassEmployeeDraftMaintainDraftResponseType createWSMassEmployeeDraftMaintainDraftResponseType() {
        return new WSMassEmployeeDraftMaintainDraftResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsResponseType }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsResponseType createWSMassEmployeeDraftGetDocumentsResponseType() {
        return new WSMassEmployeeDraftGetDocumentsResponseType();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsResponse }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsResponse createWSMassEmployeeDraftGetCardParmsResponse() {
        return new WSMassEmployeeDraftGetCardParmsResponse();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftCardOrderIssue }
     * 
     */
    public WSMassEmployeeDraftCardOrderIssue createWSMassEmployeeDraftCardOrderIssue() {
        return new WSMassEmployeeDraftCardOrderIssue();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftAddDraft }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftAddDraft")
    public JAXBElement<WSMassEmployeeDraftAddDraft> createWSMassEmployeeDraftAddDraft(WSMassEmployeeDraftAddDraft value) {
        return new JAXBElement<WSMassEmployeeDraftAddDraft>(_WSMassEmployeeDraftAddDraft_QNAME, WSMassEmployeeDraftAddDraft.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftOverlapAccept }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftOverlapAccept")
    public JAXBElement<WSMassEmployeeDraftOverlapAccept> createWSMassEmployeeDraftOverlapAccept(WSMassEmployeeDraftOverlapAccept value) {
        return new JAXBElement<WSMassEmployeeDraftOverlapAccept>(_WSMassEmployeeDraftOverlapAccept_QNAME, WSMassEmployeeDraftOverlapAccept.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetOverlaps }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetOverlaps")
    public JAXBElement<WSMassEmployeeDraftGetOverlaps> createWSMassEmployeeDraftGetOverlaps(WSMassEmployeeDraftGetOverlaps value) {
        return new JAXBElement<WSMassEmployeeDraftGetOverlaps>(_WSMassEmployeeDraftGetOverlaps_QNAME, WSMassEmployeeDraftGetOverlaps.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetCardsForIssue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetCardsForIssue")
    public JAXBElement<WSMassEmployeeDraftGetCardsForIssue> createWSMassEmployeeDraftGetCardsForIssue(WSMassEmployeeDraftGetCardsForIssue value) {
        return new JAXBElement<WSMassEmployeeDraftGetCardsForIssue>(_WSMassEmployeeDraftGetCardsForIssue_QNAME, WSMassEmployeeDraftGetCardsForIssue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftListResponse")
    public JAXBElement<WSMassEmployeeDraftListResponse> createWSMassEmployeeDraftListResponse(WSMassEmployeeDraftListResponse value) {
        return new JAXBElement<WSMassEmployeeDraftListResponse>(_WSMassEmployeeDraftListResponse_QNAME, WSMassEmployeeDraftListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetConvAccParms }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetConvAccParms")
    public JAXBElement<WSMassEmployeeDraftGetConvAccParms> createWSMassEmployeeDraftGetConvAccParms(WSMassEmployeeDraftGetConvAccParms value) {
        return new JAXBElement<WSMassEmployeeDraftGetConvAccParms>(_WSMassEmployeeDraftGetConvAccParms_QNAME, WSMassEmployeeDraftGetConvAccParms.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetHistoryResponse")
    public JAXBElement<WSMassEmployeeDraftGetHistoryResponse> createWSMassEmployeeDraftGetHistoryResponse(WSMassEmployeeDraftGetHistoryResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetHistoryResponse>(_WSMassEmployeeDraftGetHistoryResponse_QNAME, WSMassEmployeeDraftGetHistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetHistory }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetHistory")
    public JAXBElement<WSMassEmployeeDraftGetHistory> createWSMassEmployeeDraftGetHistory(WSMassEmployeeDraftGetHistory value) {
        return new JAXBElement<WSMassEmployeeDraftGetHistory>(_WSMassEmployeeDraftGetHistory_QNAME, WSMassEmployeeDraftGetHistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetAddresses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetAddresses")
    public JAXBElement<WSMassEmployeeDraftGetAddresses> createWSMassEmployeeDraftGetAddresses(WSMassEmployeeDraftGetAddresses value) {
        return new JAXBElement<WSMassEmployeeDraftGetAddresses>(_WSMassEmployeeDraftGetAddresses_QNAME, WSMassEmployeeDraftGetAddresses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetDocumentsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetDocumentsResponse")
    public JAXBElement<WSMassEmployeeDraftGetDocumentsResponse> createWSMassEmployeeDraftGetDocumentsResponse(WSMassEmployeeDraftGetDocumentsResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetDocumentsResponse>(_WSMassEmployeeDraftGetDocumentsResponse_QNAME, WSMassEmployeeDraftGetDocumentsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftMaintainEmployeeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftMaintainEmployeeResponse")
    public JAXBElement<WSMassEmployeeDraftMaintainEmployeeResponse> createWSMassEmployeeDraftMaintainEmployeeResponse(WSMassEmployeeDraftMaintainEmployeeResponse value) {
        return new JAXBElement<WSMassEmployeeDraftMaintainEmployeeResponse>(_WSMassEmployeeDraftMaintainEmployeeResponse_QNAME, WSMassEmployeeDraftMaintainEmployeeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftList")
    public JAXBElement<WSMassEmployeeDraftList> createWSMassEmployeeDraftList(WSMassEmployeeDraftList value) {
        return new JAXBElement<WSMassEmployeeDraftList>(_WSMassEmployeeDraftList_QNAME, WSMassEmployeeDraftList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftMaintainDraft }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftMaintainDraft")
    public JAXBElement<WSMassEmployeeDraftMaintainDraft> createWSMassEmployeeDraftMaintainDraft(WSMassEmployeeDraftMaintainDraft value) {
        return new JAXBElement<WSMassEmployeeDraftMaintainDraft>(_WSMassEmployeeDraftMaintainDraft_QNAME, WSMassEmployeeDraftMaintainDraft.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftDeleteDismiss }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftDeleteDismiss")
    public JAXBElement<WSMassEmployeeDraftDeleteDismiss> createWSMassEmployeeDraftDeleteDismiss(WSMassEmployeeDraftDeleteDismiss value) {
        return new JAXBElement<WSMassEmployeeDraftDeleteDismiss>(_WSMassEmployeeDraftDeleteDismiss_QNAME, WSMassEmployeeDraftDeleteDismiss.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftAddDraftResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftAddDraftResponse")
    public JAXBElement<WSMassEmployeeDraftAddDraftResponse> createWSMassEmployeeDraftAddDraftResponse(WSMassEmployeeDraftAddDraftResponse value) {
        return new JAXBElement<WSMassEmployeeDraftAddDraftResponse>(_WSMassEmployeeDraftAddDraftResponse_QNAME, WSMassEmployeeDraftAddDraftResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftMaintainEmployee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftMaintainEmployee")
    public JAXBElement<WSMassEmployeeDraftMaintainEmployee> createWSMassEmployeeDraftMaintainEmployee(WSMassEmployeeDraftMaintainEmployee value) {
        return new JAXBElement<WSMassEmployeeDraftMaintainEmployee>(_WSMassEmployeeDraftMaintainEmployee_QNAME, WSMassEmployeeDraftMaintainEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftCardOrderIssue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftCardOrderIssue")
    public JAXBElement<WSMassEmployeeDraftCardOrderIssue> createWSMassEmployeeDraftCardOrderIssue(WSMassEmployeeDraftCardOrderIssue value) {
        return new JAXBElement<WSMassEmployeeDraftCardOrderIssue>(_WSMassEmployeeDraftCardOrderIssue_QNAME, WSMassEmployeeDraftCardOrderIssue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftChangeOrganizationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftChangeOrganizationResponse")
    public JAXBElement<WSMassEmployeeDraftChangeOrganizationResponse> createWSMassEmployeeDraftChangeOrganizationResponse(WSMassEmployeeDraftChangeOrganizationResponse value) {
        return new JAXBElement<WSMassEmployeeDraftChangeOrganizationResponse>(_WSMassEmployeeDraftChangeOrganizationResponse_QNAME, WSMassEmployeeDraftChangeOrganizationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftChangeOrganization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftChangeOrganization")
    public JAXBElement<WSMassEmployeeDraftChangeOrganization> createWSMassEmployeeDraftChangeOrganization(WSMassEmployeeDraftChangeOrganization value) {
        return new JAXBElement<WSMassEmployeeDraftChangeOrganization>(_WSMassEmployeeDraftChangeOrganization_QNAME, WSMassEmployeeDraftChangeOrganization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftDeleteDraft }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftDeleteDraft")
    public JAXBElement<WSMassEmployeeDraftDeleteDraft> createWSMassEmployeeDraftDeleteDraft(WSMassEmployeeDraftDeleteDraft value) {
        return new JAXBElement<WSMassEmployeeDraftDeleteDraft>(_WSMassEmployeeDraftDeleteDraft_QNAME, WSMassEmployeeDraftDeleteDraft.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetOverlapsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetOverlapsResponse")
    public JAXBElement<WSMassEmployeeDraftGetOverlapsResponse> createWSMassEmployeeDraftGetOverlapsResponse(WSMassEmployeeDraftGetOverlapsResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetOverlapsResponse>(_WSMassEmployeeDraftGetOverlapsResponse_QNAME, WSMassEmployeeDraftGetOverlapsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftDeleteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftDeleteResponse")
    public JAXBElement<WSMassEmployeeDraftDeleteResponse> createWSMassEmployeeDraftDeleteResponse(WSMassEmployeeDraftDeleteResponse value) {
        return new JAXBElement<WSMassEmployeeDraftDeleteResponse>(_WSMassEmployeeDraftDeleteResponse_QNAME, WSMassEmployeeDraftDeleteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftMaintainDraftResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftMaintainDraftResponse")
    public JAXBElement<WSMassEmployeeDraftMaintainDraftResponse> createWSMassEmployeeDraftMaintainDraftResponse(WSMassEmployeeDraftMaintainDraftResponse value) {
        return new JAXBElement<WSMassEmployeeDraftMaintainDraftResponse>(_WSMassEmployeeDraftMaintainDraftResponse_QNAME, WSMassEmployeeDraftMaintainDraftResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetAddressesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetAddressesResponse")
    public JAXBElement<WSMassEmployeeDraftGetAddressesResponse> createWSMassEmployeeDraftGetAddressesResponse(WSMassEmployeeDraftGetAddressesResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetAddressesResponse>(_WSMassEmployeeDraftGetAddressesResponse_QNAME, WSMassEmployeeDraftGetAddressesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftMaintainDismiss }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftMaintainDismiss")
    public JAXBElement<WSMassEmployeeDraftMaintainDismiss> createWSMassEmployeeDraftMaintainDismiss(WSMassEmployeeDraftMaintainDismiss value) {
        return new JAXBElement<WSMassEmployeeDraftMaintainDismiss>(_WSMassEmployeeDraftMaintainDismiss_QNAME, WSMassEmployeeDraftMaintainDismiss.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetConvAccParmsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetConvAccParmsResponse")
    public JAXBElement<WSMassEmployeeDraftGetConvAccParmsResponse> createWSMassEmployeeDraftGetConvAccParmsResponse(WSMassEmployeeDraftGetConvAccParmsResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetConvAccParmsResponse>(_WSMassEmployeeDraftGetConvAccParmsResponse_QNAME, WSMassEmployeeDraftGetConvAccParmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftResponse")
    public JAXBElement<WSMassEmployeeDraftResponse> createWSMassEmployeeDraftResponse(WSMassEmployeeDraftResponse value) {
        return new JAXBElement<WSMassEmployeeDraftResponse>(_WSMassEmployeeDraftResponse_QNAME, WSMassEmployeeDraftResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftOverlapAcceptResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftOverlapAcceptResponse")
    public JAXBElement<WSMassEmployeeDraftOverlapAcceptResponse> createWSMassEmployeeDraftOverlapAcceptResponse(WSMassEmployeeDraftOverlapAcceptResponse value) {
        return new JAXBElement<WSMassEmployeeDraftOverlapAcceptResponse>(_WSMassEmployeeDraftOverlapAcceptResponse_QNAME, WSMassEmployeeDraftOverlapAcceptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetCardParms }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetCardParms")
    public JAXBElement<WSMassEmployeeDraftGetCardParms> createWSMassEmployeeDraftGetCardParms(WSMassEmployeeDraftGetCardParms value) {
        return new JAXBElement<WSMassEmployeeDraftGetCardParms>(_WSMassEmployeeDraftGetCardParms_QNAME, WSMassEmployeeDraftGetCardParms.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftDeleteDraftResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftDeleteDraftResponse")
    public JAXBElement<WSMassEmployeeDraftDeleteDraftResponse> createWSMassEmployeeDraftDeleteDraftResponse(WSMassEmployeeDraftDeleteDraftResponse value) {
        return new JAXBElement<WSMassEmployeeDraftDeleteDraftResponse>(_WSMassEmployeeDraftDeleteDraftResponse_QNAME, WSMassEmployeeDraftDeleteDraftResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftDelete }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftDelete")
    public JAXBElement<WSMassEmployeeDraftDelete> createWSMassEmployeeDraftDelete(WSMassEmployeeDraftDelete value) {
        return new JAXBElement<WSMassEmployeeDraftDelete>(_WSMassEmployeeDraftDelete_QNAME, WSMassEmployeeDraftDelete.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetCardParmsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetCardParmsResponse")
    public JAXBElement<WSMassEmployeeDraftGetCardParmsResponse> createWSMassEmployeeDraftGetCardParmsResponse(WSMassEmployeeDraftGetCardParmsResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetCardParmsResponse>(_WSMassEmployeeDraftGetCardParmsResponse_QNAME, WSMassEmployeeDraftGetCardParmsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetDocuments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetDocuments")
    public JAXBElement<WSMassEmployeeDraftGetDocuments> createWSMassEmployeeDraftGetDocuments(WSMassEmployeeDraftGetDocuments value) {
        return new JAXBElement<WSMassEmployeeDraftGetDocuments>(_WSMassEmployeeDraftGetDocuments_QNAME, WSMassEmployeeDraftGetDocuments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftGetCardsForIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftGetCardsForIssueResponse")
    public JAXBElement<WSMassEmployeeDraftGetCardsForIssueResponse> createWSMassEmployeeDraftGetCardsForIssueResponse(WSMassEmployeeDraftGetCardsForIssueResponse value) {
        return new JAXBElement<WSMassEmployeeDraftGetCardsForIssueResponse>(_WSMassEmployeeDraftGetCardsForIssueResponse_QNAME, WSMassEmployeeDraftGetCardsForIssueResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftAdd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftAdd")
    public JAXBElement<WSMassEmployeeDraftAdd> createWSMassEmployeeDraftAdd(WSMassEmployeeDraftAdd value) {
        return new JAXBElement<WSMassEmployeeDraftAdd>(_WSMassEmployeeDraftAdd_QNAME, WSMassEmployeeDraftAdd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftAddResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftAddResponse")
    public JAXBElement<WSMassEmployeeDraftAddResponse> createWSMassEmployeeDraftAddResponse(WSMassEmployeeDraftAddResponse value) {
        return new JAXBElement<WSMassEmployeeDraftAddResponse>(_WSMassEmployeeDraftAddResponse_QNAME, WSMassEmployeeDraftAddResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftAddDismiss }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftAddDismiss")
    public JAXBElement<WSMassEmployeeDraftAddDismiss> createWSMassEmployeeDraftAddDismiss(WSMassEmployeeDraftAddDismiss value) {
        return new JAXBElement<WSMassEmployeeDraftAddDismiss>(_WSMassEmployeeDraftAddDismiss_QNAME, WSMassEmployeeDraftAddDismiss.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSMassEmployeeDraftCardOrderIssueResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", name = "WSMassEmployeeDraftCardOrderIssueResponse")
    public JAXBElement<WSMassEmployeeDraftCardOrderIssueResponse> createWSMassEmployeeDraftCardOrderIssueResponse(WSMassEmployeeDraftCardOrderIssueResponse value) {
        return new JAXBElement<WSMassEmployeeDraftCardOrderIssueResponse>(_WSMassEmployeeDraftCardOrderIssueResponse_QNAME, WSMassEmployeeDraftCardOrderIssueResponse.class, null, value);
    }

}
