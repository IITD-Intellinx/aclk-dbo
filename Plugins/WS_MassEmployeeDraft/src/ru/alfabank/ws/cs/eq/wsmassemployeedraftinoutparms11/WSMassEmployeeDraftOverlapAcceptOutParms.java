
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftOverlapAcceptOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftOverlapAcceptOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="res" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="wmsg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar300" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftOverlapAcceptOutParms", propOrder = {
    "res",
    "wmsg"
})
public class WSMassEmployeeDraftOverlapAcceptOutParms {

    protected String res;
    protected String wmsg;

    /**
     * Gets the value of the res property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRes() {
        return res;
    }

    /**
     * Sets the value of the res property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRes(String value) {
        this.res = value;
    }

    /**
     * Gets the value of the wmsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWmsg() {
        return wmsg;
    }

    /**
     * Sets the value of the wmsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWmsg(String value) {
        this.wmsg = value;
    }

}
