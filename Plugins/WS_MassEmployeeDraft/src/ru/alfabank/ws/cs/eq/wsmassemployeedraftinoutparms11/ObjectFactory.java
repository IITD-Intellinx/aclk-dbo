
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsOutParms }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsOutParms createWSMassEmployeeDraftGetConvAccParmsOutParms() {
        return new WSMassEmployeeDraftGetConvAccParmsOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDraftOutParms }
     * 
     */
    public WSMassEmployeeDraftAddDraftOutParms createWSMassEmployeeDraftAddDraftOutParms() {
        return new WSMassEmployeeDraftAddDraftOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesInParms }
     * 
     */
    public WSMassEmployeeDraftGetAddressesInParms createWSMassEmployeeDraftGetAddressesInParms() {
        return new WSMassEmployeeDraftGetAddressesInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsOutResultSet createWSMassEmployeeDraftGetConvAccParmsOutResultSet() {
        return new WSMassEmployeeDraftGetConvAccParmsOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDraftOutParms }
     * 
     */
    public WSMassEmployeeDraftDeleteDraftOutParms createWSMassEmployeeDraftDeleteDraftOutParms() {
        return new WSMassEmployeeDraftDeleteDraftOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetAddressesOutResultSetRow createWSMassEmployeeDraftGetAddressesOutResultSetRow() {
        return new WSMassEmployeeDraftGetAddressesOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryInParms }
     * 
     */
    public WSMassEmployeeDraftGetHistoryInParms createWSMassEmployeeDraftGetHistoryInParms() {
        return new WSMassEmployeeDraftGetHistoryInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteDraftInParms }
     * 
     */
    public WSMassEmployeeDraftDeleteDraftInParms createWSMassEmployeeDraftDeleteDraftInParms() {
        return new WSMassEmployeeDraftDeleteDraftInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueOutResultSetRow createWSMassEmployeeDraftGetCardsForIssueOutResultSetRow() {
        return new WSMassEmployeeDraftGetCardsForIssueOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftOverlapAcceptOutParms }
     * 
     */
    public WSMassEmployeeDraftOverlapAcceptOutParms createWSMassEmployeeDraftOverlapAcceptOutParms() {
        return new WSMassEmployeeDraftOverlapAcceptOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsInParms }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsInParms createWSMassEmployeeDraftGetOverlapsInParms() {
        return new WSMassEmployeeDraftGetOverlapsInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDraftInParms }
     * 
     */
    public WSMassEmployeeDraftMaintainDraftInParms createWSMassEmployeeDraftMaintainDraftInParms() {
        return new WSMassEmployeeDraftMaintainDraftInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftChangeOrganizationInParms }
     * 
     */
    public WSMassEmployeeDraftChangeOrganizationInParms createWSMassEmployeeDraftChangeOrganizationInParms() {
        return new WSMassEmployeeDraftChangeOrganizationInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListOutParms }
     * 
     */
    public WSMassEmployeeDraftListOutParms createWSMassEmployeeDraftListOutParms() {
        return new WSMassEmployeeDraftListOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsInParms }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsInParms createWSMassEmployeeDraftGetCardParmsInParms() {
        return new WSMassEmployeeDraftGetCardParmsInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueInParms }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueInParms createWSMassEmployeeDraftGetCardsForIssueInParms() {
        return new WSMassEmployeeDraftGetCardsForIssueInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueOutResultSet createWSMassEmployeeDraftGetCardsForIssueOutResultSet() {
        return new WSMassEmployeeDraftGetCardsForIssueOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardsForIssueOutParms }
     * 
     */
    public WSMassEmployeeDraftGetCardsForIssueOutParms createWSMassEmployeeDraftGetCardsForIssueOutParms() {
        return new WSMassEmployeeDraftGetCardsForIssueOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListOutResultSet }
     * 
     */
    public WSMassEmployeeDraftListOutResultSet createWSMassEmployeeDraftListOutResultSet() {
        return new WSMassEmployeeDraftListOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsOutResultSetRow createWSMassEmployeeDraftGetCardParmsOutResultSetRow() {
        return new WSMassEmployeeDraftGetCardParmsOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetHistoryOutResultSet createWSMassEmployeeDraftGetHistoryOutResultSet() {
        return new WSMassEmployeeDraftGetHistoryOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryOutParms }
     * 
     */
    public WSMassEmployeeDraftGetHistoryOutParms createWSMassEmployeeDraftGetHistoryOutParms() {
        return new WSMassEmployeeDraftGetHistoryOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsOutResultSet createWSMassEmployeeDraftGetOverlapsOutResultSet() {
        return new WSMassEmployeeDraftGetOverlapsOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftCardOrderIssueInParms }
     * 
     */
    public WSMassEmployeeDraftCardOrderIssueInParms createWSMassEmployeeDraftCardOrderIssueInParms() {
        return new WSMassEmployeeDraftCardOrderIssueInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainDraftOutParms }
     * 
     */
    public WSMassEmployeeDraftMaintainDraftOutParms createWSMassEmployeeDraftMaintainDraftOutParms() {
        return new WSMassEmployeeDraftMaintainDraftOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddOutParms }
     * 
     */
    public WSMassEmployeeDraftAddOutParms createWSMassEmployeeDraftAddOutParms() {
        return new WSMassEmployeeDraftAddOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftDeleteInParms }
     * 
     */
    public WSMassEmployeeDraftDeleteInParms createWSMassEmployeeDraftDeleteInParms() {
        return new WSMassEmployeeDraftDeleteInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsOutResultSetRow createWSMassEmployeeDraftGetOverlapsOutResultSetRow() {
        return new WSMassEmployeeDraftGetOverlapsOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftMaintainEmployeeInParms }
     * 
     */
    public WSMassEmployeeDraftMaintainEmployeeInParms createWSMassEmployeeDraftMaintainEmployeeInParms() {
        return new WSMassEmployeeDraftMaintainEmployeeInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsOutResultSetRow createWSMassEmployeeDraftGetConvAccParmsOutResultSetRow() {
        return new WSMassEmployeeDraftGetConvAccParmsOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsOutParms }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsOutParms createWSMassEmployeeDraftGetDocumentsOutParms() {
        return new WSMassEmployeeDraftGetDocumentsOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetOverlapsOutParms }
     * 
     */
    public WSMassEmployeeDraftGetOverlapsOutParms createWSMassEmployeeDraftGetOverlapsOutParms() {
        return new WSMassEmployeeDraftGetOverlapsOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsInParms }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsInParms createWSMassEmployeeDraftGetDocumentsInParms() {
        return new WSMassEmployeeDraftGetDocumentsInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsOutResultSetRow createWSMassEmployeeDraftGetDocumentsOutResultSetRow() {
        return new WSMassEmployeeDraftGetDocumentsOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetDocumentsOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetDocumentsOutResultSet createWSMassEmployeeDraftGetDocumentsOutResultSet() {
        return new WSMassEmployeeDraftGetDocumentsOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetHistoryOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftGetHistoryOutResultSetRow createWSMassEmployeeDraftGetHistoryOutResultSetRow() {
        return new WSMassEmployeeDraftGetHistoryOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetAddressesOutResultSet createWSMassEmployeeDraftGetAddressesOutResultSet() {
        return new WSMassEmployeeDraftGetAddressesOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftOverlapAcceptInParms }
     * 
     */
    public WSMassEmployeeDraftOverlapAcceptInParms createWSMassEmployeeDraftOverlapAcceptInParms() {
        return new WSMassEmployeeDraftOverlapAcceptInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListInParms }
     * 
     */
    public WSMassEmployeeDraftListInParms createWSMassEmployeeDraftListInParms() {
        return new WSMassEmployeeDraftListInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddMaintainDeleteDismissInParms }
     * 
     */
    public WSMassEmployeeDraftAddMaintainDeleteDismissInParms createWSMassEmployeeDraftAddMaintainDeleteDismissInParms() {
        return new WSMassEmployeeDraftAddMaintainDeleteDismissInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetConvAccParmsInParms }
     * 
     */
    public WSMassEmployeeDraftGetConvAccParmsInParms createWSMassEmployeeDraftGetConvAccParmsInParms() {
        return new WSMassEmployeeDraftGetConvAccParmsInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsOutParms }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsOutParms createWSMassEmployeeDraftGetCardParmsOutParms() {
        return new WSMassEmployeeDraftGetCardParmsOutParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftListOutResultSetRow }
     * 
     */
    public WSMassEmployeeDraftListOutResultSetRow createWSMassEmployeeDraftListOutResultSetRow() {
        return new WSMassEmployeeDraftListOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddDraftInParms }
     * 
     */
    public WSMassEmployeeDraftAddDraftInParms createWSMassEmployeeDraftAddDraftInParms() {
        return new WSMassEmployeeDraftAddDraftInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetCardParmsOutResultSet }
     * 
     */
    public WSMassEmployeeDraftGetCardParmsOutResultSet createWSMassEmployeeDraftGetCardParmsOutResultSet() {
        return new WSMassEmployeeDraftGetCardParmsOutResultSet();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftAddInParms }
     * 
     */
    public WSMassEmployeeDraftAddInParms createWSMassEmployeeDraftAddInParms() {
        return new WSMassEmployeeDraftAddInParms();
    }

    /**
     * Create an instance of {@link WSMassEmployeeDraftGetAddressesOutParms }
     * 
     */
    public WSMassEmployeeDraftGetAddressesOutParms createWSMassEmployeeDraftGetAddressesOutParms() {
        return new WSMassEmployeeDraftGetAddressesOutParms();
    }

}
