
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftListOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftListOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="stsn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tme" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDateTime" minOccurs="0"/>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="pkid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rcus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="rclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fnm1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cnal" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnaln" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cnap" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnapn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="inn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar12" minOccurs="0"/>
 *         &lt;element name="tab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dlgn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="work" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="wdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="emb" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wadr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="phor" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="qni" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ddte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="obt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="arc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="new" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="srur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="susd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="seur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="acus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="aidn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar105" minOccurs="0"/>
 *         &lt;element name="ocr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="crpr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="crprn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="crdoc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="crdt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="crlim" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="brnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="brnmn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="wphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="mpho" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="mail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wmail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="prp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sconn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="ucd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ucdnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="ser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="num" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="or" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="cod" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar7" minOccurs="0"/>
 *         &lt;element name="opn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="regdt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="edte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="cdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="acc1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="acc2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="acc3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="min" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="max" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="ccy" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="usgn1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="fsgn1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="usgn2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="fsgn2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="comm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1000" minOccurs="0"/>
 *         &lt;element name="orcun" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="orinn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar12" minOccurs="0"/>
 *         &lt;element name="dsts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="brne" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="phid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fxmid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fnam" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="orinf" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="oed" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftListOutResultSetRow", propOrder = {
    "dfid",
    "orid",
    "cus",
    "clc",
    "sts",
    "stsn",
    "tme",
    "usid",
    "pkid",
    "rcus",
    "rclc",
    "fnm1",
    "fnm2",
    "fnm3",
    "dtbr",
    "mtbr",
    "sex",
    "cnal",
    "cnaln",
    "cnap",
    "cnapn",
    "inn",
    "tab",
    "dlgn",
    "work",
    "wdte",
    "emb",
    "wadr",
    "phor",
    "qni",
    "ddte",
    "obt",
    "arc",
    "_new",
    "srur",
    "susd",
    "seur",
    "acus",
    "aclc",
    "aidn",
    "ocr",
    "crpr",
    "crprn",
    "crdoc",
    "crdt",
    "crlim",
    "brnm",
    "brnmn",
    "fphn",
    "wphn",
    "mpho",
    "mail",
    "wmail",
    "prp",
    "scon",
    "sconn",
    "ucd",
    "ucdnm",
    "ser",
    "num",
    "or",
    "cod",
    "opn",
    "regdt",
    "edte",
    "cdte",
    "acc1",
    "acc2",
    "acc3",
    "min",
    "max",
    "ccy",
    "usgn1",
    "fsgn1",
    "usgn2",
    "fsgn2",
    "comm",
    "orcun",
    "orinn",
    "dsts",
    "brne",
    "phid",
    "fxmid",
    "fnam",
    "orinf",
    "oed"
})
public class WSMassEmployeeDraftListOutResultSetRow {

    protected String dfid;
    protected String orid;
    protected String cus;
    protected String clc;
    protected String sts;
    protected String stsn;
    protected XMLGregorianCalendar tme;
    protected String usid;
    protected String pkid;
    protected String rcus;
    protected String rclc;
    protected String fnm1;
    protected String fnm2;
    protected String fnm3;
    protected XMLGregorianCalendar dtbr;
    protected String mtbr;
    protected String sex;
    protected String cnal;
    protected String cnaln;
    protected String cnap;
    protected String cnapn;
    protected String inn;
    protected String tab;
    protected String dlgn;
    protected String work;
    protected XMLGregorianCalendar wdte;
    protected String emb;
    protected String wadr;
    protected String phor;
    protected String qni;
    protected XMLGregorianCalendar ddte;
    protected String obt;
    protected String arc;
    @XmlElement(name = "new")
    protected String _new;
    protected String srur;
    protected String susd;
    protected String seur;
    protected String acus;
    protected String aclc;
    protected String aidn;
    protected String ocr;
    protected String crpr;
    protected String crprn;
    protected String crdoc;
    protected XMLGregorianCalendar crdt;
    protected BigDecimal crlim;
    protected String brnm;
    protected String brnmn;
    protected String fphn;
    protected String wphn;
    protected String mpho;
    protected String mail;
    protected String wmail;
    protected String prp;
    protected String scon;
    protected String sconn;
    protected String ucd;
    protected String ucdnm;
    protected String ser;
    protected String num;
    protected String or;
    protected String cod;
    protected XMLGregorianCalendar opn;
    protected XMLGregorianCalendar regdt;
    protected XMLGregorianCalendar edte;
    protected XMLGregorianCalendar cdte;
    protected String acc1;
    protected String acc2;
    protected String acc3;
    protected BigDecimal min;
    protected BigDecimal max;
    protected String ccy;
    protected String usgn1;
    protected String fsgn1;
    protected String usgn2;
    protected String fsgn2;
    protected String comm;
    protected String orcun;
    protected String orinn;
    protected String dsts;
    protected String brne;
    protected String phid;
    protected String fxmid;
    protected String fnam;
    protected String orinf;
    protected XMLGregorianCalendar oed;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the stsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStsn() {
        return stsn;
    }

    /**
     * Sets the value of the stsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStsn(String value) {
        this.stsn = value;
    }

    /**
     * Gets the value of the tme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTme() {
        return tme;
    }

    /**
     * Sets the value of the tme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTme(XMLGregorianCalendar value) {
        this.tme = value;
    }

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the pkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * Sets the value of the pkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkid(String value) {
        this.pkid = value;
    }

    /**
     * Gets the value of the rcus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcus() {
        return rcus;
    }

    /**
     * Sets the value of the rcus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcus(String value) {
        this.rcus = value;
    }

    /**
     * Gets the value of the rclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRclc() {
        return rclc;
    }

    /**
     * Sets the value of the rclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRclc(String value) {
        this.rclc = value;
    }

    /**
     * Gets the value of the fnm1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm1() {
        return fnm1;
    }

    /**
     * Sets the value of the fnm1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm1(String value) {
        this.fnm1 = value;
    }

    /**
     * Gets the value of the fnm2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm2() {
        return fnm2;
    }

    /**
     * Sets the value of the fnm2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm2(String value) {
        this.fnm2 = value;
    }

    /**
     * Gets the value of the fnm3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm3() {
        return fnm3;
    }

    /**
     * Sets the value of the fnm3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm3(String value) {
        this.fnm3 = value;
    }

    /**
     * Gets the value of the dtbr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtbr() {
        return dtbr;
    }

    /**
     * Sets the value of the dtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtbr(XMLGregorianCalendar value) {
        this.dtbr = value;
    }

    /**
     * Gets the value of the mtbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMtbr() {
        return mtbr;
    }

    /**
     * Sets the value of the mtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMtbr(String value) {
        this.mtbr = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Gets the value of the cnal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnal() {
        return cnal;
    }

    /**
     * Sets the value of the cnal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnal(String value) {
        this.cnal = value;
    }

    /**
     * Gets the value of the cnaln property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnaln() {
        return cnaln;
    }

    /**
     * Sets the value of the cnaln property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnaln(String value) {
        this.cnaln = value;
    }

    /**
     * Gets the value of the cnap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnap() {
        return cnap;
    }

    /**
     * Sets the value of the cnap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnap(String value) {
        this.cnap = value;
    }

    /**
     * Gets the value of the cnapn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnapn() {
        return cnapn;
    }

    /**
     * Sets the value of the cnapn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnapn(String value) {
        this.cnapn = value;
    }

    /**
     * Gets the value of the inn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInn() {
        return inn;
    }

    /**
     * Sets the value of the inn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInn(String value) {
        this.inn = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTab() {
        return tab;
    }

    /**
     * Sets the value of the tab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTab(String value) {
        this.tab = value;
    }

    /**
     * Gets the value of the dlgn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlgn() {
        return dlgn;
    }

    /**
     * Sets the value of the dlgn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlgn(String value) {
        this.dlgn = value;
    }

    /**
     * Gets the value of the work property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWork() {
        return work;
    }

    /**
     * Sets the value of the work property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWork(String value) {
        this.work = value;
    }

    /**
     * Gets the value of the wdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWdte() {
        return wdte;
    }

    /**
     * Sets the value of the wdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWdte(XMLGregorianCalendar value) {
        this.wdte = value;
    }

    /**
     * Gets the value of the emb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmb() {
        return emb;
    }

    /**
     * Sets the value of the emb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmb(String value) {
        this.emb = value;
    }

    /**
     * Gets the value of the wadr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWadr() {
        return wadr;
    }

    /**
     * Sets the value of the wadr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWadr(String value) {
        this.wadr = value;
    }

    /**
     * Gets the value of the phor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhor() {
        return phor;
    }

    /**
     * Sets the value of the phor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhor(String value) {
        this.phor = value;
    }

    /**
     * Gets the value of the qni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQni() {
        return qni;
    }

    /**
     * Sets the value of the qni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQni(String value) {
        this.qni = value;
    }

    /**
     * Gets the value of the ddte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDdte() {
        return ddte;
    }

    /**
     * Sets the value of the ddte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDdte(XMLGregorianCalendar value) {
        this.ddte = value;
    }

    /**
     * Gets the value of the obt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObt() {
        return obt;
    }

    /**
     * Sets the value of the obt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObt(String value) {
        this.obt = value;
    }

    /**
     * Gets the value of the arc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArc() {
        return arc;
    }

    /**
     * Sets the value of the arc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArc(String value) {
        this.arc = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNew(String value) {
        this._new = value;
    }

    /**
     * Gets the value of the srur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrur() {
        return srur;
    }

    /**
     * Sets the value of the srur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrur(String value) {
        this.srur = value;
    }

    /**
     * Gets the value of the susd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSusd() {
        return susd;
    }

    /**
     * Sets the value of the susd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSusd(String value) {
        this.susd = value;
    }

    /**
     * Gets the value of the seur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeur() {
        return seur;
    }

    /**
     * Sets the value of the seur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeur(String value) {
        this.seur = value;
    }

    /**
     * Gets the value of the acus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcus() {
        return acus;
    }

    /**
     * Sets the value of the acus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcus(String value) {
        this.acus = value;
    }

    /**
     * Gets the value of the aclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclc() {
        return aclc;
    }

    /**
     * Sets the value of the aclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclc(String value) {
        this.aclc = value;
    }

    /**
     * Gets the value of the aidn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAidn() {
        return aidn;
    }

    /**
     * Sets the value of the aidn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAidn(String value) {
        this.aidn = value;
    }

    /**
     * Gets the value of the ocr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcr() {
        return ocr;
    }

    /**
     * Sets the value of the ocr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcr(String value) {
        this.ocr = value;
    }

    /**
     * Gets the value of the crpr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrpr() {
        return crpr;
    }

    /**
     * Sets the value of the crpr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrpr(String value) {
        this.crpr = value;
    }

    /**
     * Gets the value of the crprn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrprn() {
        return crprn;
    }

    /**
     * Sets the value of the crprn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrprn(String value) {
        this.crprn = value;
    }

    /**
     * Gets the value of the crdoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrdoc() {
        return crdoc;
    }

    /**
     * Sets the value of the crdoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrdoc(String value) {
        this.crdoc = value;
    }

    /**
     * Gets the value of the crdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCrdt() {
        return crdt;
    }

    /**
     * Sets the value of the crdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCrdt(XMLGregorianCalendar value) {
        this.crdt = value;
    }

    /**
     * Gets the value of the crlim property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCrlim() {
        return crlim;
    }

    /**
     * Sets the value of the crlim property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCrlim(BigDecimal value) {
        this.crlim = value;
    }

    /**
     * Gets the value of the brnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnm() {
        return brnm;
    }

    /**
     * Sets the value of the brnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnm(String value) {
        this.brnm = value;
    }

    /**
     * Gets the value of the brnmn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnmn() {
        return brnmn;
    }

    /**
     * Sets the value of the brnmn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnmn(String value) {
        this.brnmn = value;
    }

    /**
     * Gets the value of the fphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFphn() {
        return fphn;
    }

    /**
     * Sets the value of the fphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFphn(String value) {
        this.fphn = value;
    }

    /**
     * Gets the value of the wphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWphn() {
        return wphn;
    }

    /**
     * Sets the value of the wphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWphn(String value) {
        this.wphn = value;
    }

    /**
     * Gets the value of the mpho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpho() {
        return mpho;
    }

    /**
     * Sets the value of the mpho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpho(String value) {
        this.mpho = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the wmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWmail() {
        return wmail;
    }

    /**
     * Sets the value of the wmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWmail(String value) {
        this.wmail = value;
    }

    /**
     * Gets the value of the prp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrp() {
        return prp;
    }

    /**
     * Sets the value of the prp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrp(String value) {
        this.prp = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the sconn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSconn() {
        return sconn;
    }

    /**
     * Sets the value of the sconn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSconn(String value) {
        this.sconn = value;
    }

    /**
     * Gets the value of the ucd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcd() {
        return ucd;
    }

    /**
     * Sets the value of the ucd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcd(String value) {
        this.ucd = value;
    }

    /**
     * Gets the value of the ucdnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcdnm() {
        return ucdnm;
    }

    /**
     * Sets the value of the ucdnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcdnm(String value) {
        this.ucdnm = value;
    }

    /**
     * Gets the value of the ser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSer() {
        return ser;
    }

    /**
     * Sets the value of the ser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSer(String value) {
        this.ser = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

    /**
     * Gets the value of the or property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOr() {
        return or;
    }

    /**
     * Sets the value of the or property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOr(String value) {
        this.or = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCod() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCod(String value) {
        this.cod = value;
    }

    /**
     * Gets the value of the opn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpn() {
        return opn;
    }

    /**
     * Sets the value of the opn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpn(XMLGregorianCalendar value) {
        this.opn = value;
    }

    /**
     * Gets the value of the regdt property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRegdt() {
        return regdt;
    }

    /**
     * Sets the value of the regdt property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRegdt(XMLGregorianCalendar value) {
        this.regdt = value;
    }

    /**
     * Gets the value of the edte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEdte() {
        return edte;
    }

    /**
     * Sets the value of the edte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEdte(XMLGregorianCalendar value) {
        this.edte = value;
    }

    /**
     * Gets the value of the cdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCdte() {
        return cdte;
    }

    /**
     * Sets the value of the cdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCdte(XMLGregorianCalendar value) {
        this.cdte = value;
    }

    /**
     * Gets the value of the acc1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcc1() {
        return acc1;
    }

    /**
     * Sets the value of the acc1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcc1(String value) {
        this.acc1 = value;
    }

    /**
     * Gets the value of the acc2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcc2() {
        return acc2;
    }

    /**
     * Sets the value of the acc2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcc2(String value) {
        this.acc2 = value;
    }

    /**
     * Gets the value of the acc3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcc3() {
        return acc3;
    }

    /**
     * Sets the value of the acc3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcc3(String value) {
        this.acc3 = value;
    }

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMin(BigDecimal value) {
        this.min = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMax(BigDecimal value) {
        this.max = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the usgn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsgn1() {
        return usgn1;
    }

    /**
     * Sets the value of the usgn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsgn1(String value) {
        this.usgn1 = value;
    }

    /**
     * Gets the value of the fsgn1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFsgn1() {
        return fsgn1;
    }

    /**
     * Sets the value of the fsgn1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFsgn1(String value) {
        this.fsgn1 = value;
    }

    /**
     * Gets the value of the usgn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsgn2() {
        return usgn2;
    }

    /**
     * Sets the value of the usgn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsgn2(String value) {
        this.usgn2 = value;
    }

    /**
     * Gets the value of the fsgn2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFsgn2() {
        return fsgn2;
    }

    /**
     * Sets the value of the fsgn2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFsgn2(String value) {
        this.fsgn2 = value;
    }

    /**
     * Gets the value of the comm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComm() {
        return comm;
    }

    /**
     * Sets the value of the comm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComm(String value) {
        this.comm = value;
    }

    /**
     * Gets the value of the orcun property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrcun() {
        return orcun;
    }

    /**
     * Sets the value of the orcun property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrcun(String value) {
        this.orcun = value;
    }

    /**
     * Gets the value of the orinn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrinn() {
        return orinn;
    }

    /**
     * Sets the value of the orinn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrinn(String value) {
        this.orinn = value;
    }

    /**
     * Gets the value of the dsts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDsts() {
        return dsts;
    }

    /**
     * Sets the value of the dsts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDsts(String value) {
        this.dsts = value;
    }

    /**
     * Gets the value of the brne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrne() {
        return brne;
    }

    /**
     * Sets the value of the brne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrne(String value) {
        this.brne = value;
    }

    /**
     * Gets the value of the phid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhid() {
        return phid;
    }

    /**
     * Sets the value of the phid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhid(String value) {
        this.phid = value;
    }

    /**
     * Gets the value of the fxmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFxmid() {
        return fxmid;
    }

    /**
     * Sets the value of the fxmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFxmid(String value) {
        this.fxmid = value;
    }

    /**
     * Gets the value of the fnam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnam() {
        return fnam;
    }

    /**
     * Sets the value of the fnam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnam(String value) {
        this.fnam = value;
    }

    /**
     * Gets the value of the orinf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrinf() {
        return orinf;
    }

    /**
     * Sets the value of the orinf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrinf(String value) {
        this.orinf = value;
    }

    /**
     * Gets the value of the oed property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOed() {
        return oed;
    }

    /**
     * Sets the value of the oed property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOed(XMLGregorianCalendar value) {
        this.oed = value;
    }

}
