
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftMaintainEmployeeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftMaintainEmployeeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftMaintainEmployeeResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftMaintainEmployeeResponse", propOrder = {
    "response"
})
public class WSMassEmployeeDraftMaintainEmployeeResponse {

    @XmlElement(required = true)
    protected WSMassEmployeeDraftMaintainEmployeeResponseType response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftMaintainEmployeeResponseType }
     *     
     */
    public WSMassEmployeeDraftMaintainEmployeeResponseType getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftMaintainEmployeeResponseType }
     *     
     */
    public void setResponse(WSMassEmployeeDraftMaintainEmployeeResponseType value) {
        this.response = value;
    }

}
