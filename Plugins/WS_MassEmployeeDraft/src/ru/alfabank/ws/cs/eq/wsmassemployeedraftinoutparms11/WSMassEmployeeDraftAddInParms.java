
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftAddInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftAddInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal50" minOccurs="0"/>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="pkid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rcus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="rclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fnm1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="sex" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cnal" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnap" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="inn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar12" minOccurs="0"/>
 *         &lt;element name="tab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dlgn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="work" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="wdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="emb" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wadr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="phor" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="srur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="susd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="seur" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="acus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="aclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ocr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="crpr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="crlim" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="brnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="fphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="wphn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="mpho" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="mail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="wmail" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="prp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="scon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="zip" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="cna" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="reg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="treg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="regn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cit" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tcit" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="dwe" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tstr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="str" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="hnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="bnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="flt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="phn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *         &lt;element name="dulucd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="dulser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dulnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dulor" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *         &lt;element name="dulcod" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar7" minOccurs="0"/>
 *         &lt;element name="dulopn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mcnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="mcsdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mcedte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="dupucd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="dupnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dupsdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="dupedte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="ccy1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ccy2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ama2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="rat2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal117" minOccurs="0"/>
 *         &lt;element name="ccy3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ama3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="rat3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal117" minOccurs="0"/>
 *         &lt;element name="min" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="max" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="scrd1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scon1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sms1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="inbo1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="scrd2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scon2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sms2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="inbo2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="scrd3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar8" minOccurs="0"/>
 *         &lt;element name="scon3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="sms3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="inbo3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="brdeliv" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="idfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="ean1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ean2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ean3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="err" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1000" minOccurs="0"/>
 *         &lt;element name="odfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="brne" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="phid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="fxmid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="aifp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="mifare" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2048" minOccurs="0"/>
 *         &lt;element name="tme" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDateTime" minOccurs="0"/>
 *         &lt;element name="fnam" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftAddInParms", propOrder = {
    "usid",
    "rnm",
    "orid",
    "sts",
    "cus",
    "clc",
    "pkid",
    "rcus",
    "rclc",
    "fnm1",
    "fnm2",
    "fnm3",
    "dtbr",
    "mtbr",
    "sex",
    "cnal",
    "cnap",
    "inn",
    "tab",
    "dlgn",
    "work",
    "wdte",
    "emb",
    "wadr",
    "phor",
    "srur",
    "susd",
    "seur",
    "acus",
    "aclc",
    "ocr",
    "crpr",
    "crlim",
    "brnm",
    "fphn",
    "wphn",
    "mpho",
    "mail",
    "wmail",
    "prp",
    "scon",
    "zip",
    "cna",
    "reg",
    "treg",
    "regn",
    "cit",
    "tcit",
    "dwe",
    "tstr",
    "str",
    "hnum",
    "bnum",
    "flt",
    "phn",
    "dulucd",
    "dulser",
    "dulnum",
    "dulor",
    "dulcod",
    "dulopn",
    "mcnum",
    "mcsdte",
    "mcedte",
    "dupucd",
    "dupnum",
    "dupsdte",
    "dupedte",
    "ccy1",
    "ccy2",
    "ama2",
    "rat2",
    "ccy3",
    "ama3",
    "rat3",
    "min",
    "max",
    "scrd1",
    "scon1",
    "sms1",
    "inbo1",
    "scrd2",
    "scon2",
    "sms2",
    "inbo2",
    "scrd3",
    "scon3",
    "sms3",
    "inbo3",
    "brdeliv",
    "idfid",
    "ean1",
    "ean2",
    "ean3",
    "err",
    "odfid",
    "brne",
    "phid",
    "fxmid",
    "aifp",
    "mifare",
    "tme",
    "fnam"
})
public class WSMassEmployeeDraftAddInParms {

    protected String usid;
    protected BigDecimal rnm;
    protected String orid;
    protected String sts;
    protected String cus;
    protected String clc;
    protected String pkid;
    protected String rcus;
    protected String rclc;
    protected String fnm1;
    protected String fnm2;
    protected String fnm3;
    protected XMLGregorianCalendar dtbr;
    protected String mtbr;
    protected String sex;
    protected String cnal;
    protected String cnap;
    protected String inn;
    protected String tab;
    protected String dlgn;
    protected String work;
    protected XMLGregorianCalendar wdte;
    protected String emb;
    protected String wadr;
    protected String phor;
    protected String srur;
    protected String susd;
    protected String seur;
    protected String acus;
    protected String aclc;
    protected String ocr;
    protected String crpr;
    protected BigDecimal crlim;
    protected String brnm;
    protected String fphn;
    protected String wphn;
    protected String mpho;
    protected String mail;
    protected String wmail;
    protected String prp;
    protected String scon;
    protected String zip;
    protected String cna;
    protected String reg;
    protected String treg;
    protected String regn;
    protected String cit;
    protected String tcit;
    protected String dwe;
    protected String tstr;
    protected String str;
    protected String hnum;
    protected String bnum;
    protected String flt;
    protected String phn;
    protected String dulucd;
    protected String dulser;
    protected String dulnum;
    protected String dulor;
    protected String dulcod;
    protected XMLGregorianCalendar dulopn;
    protected String mcnum;
    protected XMLGregorianCalendar mcsdte;
    protected XMLGregorianCalendar mcedte;
    protected String dupucd;
    protected String dupnum;
    protected XMLGregorianCalendar dupsdte;
    protected XMLGregorianCalendar dupedte;
    protected String ccy1;
    protected String ccy2;
    protected BigDecimal ama2;
    protected BigDecimal rat2;
    protected String ccy3;
    protected BigDecimal ama3;
    protected BigDecimal rat3;
    protected BigDecimal min;
    protected BigDecimal max;
    protected String scrd1;
    protected String scon1;
    protected String sms1;
    protected String inbo1;
    protected String scrd2;
    protected String scon2;
    protected String sms2;
    protected String inbo2;
    protected String scrd3;
    protected String scon3;
    protected String sms3;
    protected String inbo3;
    protected String brdeliv;
    protected String idfid;
    protected String ean1;
    protected String ean2;
    protected String ean3;
    protected String err;
    protected String odfid;
    protected String brne;
    protected String phid;
    protected String fxmid;
    protected String aifp;
    protected String mifare;
    protected XMLGregorianCalendar tme;
    protected String fnam;

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the rnm property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRnm() {
        return rnm;
    }

    /**
     * Sets the value of the rnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRnm(BigDecimal value) {
        this.rnm = value;
    }

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the pkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * Sets the value of the pkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkid(String value) {
        this.pkid = value;
    }

    /**
     * Gets the value of the rcus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcus() {
        return rcus;
    }

    /**
     * Sets the value of the rcus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcus(String value) {
        this.rcus = value;
    }

    /**
     * Gets the value of the rclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRclc() {
        return rclc;
    }

    /**
     * Sets the value of the rclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRclc(String value) {
        this.rclc = value;
    }

    /**
     * Gets the value of the fnm1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm1() {
        return fnm1;
    }

    /**
     * Sets the value of the fnm1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm1(String value) {
        this.fnm1 = value;
    }

    /**
     * Gets the value of the fnm2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm2() {
        return fnm2;
    }

    /**
     * Sets the value of the fnm2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm2(String value) {
        this.fnm2 = value;
    }

    /**
     * Gets the value of the fnm3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm3() {
        return fnm3;
    }

    /**
     * Sets the value of the fnm3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm3(String value) {
        this.fnm3 = value;
    }

    /**
     * Gets the value of the dtbr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtbr() {
        return dtbr;
    }

    /**
     * Sets the value of the dtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtbr(XMLGregorianCalendar value) {
        this.dtbr = value;
    }

    /**
     * Gets the value of the mtbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMtbr() {
        return mtbr;
    }

    /**
     * Sets the value of the mtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMtbr(String value) {
        this.mtbr = value;
    }

    /**
     * Gets the value of the sex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets the value of the sex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSex(String value) {
        this.sex = value;
    }

    /**
     * Gets the value of the cnal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnal() {
        return cnal;
    }

    /**
     * Sets the value of the cnal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnal(String value) {
        this.cnal = value;
    }

    /**
     * Gets the value of the cnap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnap() {
        return cnap;
    }

    /**
     * Sets the value of the cnap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnap(String value) {
        this.cnap = value;
    }

    /**
     * Gets the value of the inn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInn() {
        return inn;
    }

    /**
     * Sets the value of the inn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInn(String value) {
        this.inn = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTab() {
        return tab;
    }

    /**
     * Sets the value of the tab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTab(String value) {
        this.tab = value;
    }

    /**
     * Gets the value of the dlgn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDlgn() {
        return dlgn;
    }

    /**
     * Sets the value of the dlgn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDlgn(String value) {
        this.dlgn = value;
    }

    /**
     * Gets the value of the work property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWork() {
        return work;
    }

    /**
     * Sets the value of the work property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWork(String value) {
        this.work = value;
    }

    /**
     * Gets the value of the wdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getWdte() {
        return wdte;
    }

    /**
     * Sets the value of the wdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setWdte(XMLGregorianCalendar value) {
        this.wdte = value;
    }

    /**
     * Gets the value of the emb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmb() {
        return emb;
    }

    /**
     * Sets the value of the emb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmb(String value) {
        this.emb = value;
    }

    /**
     * Gets the value of the wadr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWadr() {
        return wadr;
    }

    /**
     * Sets the value of the wadr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWadr(String value) {
        this.wadr = value;
    }

    /**
     * Gets the value of the phor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhor() {
        return phor;
    }

    /**
     * Sets the value of the phor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhor(String value) {
        this.phor = value;
    }

    /**
     * Gets the value of the srur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrur() {
        return srur;
    }

    /**
     * Sets the value of the srur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrur(String value) {
        this.srur = value;
    }

    /**
     * Gets the value of the susd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSusd() {
        return susd;
    }

    /**
     * Sets the value of the susd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSusd(String value) {
        this.susd = value;
    }

    /**
     * Gets the value of the seur property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeur() {
        return seur;
    }

    /**
     * Sets the value of the seur property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeur(String value) {
        this.seur = value;
    }

    /**
     * Gets the value of the acus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcus() {
        return acus;
    }

    /**
     * Sets the value of the acus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcus(String value) {
        this.acus = value;
    }

    /**
     * Gets the value of the aclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclc() {
        return aclc;
    }

    /**
     * Sets the value of the aclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclc(String value) {
        this.aclc = value;
    }

    /**
     * Gets the value of the ocr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcr() {
        return ocr;
    }

    /**
     * Sets the value of the ocr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcr(String value) {
        this.ocr = value;
    }

    /**
     * Gets the value of the crpr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrpr() {
        return crpr;
    }

    /**
     * Sets the value of the crpr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrpr(String value) {
        this.crpr = value;
    }

    /**
     * Gets the value of the crlim property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCrlim() {
        return crlim;
    }

    /**
     * Sets the value of the crlim property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCrlim(BigDecimal value) {
        this.crlim = value;
    }

    /**
     * Gets the value of the brnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnm() {
        return brnm;
    }

    /**
     * Sets the value of the brnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnm(String value) {
        this.brnm = value;
    }

    /**
     * Gets the value of the fphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFphn() {
        return fphn;
    }

    /**
     * Sets the value of the fphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFphn(String value) {
        this.fphn = value;
    }

    /**
     * Gets the value of the wphn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWphn() {
        return wphn;
    }

    /**
     * Sets the value of the wphn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWphn(String value) {
        this.wphn = value;
    }

    /**
     * Gets the value of the mpho property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpho() {
        return mpho;
    }

    /**
     * Sets the value of the mpho property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpho(String value) {
        this.mpho = value;
    }

    /**
     * Gets the value of the mail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMail() {
        return mail;
    }

    /**
     * Sets the value of the mail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMail(String value) {
        this.mail = value;
    }

    /**
     * Gets the value of the wmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWmail() {
        return wmail;
    }

    /**
     * Sets the value of the wmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWmail(String value) {
        this.wmail = value;
    }

    /**
     * Gets the value of the prp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrp() {
        return prp;
    }

    /**
     * Sets the value of the prp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrp(String value) {
        this.prp = value;
    }

    /**
     * Gets the value of the scon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon() {
        return scon;
    }

    /**
     * Sets the value of the scon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon(String value) {
        this.scon = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the cna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCna() {
        return cna;
    }

    /**
     * Sets the value of the cna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCna(String value) {
        this.cna = value;
    }

    /**
     * Gets the value of the reg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReg() {
        return reg;
    }

    /**
     * Sets the value of the reg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReg(String value) {
        this.reg = value;
    }

    /**
     * Gets the value of the treg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreg() {
        return treg;
    }

    /**
     * Sets the value of the treg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreg(String value) {
        this.treg = value;
    }

    /**
     * Gets the value of the regn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegn() {
        return regn;
    }

    /**
     * Sets the value of the regn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegn(String value) {
        this.regn = value;
    }

    /**
     * Gets the value of the cit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCit() {
        return cit;
    }

    /**
     * Sets the value of the cit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCit(String value) {
        this.cit = value;
    }

    /**
     * Gets the value of the tcit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcit() {
        return tcit;
    }

    /**
     * Sets the value of the tcit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcit(String value) {
        this.tcit = value;
    }

    /**
     * Gets the value of the dwe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDwe() {
        return dwe;
    }

    /**
     * Sets the value of the dwe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDwe(String value) {
        this.dwe = value;
    }

    /**
     * Gets the value of the tstr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstr() {
        return tstr;
    }

    /**
     * Sets the value of the tstr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstr(String value) {
        this.tstr = value;
    }

    /**
     * Gets the value of the str property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStr() {
        return str;
    }

    /**
     * Sets the value of the str property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStr(String value) {
        this.str = value;
    }

    /**
     * Gets the value of the hnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHnum() {
        return hnum;
    }

    /**
     * Sets the value of the hnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHnum(String value) {
        this.hnum = value;
    }

    /**
     * Gets the value of the bnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBnum() {
        return bnum;
    }

    /**
     * Sets the value of the bnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBnum(String value) {
        this.bnum = value;
    }

    /**
     * Gets the value of the flt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlt() {
        return flt;
    }

    /**
     * Sets the value of the flt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlt(String value) {
        this.flt = value;
    }

    /**
     * Gets the value of the phn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhn() {
        return phn;
    }

    /**
     * Sets the value of the phn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhn(String value) {
        this.phn = value;
    }

    /**
     * Gets the value of the dulucd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulucd() {
        return dulucd;
    }

    /**
     * Sets the value of the dulucd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulucd(String value) {
        this.dulucd = value;
    }

    /**
     * Gets the value of the dulser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulser() {
        return dulser;
    }

    /**
     * Sets the value of the dulser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulser(String value) {
        this.dulser = value;
    }

    /**
     * Gets the value of the dulnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulnum() {
        return dulnum;
    }

    /**
     * Sets the value of the dulnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulnum(String value) {
        this.dulnum = value;
    }

    /**
     * Gets the value of the dulor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulor() {
        return dulor;
    }

    /**
     * Sets the value of the dulor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulor(String value) {
        this.dulor = value;
    }

    /**
     * Gets the value of the dulcod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDulcod() {
        return dulcod;
    }

    /**
     * Sets the value of the dulcod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDulcod(String value) {
        this.dulcod = value;
    }

    /**
     * Gets the value of the dulopn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDulopn() {
        return dulopn;
    }

    /**
     * Sets the value of the dulopn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDulopn(XMLGregorianCalendar value) {
        this.dulopn = value;
    }

    /**
     * Gets the value of the mcnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcnum() {
        return mcnum;
    }

    /**
     * Sets the value of the mcnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcnum(String value) {
        this.mcnum = value;
    }

    /**
     * Gets the value of the mcsdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMcsdte() {
        return mcsdte;
    }

    /**
     * Sets the value of the mcsdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMcsdte(XMLGregorianCalendar value) {
        this.mcsdte = value;
    }

    /**
     * Gets the value of the mcedte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMcedte() {
        return mcedte;
    }

    /**
     * Sets the value of the mcedte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMcedte(XMLGregorianCalendar value) {
        this.mcedte = value;
    }

    /**
     * Gets the value of the dupucd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDupucd() {
        return dupucd;
    }

    /**
     * Sets the value of the dupucd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDupucd(String value) {
        this.dupucd = value;
    }

    /**
     * Gets the value of the dupnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDupnum() {
        return dupnum;
    }

    /**
     * Sets the value of the dupnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDupnum(String value) {
        this.dupnum = value;
    }

    /**
     * Gets the value of the dupsdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDupsdte() {
        return dupsdte;
    }

    /**
     * Sets the value of the dupsdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDupsdte(XMLGregorianCalendar value) {
        this.dupsdte = value;
    }

    /**
     * Gets the value of the dupedte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDupedte() {
        return dupedte;
    }

    /**
     * Sets the value of the dupedte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDupedte(XMLGregorianCalendar value) {
        this.dupedte = value;
    }

    /**
     * Gets the value of the ccy1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy1() {
        return ccy1;
    }

    /**
     * Sets the value of the ccy1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy1(String value) {
        this.ccy1 = value;
    }

    /**
     * Gets the value of the ccy2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy2() {
        return ccy2;
    }

    /**
     * Sets the value of the ccy2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy2(String value) {
        this.ccy2 = value;
    }

    /**
     * Gets the value of the ama2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAma2() {
        return ama2;
    }

    /**
     * Sets the value of the ama2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAma2(BigDecimal value) {
        this.ama2 = value;
    }

    /**
     * Gets the value of the rat2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRat2() {
        return rat2;
    }

    /**
     * Sets the value of the rat2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRat2(BigDecimal value) {
        this.rat2 = value;
    }

    /**
     * Gets the value of the ccy3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy3() {
        return ccy3;
    }

    /**
     * Sets the value of the ccy3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy3(String value) {
        this.ccy3 = value;
    }

    /**
     * Gets the value of the ama3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAma3() {
        return ama3;
    }

    /**
     * Sets the value of the ama3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAma3(BigDecimal value) {
        this.ama3 = value;
    }

    /**
     * Gets the value of the rat3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRat3() {
        return rat3;
    }

    /**
     * Sets the value of the rat3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRat3(BigDecimal value) {
        this.rat3 = value;
    }

    /**
     * Gets the value of the min property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMin() {
        return min;
    }

    /**
     * Sets the value of the min property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMin(BigDecimal value) {
        this.min = value;
    }

    /**
     * Gets the value of the max property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMax() {
        return max;
    }

    /**
     * Sets the value of the max property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMax(BigDecimal value) {
        this.max = value;
    }

    /**
     * Gets the value of the scrd1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd1() {
        return scrd1;
    }

    /**
     * Sets the value of the scrd1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd1(String value) {
        this.scrd1 = value;
    }

    /**
     * Gets the value of the scon1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon1() {
        return scon1;
    }

    /**
     * Sets the value of the scon1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon1(String value) {
        this.scon1 = value;
    }

    /**
     * Gets the value of the sms1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSms1() {
        return sms1;
    }

    /**
     * Sets the value of the sms1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSms1(String value) {
        this.sms1 = value;
    }

    /**
     * Gets the value of the inbo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInbo1() {
        return inbo1;
    }

    /**
     * Sets the value of the inbo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInbo1(String value) {
        this.inbo1 = value;
    }

    /**
     * Gets the value of the scrd2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd2() {
        return scrd2;
    }

    /**
     * Sets the value of the scrd2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd2(String value) {
        this.scrd2 = value;
    }

    /**
     * Gets the value of the scon2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon2() {
        return scon2;
    }

    /**
     * Sets the value of the scon2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon2(String value) {
        this.scon2 = value;
    }

    /**
     * Gets the value of the sms2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSms2() {
        return sms2;
    }

    /**
     * Sets the value of the sms2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSms2(String value) {
        this.sms2 = value;
    }

    /**
     * Gets the value of the inbo2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInbo2() {
        return inbo2;
    }

    /**
     * Sets the value of the inbo2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInbo2(String value) {
        this.inbo2 = value;
    }

    /**
     * Gets the value of the scrd3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScrd3() {
        return scrd3;
    }

    /**
     * Sets the value of the scrd3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScrd3(String value) {
        this.scrd3 = value;
    }

    /**
     * Gets the value of the scon3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScon3() {
        return scon3;
    }

    /**
     * Sets the value of the scon3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScon3(String value) {
        this.scon3 = value;
    }

    /**
     * Gets the value of the sms3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSms3() {
        return sms3;
    }

    /**
     * Sets the value of the sms3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSms3(String value) {
        this.sms3 = value;
    }

    /**
     * Gets the value of the inbo3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInbo3() {
        return inbo3;
    }

    /**
     * Sets the value of the inbo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInbo3(String value) {
        this.inbo3 = value;
    }

    /**
     * Gets the value of the brdeliv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrdeliv() {
        return brdeliv;
    }

    /**
     * Sets the value of the brdeliv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrdeliv(String value) {
        this.brdeliv = value;
    }

    /**
     * Gets the value of the idfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdfid() {
        return idfid;
    }

    /**
     * Sets the value of the idfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdfid(String value) {
        this.idfid = value;
    }

    /**
     * Gets the value of the ean1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan1() {
        return ean1;
    }

    /**
     * Sets the value of the ean1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan1(String value) {
        this.ean1 = value;
    }

    /**
     * Gets the value of the ean2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan2() {
        return ean2;
    }

    /**
     * Sets the value of the ean2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan2(String value) {
        this.ean2 = value;
    }

    /**
     * Gets the value of the ean3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan3() {
        return ean3;
    }

    /**
     * Sets the value of the ean3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan3(String value) {
        this.ean3 = value;
    }

    /**
     * Gets the value of the err property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErr() {
        return err;
    }

    /**
     * Sets the value of the err property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErr(String value) {
        this.err = value;
    }

    /**
     * Gets the value of the odfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOdfid() {
        return odfid;
    }

    /**
     * Sets the value of the odfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOdfid(String value) {
        this.odfid = value;
    }

    /**
     * Gets the value of the brne property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrne() {
        return brne;
    }

    /**
     * Sets the value of the brne property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrne(String value) {
        this.brne = value;
    }

    /**
     * Gets the value of the phid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhid() {
        return phid;
    }

    /**
     * Sets the value of the phid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhid(String value) {
        this.phid = value;
    }

    /**
     * Gets the value of the fxmid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFxmid() {
        return fxmid;
    }

    /**
     * Sets the value of the fxmid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFxmid(String value) {
        this.fxmid = value;
    }

    /**
     * Gets the value of the aifp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAifp() {
        return aifp;
    }

    /**
     * Sets the value of the aifp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAifp(String value) {
        this.aifp = value;
    }

    /**
     * Gets the value of the mifare property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMifare() {
        return mifare;
    }

    /**
     * Sets the value of the mifare property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMifare(String value) {
        this.mifare = value;
    }

    /**
     * Gets the value of the tme property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTme() {
        return tme;
    }

    /**
     * Sets the value of the tme property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTme(XMLGregorianCalendar value) {
        this.tme = value;
    }

    /**
     * Gets the value of the fnam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnam() {
        return fnam;
    }

    /**
     * Sets the value of the fnam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnam(String value) {
        this.fnam = value;
    }

}
