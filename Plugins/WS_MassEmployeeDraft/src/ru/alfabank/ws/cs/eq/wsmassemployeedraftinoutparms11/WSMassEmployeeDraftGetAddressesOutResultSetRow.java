
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftGetAddressesOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetAddressesOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="typ" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="typnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="zip" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="cna" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnanm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="reg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="regnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="treg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="tregnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="regn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cit" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tcit" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="tcitnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dwe" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tstr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="tstrnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="str" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="hnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="bnum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="flt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="phn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar18" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetAddressesOutResultSetRow", propOrder = {
    "dfid",
    "typ",
    "typnm",
    "zip",
    "cna",
    "cnanm",
    "reg",
    "regnm",
    "treg",
    "tregnm",
    "regn",
    "cit",
    "tcit",
    "tcitnm",
    "dwe",
    "tstr",
    "tstrnm",
    "str",
    "hnum",
    "bnum",
    "flt",
    "phn"
})
public class WSMassEmployeeDraftGetAddressesOutResultSetRow {

    protected String dfid;
    protected String typ;
    protected String typnm;
    protected String zip;
    protected String cna;
    protected String cnanm;
    protected String reg;
    protected String regnm;
    protected String treg;
    protected String tregnm;
    protected String regn;
    protected String cit;
    protected String tcit;
    protected String tcitnm;
    protected String dwe;
    protected String tstr;
    protected String tstrnm;
    protected String str;
    protected String hnum;
    protected String bnum;
    protected String flt;
    protected String phn;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the typ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTyp() {
        return typ;
    }

    /**
     * Sets the value of the typ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTyp(String value) {
        this.typ = value;
    }

    /**
     * Gets the value of the typnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypnm() {
        return typnm;
    }

    /**
     * Sets the value of the typnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypnm(String value) {
        this.typnm = value;
    }

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZip() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZip(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the cna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCna() {
        return cna;
    }

    /**
     * Sets the value of the cna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCna(String value) {
        this.cna = value;
    }

    /**
     * Gets the value of the cnanm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnanm() {
        return cnanm;
    }

    /**
     * Sets the value of the cnanm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnanm(String value) {
        this.cnanm = value;
    }

    /**
     * Gets the value of the reg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReg() {
        return reg;
    }

    /**
     * Sets the value of the reg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReg(String value) {
        this.reg = value;
    }

    /**
     * Gets the value of the regnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegnm() {
        return regnm;
    }

    /**
     * Sets the value of the regnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegnm(String value) {
        this.regnm = value;
    }

    /**
     * Gets the value of the treg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTreg() {
        return treg;
    }

    /**
     * Sets the value of the treg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTreg(String value) {
        this.treg = value;
    }

    /**
     * Gets the value of the tregnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTregnm() {
        return tregnm;
    }

    /**
     * Sets the value of the tregnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTregnm(String value) {
        this.tregnm = value;
    }

    /**
     * Gets the value of the regn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegn() {
        return regn;
    }

    /**
     * Sets the value of the regn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegn(String value) {
        this.regn = value;
    }

    /**
     * Gets the value of the cit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCit() {
        return cit;
    }

    /**
     * Sets the value of the cit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCit(String value) {
        this.cit = value;
    }

    /**
     * Gets the value of the tcit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcit() {
        return tcit;
    }

    /**
     * Sets the value of the tcit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcit(String value) {
        this.tcit = value;
    }

    /**
     * Gets the value of the tcitnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTcitnm() {
        return tcitnm;
    }

    /**
     * Sets the value of the tcitnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTcitnm(String value) {
        this.tcitnm = value;
    }

    /**
     * Gets the value of the dwe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDwe() {
        return dwe;
    }

    /**
     * Sets the value of the dwe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDwe(String value) {
        this.dwe = value;
    }

    /**
     * Gets the value of the tstr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstr() {
        return tstr;
    }

    /**
     * Sets the value of the tstr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstr(String value) {
        this.tstr = value;
    }

    /**
     * Gets the value of the tstrnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTstrnm() {
        return tstrnm;
    }

    /**
     * Sets the value of the tstrnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTstrnm(String value) {
        this.tstrnm = value;
    }

    /**
     * Gets the value of the str property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStr() {
        return str;
    }

    /**
     * Sets the value of the str property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStr(String value) {
        this.str = value;
    }

    /**
     * Gets the value of the hnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHnum() {
        return hnum;
    }

    /**
     * Sets the value of the hnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHnum(String value) {
        this.hnum = value;
    }

    /**
     * Gets the value of the bnum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBnum() {
        return bnum;
    }

    /**
     * Sets the value of the bnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBnum(String value) {
        this.bnum = value;
    }

    /**
     * Gets the value of the flt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlt() {
        return flt;
    }

    /**
     * Sets the value of the flt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlt(String value) {
        this.flt = value;
    }

    /**
     * Gets the value of the phn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhn() {
        return phn;
    }

    /**
     * Sets the value of the phn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhn(String value) {
        this.phn = value;
    }

}
