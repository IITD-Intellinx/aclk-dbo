
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftDeleteInParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftDeleteInParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="pkid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="usid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dtor" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftDeleteInParms", propOrder = {
    "orid",
    "cus",
    "clc",
    "pkid",
    "usid",
    "dtor"
})
public class WSMassEmployeeDraftDeleteInParms {

    protected String orid;
    protected String cus;
    protected String clc;
    protected String pkid;
    protected String usid;
    protected XMLGregorianCalendar dtor;

    /**
     * Gets the value of the orid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrid() {
        return orid;
    }

    /**
     * Sets the value of the orid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrid(String value) {
        this.orid = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the pkid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPkid() {
        return pkid;
    }

    /**
     * Sets the value of the pkid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPkid(String value) {
        this.pkid = value;
    }

    /**
     * Gets the value of the usid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsid() {
        return usid;
    }

    /**
     * Sets the value of the usid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsid(String value) {
        this.usid = value;
    }

    /**
     * Gets the value of the dtor property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtor() {
        return dtor;
    }

    /**
     * Sets the value of the dtor property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtor(XMLGregorianCalendar value) {
        this.dtor = value;
    }

}
