
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSMassEmployeeDraftGetAddressesOutResultSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetAddressesOutResultSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultSetRow" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftGetAddressesOutResultSetRow" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetAddressesOutResultSet", propOrder = {
    "resultSetRow"
})
public class WSMassEmployeeDraftGetAddressesOutResultSet {

    protected List<WSMassEmployeeDraftGetAddressesOutResultSetRow> resultSetRow;

    /**
     * Gets the value of the resultSetRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resultSetRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResultSetRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSMassEmployeeDraftGetAddressesOutResultSetRow }
     * 
     * 
     */
    public List<WSMassEmployeeDraftGetAddressesOutResultSetRow> getResultSetRow() {
        if (resultSetRow == null) {
            resultSetRow = new ArrayList<WSMassEmployeeDraftGetAddressesOutResultSetRow>();
        }
        return this.resultSetRow;
    }

}
