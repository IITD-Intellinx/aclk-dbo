
package ru.alfabank.ws.cs.eq.wsmassemployeedraft11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftOverlapAcceptOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;


/**
 * <p>Java class for WSMassEmployeeDraftOverlapAcceptResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftOverlapAcceptResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outCommonParms" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSCommonOutParms"/>
 *         &lt;element name="outParms" type="{http://WSMassEmployeeDraftInOutParms11.EQ.CS.ws.alfabank.ru}WSMassEmployeeDraftOverlapAcceptOutParms"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftOverlapAcceptResponseType", propOrder = {
    "outCommonParms",
    "outParms"
})
public class WSMassEmployeeDraftOverlapAcceptResponseType {

    @XmlElement(required = true)
    protected WSCommonOutParms outCommonParms;
    @XmlElement(required = true)
    protected WSMassEmployeeDraftOverlapAcceptOutParms outParms;

    /**
     * Gets the value of the outCommonParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSCommonOutParms }
     *     
     */
    public WSCommonOutParms getOutCommonParms() {
        return outCommonParms;
    }

    /**
     * Sets the value of the outCommonParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCommonOutParms }
     *     
     */
    public void setOutCommonParms(WSCommonOutParms value) {
        this.outCommonParms = value;
    }

    /**
     * Gets the value of the outParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSMassEmployeeDraftOverlapAcceptOutParms }
     *     
     */
    public WSMassEmployeeDraftOverlapAcceptOutParms getOutParms() {
        return outParms;
    }

    /**
     * Sets the value of the outParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSMassEmployeeDraftOverlapAcceptOutParms }
     *     
     */
    public void setOutParms(WSMassEmployeeDraftOverlapAcceptOutParms value) {
        this.outParms = value;
    }

}
