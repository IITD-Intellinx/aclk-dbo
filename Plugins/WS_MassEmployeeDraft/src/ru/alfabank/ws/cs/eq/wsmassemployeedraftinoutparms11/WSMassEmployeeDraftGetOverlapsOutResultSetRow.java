
package ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSMassEmployeeDraftGetOverlapsOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSMassEmployeeDraftGetOverlapsOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dfid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="rno" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal30" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fnm1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="fnm3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="dtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="ucd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="shn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="kser" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="knum" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="kopn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="mtbr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cnap" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnapn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cnal" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnaln" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSMassEmployeeDraftGetOverlapsOutResultSetRow", propOrder = {
    "dfid",
    "rno",
    "cus",
    "clc",
    "fnm1",
    "fnm2",
    "fnm3",
    "tab",
    "dtbr",
    "ucd",
    "shn",
    "kser",
    "knum",
    "kopn",
    "mtbr",
    "cnap",
    "cnapn",
    "cnal",
    "cnaln"
})
public class WSMassEmployeeDraftGetOverlapsOutResultSetRow {

    protected String dfid;
    protected BigDecimal rno;
    protected String cus;
    protected String clc;
    protected String fnm1;
    protected String fnm2;
    protected String fnm3;
    protected String tab;
    protected XMLGregorianCalendar dtbr;
    protected String ucd;
    protected String shn;
    protected String kser;
    protected String knum;
    protected XMLGregorianCalendar kopn;
    protected String mtbr;
    protected String cnap;
    protected String cnapn;
    protected String cnal;
    protected String cnaln;

    /**
     * Gets the value of the dfid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDfid() {
        return dfid;
    }

    /**
     * Sets the value of the dfid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDfid(String value) {
        this.dfid = value;
    }

    /**
     * Gets the value of the rno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRno() {
        return rno;
    }

    /**
     * Sets the value of the rno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRno(BigDecimal value) {
        this.rno = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the fnm1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm1() {
        return fnm1;
    }

    /**
     * Sets the value of the fnm1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm1(String value) {
        this.fnm1 = value;
    }

    /**
     * Gets the value of the fnm2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm2() {
        return fnm2;
    }

    /**
     * Sets the value of the fnm2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm2(String value) {
        this.fnm2 = value;
    }

    /**
     * Gets the value of the fnm3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFnm3() {
        return fnm3;
    }

    /**
     * Sets the value of the fnm3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFnm3(String value) {
        this.fnm3 = value;
    }

    /**
     * Gets the value of the tab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTab() {
        return tab;
    }

    /**
     * Sets the value of the tab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTab(String value) {
        this.tab = value;
    }

    /**
     * Gets the value of the dtbr property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDtbr() {
        return dtbr;
    }

    /**
     * Sets the value of the dtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDtbr(XMLGregorianCalendar value) {
        this.dtbr = value;
    }

    /**
     * Gets the value of the ucd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcd() {
        return ucd;
    }

    /**
     * Sets the value of the ucd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcd(String value) {
        this.ucd = value;
    }

    /**
     * Gets the value of the shn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShn() {
        return shn;
    }

    /**
     * Sets the value of the shn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShn(String value) {
        this.shn = value;
    }

    /**
     * Gets the value of the kser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKser() {
        return kser;
    }

    /**
     * Sets the value of the kser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKser(String value) {
        this.kser = value;
    }

    /**
     * Gets the value of the knum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKnum() {
        return knum;
    }

    /**
     * Sets the value of the knum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKnum(String value) {
        this.knum = value;
    }

    /**
     * Gets the value of the kopn property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getKopn() {
        return kopn;
    }

    /**
     * Sets the value of the kopn property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setKopn(XMLGregorianCalendar value) {
        this.kopn = value;
    }

    /**
     * Gets the value of the mtbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMtbr() {
        return mtbr;
    }

    /**
     * Sets the value of the mtbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMtbr(String value) {
        this.mtbr = value;
    }

    /**
     * Gets the value of the cnap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnap() {
        return cnap;
    }

    /**
     * Sets the value of the cnap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnap(String value) {
        this.cnap = value;
    }

    /**
     * Gets the value of the cnapn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnapn() {
        return cnapn;
    }

    /**
     * Sets the value of the cnapn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnapn(String value) {
        this.cnapn = value;
    }

    /**
     * Gets the value of the cnal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnal() {
        return cnal;
    }

    /**
     * Sets the value of the cnal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnal(String value) {
        this.cnal = value;
    }

    /**
     * Gets the value of the cnaln property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnaln() {
        return cnaln;
    }

    /**
     * Sets the value of the cnaln property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnaln(String value) {
        this.cnaln = value;
    }

}
