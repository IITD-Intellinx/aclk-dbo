package ru.iitdgroup.wsmassemployeelist;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;
import ru.alfabank.ws.cs.eq.wsmassemployeedraft11.WSMassEmployeeDraft11Service;

public class ChangeEndPoint extends GXFunctionExtension {

    @Override
    public void invoke(String functionName, GXArguments Arguments, GXResult Result)
            throws Exception {
        // TODO Auto-generated method stub

        try {
            String URL = "";
            URL = Arguments.getString("URL");

            if (!URL.isEmpty()) {

                WSMassEmployeeDraft11.WSMED11Service = new WSMassEmployeeDraft11Service(new URL(URL), new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraft11Service"));
                WSMassEmployeeDraft11.WSMED11Port = WSMassEmployeeDraft11.WSMED11Service.getWSMassEmployeeDraft11Port();
                WSMassEmployeeDraft11.IsBegin = true;

                Result.setString("Connected");

            } else {
                Result.setString("URL is empty");
            }


        } catch (MalformedURLException e) {
            // TODO: handle exception
            Result.setString("MalformedURLException: " + e.getMessage());
        }
    }

}
