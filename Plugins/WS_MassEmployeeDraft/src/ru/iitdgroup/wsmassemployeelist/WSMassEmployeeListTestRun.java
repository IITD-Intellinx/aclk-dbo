package ru.iitdgroup.wsmassemployeelist;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.namespace.QName;

import ru.alfabank.ws.cs.eq.wsmassemployeedraft11.WSMassEmployeeDraft11Service;

public class WSMassEmployeeListTestRun {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        String  Client = "AC7IDJ",
                URL = "http://localhost:9200/CS/EQ/WSMassEmployeeDraft/WSMassEmployeeDraft11?WSDL",
                //"http://localhost:9200/CS/EQ/WSMassEmployeeDraft/WSMassEmployeeDraft11",
                USID = "USER111AGZ",
                ORID = "",
                userID = "WSIX", branchNumber = "0000",
                externalSystemCode = "INTLX", externalUserCode = "INTLX";

        long i = Long.MAX_VALUE + 1;
        double d = Double.MAX_VALUE + Long.MAX_VALUE;
        BigDecimal bd = new BigDecimal(Double.MAX_VALUE);
        bd = bd.add(new BigDecimal(Double.MAX_VALUE));

        System.out.println( "i=" + i + " max_val=" +Long.MAX_VALUE );
        System.out.println( "d=" + d + " max_val=" +Double.MAX_VALUE );
        System.out.println( "bd=" + bd + " \n\r max_val=" +Double.MAX_VALUE );

        try {

            WSMassEmployeeDraft11.WSMED11Service = new WSMassEmployeeDraft11Service(new URL(URL), new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraft11Service"));
            WSMassEmployeeDraft11.WSMED11Port = WSMassEmployeeDraft11.WSMED11Service.getWSMassEmployeeDraft11Port();

            String result = WSMassEmployeeDraft11Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, USID, ORID);
            System.out.println(result);

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    static boolean isAccountInvidual(String psAccount) {
        String AcMask = psAccount.substring(0, 5);
        String AcMask3 = psAccount.substring(0, 3);
        return "40817".equals(AcMask) || "42301".equals(AcMask) || "40803".equals(AcMask) || "40813".equals(AcMask)
                || "40818".equals(AcMask) || "40819".equals(AcMask) || "40820".equals(AcMask) || "30114".equals(AcMask)
                || "323".equals(AcMask3) || "40806".equals(AcMask) || "40809".equals(AcMask) || "40812".equals(AcMask)
                || "40814".equals(AcMask) || "40815".equals(AcMask) || "40821".equals(AcMask);
    }

    static boolean isAccountResident(String psAccount) {
        String AcMask = psAccount.substring(0, 5);
        String AcMask3 = psAccount.substring(0, 3);
        return "40803".equals(AcMask) || ("40804".equals(AcMask)) || ("40805".equals(AcMask)) || ("40806".equals(AcMask))
                || "40807".equals(AcMask) || ("40809".equals(AcMask)) || ("40812".equals(AcMask)) || ("40813".equals(AcMask))
                || "40814".equals(AcMask) || ("40815".equals(AcMask)) || ("40818".equals(AcMask)) || ("40819".equals(AcMask))
                || "40820".equals(AcMask) || ("40821".equals(AcMask)) || ("30111".equals(AcMask)) || ("30114".equals(AcMask))
                || "30230".equals(AcMask) || ("30231".equals(AcMask)) || ("30303".equals(AcMask)) || ("30304".equals(AcMask))
                || ("323".equals(AcMask3)) || ("425".equals(AcMask3));
    }
}
