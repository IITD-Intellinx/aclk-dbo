package ru.iitdgroup.wsmassemployeelist;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;

import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;

import ru.alfabank.ws.cs.eq.wsmassemployeedraft11.WSMassEmployeeDraft11PortType;
import ru.alfabank.ws.cs.eq.wsmassemployeedraft11.WSMassEmployeeDraft11Service;

public class WSMassEmployeeDraft11 extends GXFunctionExtension {


    public static WSMassEmployeeDraft11PortType WSMED11Port;
    public static WSMassEmployeeDraft11Service WSMED11Service;
    public static final String EXCEPTION = "Exception! ";
    public static volatile boolean IsBegin = false;

    @Override
	public void invoke(String functionName, GXArguments arguments, GXResult Result)
			throws Exception {
        String userID, branchNumber, externalSystemCode, externalUserCode;
        String strEQid, strOrid;
        String URL = arguments.getString("service_url");
        externalSystemCode = arguments.getString("externalSystemCode");
        externalUserCode = arguments.getString("externalUserCode");
        userID = arguments.getString("userID");
        branchNumber = arguments.getString("branchNumber");
        strEQid = arguments.getString("EQid");
        strOrid = arguments.getString("Orid");

        try {
        	if(!IsBegin) {
                WSMassEmployeeDraft11.WSMED11Service = new WSMassEmployeeDraft11Service(new URL(URL), new QName("http://WSMassEmployeeDraft11.EQ.CS.ws.alfabank.ru", "WSMassEmployeeDraft11Service"));
                WSMassEmployeeDraft11.WSMED11Port = WSMassEmployeeDraft11.WSMED11Service.getWSMassEmployeeDraft11Port();
                IsBegin = true;
            }

            String result = WSMassEmployeeDraft11Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, strEQid, strOrid);
            Result.setString(result);
        } catch(MalformedURLException e) {
        	Result.setString("MalformedURLException: " + e.getMessage());
        }
    }

    static String formatRequestParams(String userID, String branchNumber, String externalSystemCode, String externalUserCode) {
        return "<userID>"+userID+"</userID><branchNumber>"+branchNumber+"</branchNumber><externalSystemCode>"+externalSystemCode+"</externalSystemCode><externalUserCode>"+externalUserCode+"</externalUserCode>";
    }
}
