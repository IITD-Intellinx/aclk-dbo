package ru.iitdgroup.wsmassemployeelist;

import ru.alfabank.ws.cs.eq.wsmassemployeedraft11.*;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftListInParms;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftListOutParms;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftListOutResultSet;
import ru.alfabank.ws.cs.eq.wsmassemployeedraftinoutparms11.WSMassEmployeeDraftListOutResultSetRow;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class WSMassEmployeeDraft11Connector {

    private static boolean writeLog = false;

    public static String getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String EQid, String Orid) throws RemoteException {
        String retString = "";
        WSCommonParms commonParms = new WSCommonParms();
        commonParms.setUserID(userID);
        commonParms.setBranchNumber(branchNumber);
        commonParms.setExternalSystemCode(externalSystemCode);
        commonParms.setExternalUserCode(externalUserCode);
        WSMassEmployeeDraftListInParms inParms = new WSMassEmployeeDraftListInParms();

        try {
            inParms.setCus(EQid);
            if(Orid!=null && Orid.length()>0)
                inParms.setOrid(Orid);

            WSMassEmployeeDraftListResponseType e = WSMassEmployeeDraft11.WSMED11Port.wsMassEmployeeDraftList(commonParms, inParms);
            WSMassEmployeeDraftListOutParms outParms = e.getOutParms();
            WSMassEmployeeDraftListOutResultSet resultSet = outParms.getResultSet();
            List resultSetRowList = resultSet.getResultSetRow();
            if (!((WSMassEmployeeDraftListOutResultSetRow) resultSetRowList.get(0)).getUsid().equals("")) {
                retString = ((WSMassEmployeeDraftListOutResultSetRow) resultSetRowList.get(0)).getOrid();
            }
        } catch (MsgWSTechnicalException e) {
            retString = "MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAppTechnicalException e) {
            retString = "MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAppException e) {
            retString = "MsgWSAppException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAccessException e) {
            retString = "MsgWSAccessException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSVerificationException e) {
            retString = "MsgWSVerificationException: " + e.getFaultInfo().getErrorString();

        } catch (Exception e) {
            Calendar calNowDate = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            trace("***" + sdf.format(calNowDate.getTime()) + "*** Error in WSMassEmployeeDraft11>>>" + e.toString() + "\n\r<<<Request>>>\n\r"
                    + WSMassEmployeeDraft11.formatRequestParams(userID, branchNumber, externalSystemCode, externalUserCode) + "\n\r<<<Parametres>>>\n\r" + " USID:" + inParms.getUsid() + "\n\r " + " Orid:" + inParms.getOrid() + "\n\r");
            retString = WSMassEmployeeDraft11.EXCEPTION + "ErrorParams WSMassEmployeeDraft11 " + e.getMessage();
        }
        return retString;
    }

    public static void trace(String message) {
        if (writeLog) {
            try {
                FileWriter e = new FileWriter("wsmassemployeedraft11.log", true);
                BufferedWriter br = new BufferedWriter(e);
                br.write(message + "\n");
                br.close();
            } catch (IOException var3) {
                var3.printStackTrace();
            }
        }

    }
}
