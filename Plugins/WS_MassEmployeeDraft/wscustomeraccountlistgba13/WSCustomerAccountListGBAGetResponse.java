
package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSCustomerAccountListGBAGetResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerAccountListGBAGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru}WSCustomerAccountListGBAGetResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerAccountListGBAGetResponse", propOrder = {
    "response"
})
public class WSCustomerAccountListGBAGetResponse {

    @XmlElement(required = true)
    protected WSCustomerAccountListGBAGetResponseType response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link WSCustomerAccountListGBAGetResponseType }
     *     
     */
    public WSCustomerAccountListGBAGetResponseType getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCustomerAccountListGBAGetResponseType }
     *     
     */
    public void setResponse(WSCustomerAccountListGBAGetResponseType value) {
        this.response = value;
    }

}
