
package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSCustomerAccountListGBAGetResponse_QNAME = new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBAGetResponse");
    private final static QName _WSCustomerAccountListGBAGet_QNAME = new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBAGet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSCustomerAccountListGBAGetResponse }
     * 
     */
    public WSCustomerAccountListGBAGetResponse createWSCustomerAccountListGBAGetResponse() {
        return new WSCustomerAccountListGBAGetResponse();
    }

    /**
     * Create an instance of {@link WSCustomerAccountListGBAGetResponseType }
     * 
     */
    public WSCustomerAccountListGBAGetResponseType createWSCustomerAccountListGBAGetResponseType() {
        return new WSCustomerAccountListGBAGetResponseType();
    }

    /**
     * Create an instance of {@link WSCustomerAccountListGBAGet }
     * 
     */
    public WSCustomerAccountListGBAGet createWSCustomerAccountListGBAGet() {
        return new WSCustomerAccountListGBAGet();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerAccountListGBAGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", name = "WSCustomerAccountListGBAGetResponse")
    public JAXBElement<WSCustomerAccountListGBAGetResponse> createWSCustomerAccountListGBAGetResponse(WSCustomerAccountListGBAGetResponse value) {
        return new JAXBElement<WSCustomerAccountListGBAGetResponse>(_WSCustomerAccountListGBAGetResponse_QNAME, WSCustomerAccountListGBAGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerAccountListGBAGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", name = "WSCustomerAccountListGBAGet")
    public JAXBElement<WSCustomerAccountListGBAGet> createWSCustomerAccountListGBAGet(WSCustomerAccountListGBAGet value) {
        return new JAXBElement<WSCustomerAccountListGBAGet>(_WSCustomerAccountListGBAGet_QNAME, WSCustomerAccountListGBAGet.class, null, value);
    }

}
