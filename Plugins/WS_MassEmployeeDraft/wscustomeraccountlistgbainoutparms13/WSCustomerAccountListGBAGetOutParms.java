
package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSCustomerAccountListGBAGetOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerAccountListGBAGetOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="cnt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="resultSet" type="{http://WSCustomerAccountListGBAInOutParms13.EQ.CS.ws.alfabank.ru}WSCustomerAccountListGBAGetOutResultSet"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerAccountListGBAGetOutParms", propOrder = {
    "cus",
    "clc",
    "cnt",
    "resultSet"
})
public class WSCustomerAccountListGBAGetOutParms {

    protected String cus;
    protected String clc;
    protected String cnt;
    @XmlElement(required = true)
    protected WSCustomerAccountListGBAGetOutResultSet resultSet;

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the cnt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnt() {
        return cnt;
    }

    /**
     * Sets the value of the cnt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnt(String value) {
        this.cnt = value;
    }

    /**
     * Gets the value of the resultSet property.
     * 
     * @return
     *     possible object is
     *     {@link WSCustomerAccountListGBAGetOutResultSet }
     *     
     */
    public WSCustomerAccountListGBAGetOutResultSet getResultSet() {
        return resultSet;
    }

    /**
     * Sets the value of the resultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCustomerAccountListGBAGetOutResultSet }
     *     
     */
    public void setResultSet(WSCustomerAccountListGBAGetOutResultSet value) {
        this.resultSet = value;
    }

}
