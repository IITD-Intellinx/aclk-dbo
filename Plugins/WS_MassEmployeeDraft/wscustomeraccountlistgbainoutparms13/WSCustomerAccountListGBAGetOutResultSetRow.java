
package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSCustomerAccountListGBAGetOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerAccountListGBAGetOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ean" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ccy" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ama" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="brg" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="act" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="pwd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal50" minOccurs="0"/>
 *         &lt;element name="prd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="exp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="toe" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="zzz" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="p5p" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ab" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="an" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="as" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ai12" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai11" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai14" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai17" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai20" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai30" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai47" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai82" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="p3r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="gh" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="trz" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="ccym" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ccyd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="actd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="p3rd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="acsn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar15" minOccurs="0"/>
 *         &lt;element name="acln" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="total" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="holds" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal150" minOccurs="0"/>
 *         &lt;element name="oad" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="ai83" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ai87" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="acnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar70" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerAccountListGBAGetOutResultSetRow", propOrder = {
    "ean",
    "ccy",
    "ama",
    "brg",
    "act",
    "pwd",
    "prd",
    "exp",
    "sts",
    "toe",
    "zzz",
    "p5P",
    "ab",
    "an",
    "as",
    "ai12",
    "ai11",
    "ai14",
    "ai17",
    "ai20",
    "ai30",
    "ai47",
    "ai82",
    "p3R",
    "gh",
    "trz",
    "ccym",
    "ccyd",
    "actd",
    "p3Rd",
    "acsn",
    "acln",
    "total",
    "holds",
    "oad",
    "ai83",
    "ai87",
    "acnm"
})
public class WSCustomerAccountListGBAGetOutResultSetRow {

    protected String ean;
    protected String ccy;
    protected BigDecimal ama;
    protected String brg;
    protected String act;
    protected BigDecimal pwd;
    protected String prd;
    protected String exp;
    protected String sts;
    protected String toe;
    protected String zzz;
    @XmlElement(name = "p5p")
    protected String p5P;
    protected String ab;
    protected String an;
    protected String as;
    protected String ai12;
    protected String ai11;
    protected String ai14;
    protected String ai17;
    protected String ai20;
    protected String ai30;
    protected String ai47;
    protected String ai82;
    @XmlElement(name = "p3r")
    protected String p3R;
    protected String gh;
    protected String trz;
    protected String ccym;
    protected String ccyd;
    protected String actd;
    @XmlElement(name = "p3rd")
    protected String p3Rd;
    protected String acsn;
    protected String acln;
    protected BigDecimal total;
    protected BigDecimal holds;
    protected XMLGregorianCalendar oad;
    protected String ai83;
    protected String ai87;
    protected String acnm;

    /**
     * Gets the value of the ean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEan() {
        return ean;
    }

    /**
     * Sets the value of the ean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEan(String value) {
        this.ean = value;
    }

    /**
     * Gets the value of the ccy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcy() {
        return ccy;
    }

    /**
     * Sets the value of the ccy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcy(String value) {
        this.ccy = value;
    }

    /**
     * Gets the value of the ama property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAma() {
        return ama;
    }

    /**
     * Sets the value of the ama property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAma(BigDecimal value) {
        this.ama = value;
    }

    /**
     * Gets the value of the brg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrg() {
        return brg;
    }

    /**
     * Sets the value of the brg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrg(String value) {
        this.brg = value;
    }

    /**
     * Gets the value of the act property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAct() {
        return act;
    }

    /**
     * Sets the value of the act property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAct(String value) {
        this.act = value;
    }

    /**
     * Gets the value of the pwd property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPwd() {
        return pwd;
    }

    /**
     * Sets the value of the pwd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPwd(BigDecimal value) {
        this.pwd = value;
    }

    /**
     * Gets the value of the prd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrd() {
        return prd;
    }

    /**
     * Sets the value of the prd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrd(String value) {
        this.prd = value;
    }

    /**
     * Gets the value of the exp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExp() {
        return exp;
    }

    /**
     * Sets the value of the exp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExp(String value) {
        this.exp = value;
    }

    /**
     * Gets the value of the sts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSts() {
        return sts;
    }

    /**
     * Sets the value of the sts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSts(String value) {
        this.sts = value;
    }

    /**
     * Gets the value of the toe property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToe() {
        return toe;
    }

    /**
     * Sets the value of the toe property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToe(String value) {
        this.toe = value;
    }

    /**
     * Gets the value of the zzz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZzz() {
        return zzz;
    }

    /**
     * Sets the value of the zzz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZzz(String value) {
        this.zzz = value;
    }

    /**
     * Gets the value of the p5P property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5P() {
        return p5P;
    }

    /**
     * Sets the value of the p5P property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5P(String value) {
        this.p5P = value;
    }

    /**
     * Gets the value of the ab property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAb() {
        return ab;
    }

    /**
     * Sets the value of the ab property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAb(String value) {
        this.ab = value;
    }

    /**
     * Gets the value of the an property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAn() {
        return an;
    }

    /**
     * Sets the value of the an property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAn(String value) {
        this.an = value;
    }

    /**
     * Gets the value of the as property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAs() {
        return as;
    }

    /**
     * Sets the value of the as property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAs(String value) {
        this.as = value;
    }

    /**
     * Gets the value of the ai12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi12() {
        return ai12;
    }

    /**
     * Sets the value of the ai12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi12(String value) {
        this.ai12 = value;
    }

    /**
     * Gets the value of the ai11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi11() {
        return ai11;
    }

    /**
     * Sets the value of the ai11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi11(String value) {
        this.ai11 = value;
    }

    /**
     * Gets the value of the ai14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi14() {
        return ai14;
    }

    /**
     * Sets the value of the ai14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi14(String value) {
        this.ai14 = value;
    }

    /**
     * Gets the value of the ai17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi17() {
        return ai17;
    }

    /**
     * Sets the value of the ai17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi17(String value) {
        this.ai17 = value;
    }

    /**
     * Gets the value of the ai20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi20() {
        return ai20;
    }

    /**
     * Sets the value of the ai20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi20(String value) {
        this.ai20 = value;
    }

    /**
     * Gets the value of the ai30 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi30() {
        return ai30;
    }

    /**
     * Sets the value of the ai30 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi30(String value) {
        this.ai30 = value;
    }

    /**
     * Gets the value of the ai47 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi47() {
        return ai47;
    }

    /**
     * Sets the value of the ai47 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi47(String value) {
        this.ai47 = value;
    }

    /**
     * Gets the value of the ai82 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi82() {
        return ai82;
    }

    /**
     * Sets the value of the ai82 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi82(String value) {
        this.ai82 = value;
    }

    /**
     * Gets the value of the p3R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3R() {
        return p3R;
    }

    /**
     * Sets the value of the p3R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3R(String value) {
        this.p3R = value;
    }

    /**
     * Gets the value of the gh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGh() {
        return gh;
    }

    /**
     * Sets the value of the gh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGh(String value) {
        this.gh = value;
    }

    /**
     * Gets the value of the trz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrz() {
        return trz;
    }

    /**
     * Sets the value of the trz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrz(String value) {
        this.trz = value;
    }

    /**
     * Gets the value of the ccym property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcym() {
        return ccym;
    }

    /**
     * Sets the value of the ccym property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcym(String value) {
        this.ccym = value;
    }

    /**
     * Gets the value of the ccyd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCcyd() {
        return ccyd;
    }

    /**
     * Sets the value of the ccyd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCcyd(String value) {
        this.ccyd = value;
    }

    /**
     * Gets the value of the actd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActd() {
        return actd;
    }

    /**
     * Sets the value of the actd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActd(String value) {
        this.actd = value;
    }

    /**
     * Gets the value of the p3Rd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3Rd() {
        return p3Rd;
    }

    /**
     * Sets the value of the p3Rd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3Rd(String value) {
        this.p3Rd = value;
    }

    /**
     * Gets the value of the acsn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcsn() {
        return acsn;
    }

    /**
     * Sets the value of the acsn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcsn(String value) {
        this.acsn = value;
    }

    /**
     * Gets the value of the acln property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcln() {
        return acln;
    }

    /**
     * Sets the value of the acln property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcln(String value) {
        this.acln = value;
    }

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotal(BigDecimal value) {
        this.total = value;
    }

    /**
     * Gets the value of the holds property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHolds() {
        return holds;
    }

    /**
     * Sets the value of the holds property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHolds(BigDecimal value) {
        this.holds = value;
    }

    /**
     * Gets the value of the oad property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOad() {
        return oad;
    }

    /**
     * Sets the value of the oad property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOad(XMLGregorianCalendar value) {
        this.oad = value;
    }

    /**
     * Gets the value of the ai83 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi83() {
        return ai83;
    }

    /**
     * Sets the value of the ai83 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi83(String value) {
        this.ai83 = value;
    }

    /**
     * Gets the value of the ai87 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAi87() {
        return ai87;
    }

    /**
     * Sets the value of the ai87 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAi87(String value) {
        this.ai87 = value;
    }

    /**
     * Gets the value of the acnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcnm() {
        return acnm;
    }

    /**
     * Sets the value of the acnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcnm(String value) {
        this.acnm = value;
    }

}
