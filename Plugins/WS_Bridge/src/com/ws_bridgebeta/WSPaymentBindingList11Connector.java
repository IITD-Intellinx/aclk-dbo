package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.*;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetInParms;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutResultSetRow;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

import java.rmi.RemoteException;
import java.util.List;

public class WSPaymentBindingList11Connector {

    public static List<WSPaymentBindingListGetOutResultSetRow> getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String pCus, String pSts) throws RemoteException, MsgWSTechnicalException, MsgWSAppTechnicalException, MsgWSAppException, MsgWSAccessException, MsgWSVerificationException {
        WSCommonParms commonParams = new WSCommonParms();
        commonParams.setUserID(userID);
        commonParams.setBranchNumber(branchNumber);
        commonParams.setExternalSystemCode(externalSystemCode);
        commonParams.setExternalUserCode(externalUserCode);
        WSPaymentBindingListGetInParms inParams = new WSPaymentBindingListGetInParms();
        inParams.setCus(pCus);
        inParams.setSts(pSts);
        return WebServiceBridge
                .WSPBL11Port
                .wsPaymentBindingListGet(commonParams, inParams)
                .getOutParms()
                .getResultSet()
                .getResultSetRow();
    }

    public static String GetPurses(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String Cus, String Sts) //throws Exception
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            List<WSPaymentBindingListGetOutResultSetRow> bindedPurses = getResponse(userID, branchNumber, externalSystemCode, externalUserCode, Cus, Sts);
            for(WSPaymentBindingListGetOutResultSetRow purse : bindedPurses)
                 sb.append(purse.getWid()).append(";");
            return sb.toString();
        }
        catch (MsgWSTechnicalException e) {
            return sb.append("MsgWSTechnicalException: ").append(e.getFaultInfo().getErrorString()).toString();
        } catch (MsgWSAppTechnicalException e) {
            return sb.append("MsgWSAppTechnicalException: ").append(e.getFaultInfo().getErrorString()).toString();
        } catch (MsgWSAppException e) {
            return sb.append("MsgWSAppException: ").append(e.getFaultInfo().getErrorString()).toString();
        } catch (MsgWSAccessException e) {
            return sb.append("MsgWSAccessException").append(e.getFaultInfo().getErrorString()).toString();
        } catch (MsgWSVerificationException e) {
            return sb.append("MsgWSVerificationException: ").append(e.getFaultInfo().getErrorString()).toString();
        } catch (RemoteException e) {
           return sb.append("RemoteException: ").append(e.getMessage()).toString();
        } catch (Exception e) {
            return sb.append("Exception: ").append(e.getMessage()).toString();
        }
    }
}