package com.ws_bridgebeta;

import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import javax.xml.namespace.QName;

import com.sun.xml.internal.ws.wsdl.parser.InaccessibleWSDLException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPayment10Service;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBA13Service;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfo12.WSCustomerBaseInfo12Service;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCL10Service;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingList11Service;

public class TestRunner {

    private static final String wscaName = "WSAccountClickPayment10";
    private static final String wspbName = "WSCustomerBaseInfo12";
    private static final String wspcName = "WSPaymentBindingList11";
    //private static String wscaRequest = "<root><inCommonParms><userID>IBSR</userID><branchNumber>0000</branchNumber><externalSystemCode>GRCHK28</externalSystemCode><externalUserCode>WSTP</externalUserCode></inCommonParms></root>";


    public static void main(String[] args) {
        try {
            String e = "C010706120000003";
            String Urls = " http://vuws3:9081/CS/EQ/WSAccountClickPayment/WSAccountClickPayment10;http://vuws1:9080/CS/EQ/WSCustomerBaseInfo/WSCustomerBaseInfo12;http://vuws1:9080/CS/EQ/WSCustomerExtendedInfoCL/WSCustomerExtendedInfoCL10;http://vuws1:9080/CS/EQ/WSPaymentBindingList/WSPaymentBindingList11;http://vuws1:9080/CS/EQ/WSCustomerAccountListGBA/WSCustomerAccountListGBA13";
            Urls = " http://vuws3:9081/CS/EQ/WSAccountClickPayment/WSAccountClickPayment10;http://localhost:9080/CS/EQ/WSCustomerBaseInfo/WSCustomerBaseInfo12?WSDL;http://vuws1:9080/CS/EQ/WSCustomerExtendedInfoCL/WSCustomerExtendedInfoCL10;http://vuws1:9080/CS/EQ/WSPaymentBindingList/WSPaymentBindingList11;http://vuws1:9080/CS/EQ/WSCustomerAccountListGBA/WSCustomerAccountListGBA13";
          /*  Urls = " http://localhost:9081/CS/EQ/WSAccountClickPayment/WSAccountClickPayment10;"+
                    "http://localhost:9080/CS/EQ/WSCustomerBaseInfo/WSCustomerBaseInfo12?WSDL;"+
                    "http://localhost:9080/CS/EQ/WSCustomerExtendedInfoCL/WSCustomerExtendedInfoCL10;"+
                    "http://localhost:9080/CS/EQ/WSPaymentBindingList/WSPaymentBindingList11?WSDL;http://localhost:9080/CS/EQ/WSCustomerAccountListGBA/WSCustomerAccountListGBA13?WSDL";     */
            Urls="http://titova-cmp:9091/CS/EQ/WSCustomerAccountListGBA/WSCustomerAccountListGBA13?WSDL";

            String userID  = "WSIB", branchNumber = "0000",
                    externalSystemCode = "INTLX", externalUserCode = "INTLX";
            //userID="IBSR";branchNumber="0000";externalSystemCode="GRCHK28";externalUserCode="WSTP";
            String Cus = "AIZJAR";
            String DestCus="11111";
            String Purse = "1111";
            String Account = "1111";
            String Sts = "";
            String res = "";
            String sql="NEEAN=40817810107220018322";
            String grp="";
            String lnm="";
            String channelId="";
            String typ="";

            TestInv(Urls, userID, branchNumber,externalSystemCode,externalUserCode, "", e, Cus,DestCus, Sts, Account, Purse, res,sql,grp,typ,lnm,channelId);
            //TestInv(Urls, userID, branchNumber,externalSystemCode,externalUserCode, "WSPaymentBindingList11", e, Cus, Sts, Account, Purse, res);
        } catch (Exception var9) {
            var9.printStackTrace();
        }

    }

    public static void TestInv(String ServiceUrl, String userID, String branchNumber, String externalSystemCode, String externalUserCode, String serviceName, String payRef, String Cus,String destCus, String pSts, String pAccount, String pPurse, String result, String pSql, String pGRP, String pTyp,String pLnm,String pChannelId) throws Exception {
        String resString = "";

        try {
            String[] ex;
            ex = ServiceUrl.split(";");
            if (!WebServiceBridge.IsBegin) {
                if (serviceName.equals("WSCustomerBaseInfo12")) {
                    WebServiceBridge.WSCBI12Service = new WSCustomerBaseInfo12Service(new URL(ex[1]), new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfo12Service"));
                    WebServiceBridge.WSCBI12Port = WebServiceBridge.WSCBI12Service.getWSCustomerBaseInfo12Port();
                } else
                    if (serviceName.equals("WSPaymentBindingList11")) {
                        WebServiceBridge.WSPBL11Service = new WSPaymentBindingList11Service(new URL(ex[3]), new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Service"));
                        WebServiceBridge.WSPBL11Port = WebServiceBridge.WSPBL11Service.getWSPaymentBindingList11Port();
                    } else {
                  /*  WebServiceBridge.WSACP10Service = new WSAccountClickPayment10Service(new URL(ex[0]), new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Service"));
                    WebServiceBridge.WSACP10Port = WebServiceBridge.WSACP10Service.getWSAccountClickPayment10Port();
                    WebServiceBridge.WSCBI12Service = new WSCustomerBaseInfo12Service(new URL(ex[1]), new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfo12Service"));
                    WebServiceBridge.WSCBI12Port = WebServiceBridge.WSCBI12Service.getWSCustomerBaseInfo12Port();
                    WebServiceBridge.WSCEI10Service = new WSCustomerExtendedInfoCL10Service(new URL(ex[2]), new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Service"));
                    WebServiceBridge.WSCEI10Port = WebServiceBridge.WSCEI10Service.getWSCustomerExtendedInfoCL10Port();
                    WebServiceBridge.WSPBL11Service = new WSPaymentBindingList11Service(new URL(ex[3]), new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Service"));
                    WebServiceBridge.WSPBL11Port = WebServiceBridge.WSPBL11Service.getWSPaymentBindingList11Port();       */
                    WebServiceBridge.WSCALGBA13Service = new WSCustomerAccountListGBA13Service(new URL(ex[0]), new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Service"));
                    WebServiceBridge.WSCALGBA13Port = WebServiceBridge.WSCALGBA13Service.getWSCustomerAccountListGBA13Port();
                    System.out.println(WSBindingListGBA13Connector.getResponse(userID ,branchNumber,externalSystemCode,externalUserCode, Cus, pChannelId, pGRP, pSql, pLnm));
                }
            }

            if (serviceName.equals("WSPaymentBindingList11")) {
                new OuterType();
                new WSPaymentBindingList11Connector();
                resString = WSPaymentBindingList11Connector.GetPurses(userID, branchNumber,externalSystemCode,externalUserCode, Cus, pSts);
            } else
            if (serviceName.equals("WSCustomerBaseInfo12")) {
                resString = WSCustomerBaseInfo12Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, Cus, destCus);
            } else {
                OuterType Out4EQID = new OuterType();
                Calendar calStartFullAccPay10 = Calendar.getInstance();
                resString = resString + WSAccountClickPayment10Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, Out4EQID, payRef) + ";";
                Calendar calEndFullAccPay10 = Calendar.getInstance();
                long difInmSecondsFullAcPay10 = calEndFullAccPay10.getTimeInMillis() - calStartFullAccPay10.getTimeInMillis();
                if (!Cus.contentEquals("")) {
                    Out4EQID.m_strCUS[0] = Cus;
                }

               /* Calendar calStartFullBaseInfo = Calendar.getInstance();
                resString = resString + WSCustomerBaseInfo12Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, Out4EQID.m_strCUS[0],Out4EQID.m_strCUSDest[0]) + ";";
                Calendar calEndFullBaseInfo = Calendar.getInstance();
                long difInmSecondsFullBaseInfoPay10 = calEndFullBaseInfo.getTimeInMillis() - calStartFullBaseInfo.getTimeInMillis();
                Calendar calStartFullExternal = Calendar.getInstance();
                resString = resString + WSCustomerExtendedInfoCL10.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, Out4EQID.m_strCUS[0]);
                Calendar calEndFullExternal = Calendar.getInstance();               */

            }
        } catch (InaccessibleWSDLException var26) {
            result = "Error " + Arrays.toString(var26.getStackTrace()) + var26.getMessage();
            var26.printStackTrace();
            System.out.println(result);
            return;
        }catch (Exception exc) {
            exc.printStackTrace();
            return;
        }

        System.out.println(resString);
    }
}
