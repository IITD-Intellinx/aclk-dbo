package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.*;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetInParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSet;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSetRow;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;
import java.util.List;

public class WSBindingListGBA13Connector
{
    static final String WS_NAME = "WSBindingListGBA13 ";
    public static String getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String Cus, String Ext, String Grp, String SQL, String Lnm)
    {

            try
            {
                WSCommonParms commonParms = new WSCommonParms();
                commonParms.setUserID(userID);
                commonParms.setBranchNumber(branchNumber);
                commonParms.setExternalSystemCode(externalSystemCode);
                commonParms.setExternalUserCode(externalUserCode);
                WSCustomerAccountListGBAGetInParms inParms = new WSCustomerAccountListGBAGetInParms();
                    inParms.setCus(Cus);
                    inParms.setSql(SQL);
                    WSCustomerAccountListGBAGetResponseType e = WebServiceBridge.WSCALGBA13Port.wsCustomerAccountListGBAGet(commonParms, inParms);
                    WSCustomerAccountListGBAGetOutParms calNowDate1 = e.getOutParms();
                    WSCustomerAccountListGBAGetOutResultSet sdf1 = calNowDate1.getResultSet();
                    List resultSetRowList = sdf1.getResultSetRow();
                    if(Integer.valueOf(calNowDate1.getCnt())<=0)
                        return "";
                    if (!((WSCustomerAccountListGBAGetOutResultSetRow) resultSetRowList.get(0)).getEan().equals(""))
                    {
                        Double AccountRem = ((WSCustomerAccountListGBAGetOutResultSetRow) resultSetRowList.get(0)).getAma().doubleValue() / ((WSCustomerAccountListGBAGetOutResultSetRow) resultSetRowList.get(0)).getPwd().doubleValue();
                        return AccountRem.toString();
                    }
                    else return "";
            }
            catch (MsgWSTechnicalException e)
            {
                return WS_NAME+"MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppTechnicalException e)
            {
                return WS_NAME+"MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppException e)
            {
                return WS_NAME+"MsgWSAppException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAccessException e)
            {
                return WS_NAME+"MsgWSAccessException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSVerificationException e)
            {
                return WS_NAME+"MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
            }
            catch (Exception e)
            {
                return WS_NAME+"Exception: " + e.getMessage();
            }


     }

}
