package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wscustomerbaseinfo12.*;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfoinoutparms12.WSCustomerBaseInfoGetInParms;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfoinoutparms12.WSCustomerBaseInfoGetOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;
import ru.alfabank.ws.cs.wscommontypes10.WSTypeAddInfo;
import ru.alfabank.ws.cs.wscommontypes10.WSTypeAddInfoAlphanumeric;
import ru.alfabank.ws.cs.wscommontypes10.WSTypeAddInfoDate;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WSCustomerBaseInfo12Connector {

    static final String WS_NAME = "WSCustomerBaseInfo12 ";
    public static String getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String pCUS, String pCusDest) throws RemoteException, MalformedURLException {
        try {
            WSCommonParms commonParms = new WSCommonParms();
            commonParms.setUserID(userID);
            commonParms.setBranchNumber(branchNumber);
            commonParms.setExternalSystemCode(externalSystemCode);
            commonParms.setExternalUserCode(externalUserCode);
            WSCustomerBaseInfoGetInParms inParms = new WSCustomerBaseInfoGetInParms();

            String ResultString = "";
            try {
                inParms.setCus(pCUS);
                WSCustomerBaseInfoGetResponseType e = WebServiceBridge.WSCBI12Port.wsCustomerBaseInfoGet(commonParms, inParms);
                WSCustomerBaseInfoGetResponseType respDest = null;
                if(!pCusDest.isEmpty())
                {
                    inParms.setCus(pCusDest);
                    respDest=WebServiceBridge.WSCBI12Port.wsCustomerBaseInfoGet(commonParms,inParms);
                }

                WSCustomerBaseInfoGetOutParms outParms = e.getOutParms();

                WSTypeAddInfoAlphanumeric addInfoAlphanumeric = (WSTypeAddInfoAlphanumeric) outParms.getAdditionalInfo().getList().get(0);
                if (addInfoAlphanumeric != null) {
                    ResultString = ResultString + "FNM1==" + addInfoAlphanumeric.getAdt().trim() + ";";
                } else {
                    ResultString = ResultString + ";FNM1==;";
                }

                addInfoAlphanumeric = (WSTypeAddInfoAlphanumeric) outParms.getAdditionalInfo().getList().get(1);
                if (addInfoAlphanumeric != null) {
                    ResultString = ResultString + "FNM2==" + addInfoAlphanumeric.getAdt().trim() + ";";
                } else {
                    ResultString = ResultString + ";FNM2==;";
                }

                addInfoAlphanumeric = (WSTypeAddInfoAlphanumeric) outParms.getAdditionalInfo().getList().get(2);
                if (addInfoAlphanumeric != null) {
                    ResultString = ResultString + "FNM3==" + addInfoAlphanumeric.getAdt().trim() + ";";
                } else {
                    ResultString = ResultString + ";FNM3==;";
                }
                if(respDest!=null)
                {

                    String tarriffPlan = respDest.getOutParms().getP3R();
                    if (tarriffPlan != null)
                        ResultString = ResultString + "P3R==" + tarriffPlan.trim() + ";";
                    else
                        ResultString = ResultString + ";P3R==;";
                    String dateOfRegistration = "";
                    List<WSTypeAddInfo> addInfoList =  respDest.getOutParms().getAdditionalInfo().getList();
                    for (WSTypeAddInfo addInfo : addInfoList)
                    {
                        if (addInfo.getAnm().equalsIgnoreCase("DACC"))
                        {
                            Format format = new SimpleDateFormat("yyyy-MM-dd");
                            WSTypeAddInfoDate date = (WSTypeAddInfoDate) addInfo;
                            XMLGregorianCalendar xmlGregorianCalendar = date.getAdt();
                            Date normalDate = xmlGregorianCalendar.toGregorianCalendar().getTime();
                            dateOfRegistration = format.format(normalDate);
                        }
                    }
                    if (dateOfRegistration != null)
                        ResultString = ResultString + "DACC==" + dateOfRegistration.trim();
                    else
                        ResultString = ResultString + ";DACC==";

                }
                else
                    ResultString+="P3R==;DACC==";



            } catch (MsgWSTechnicalException e) {
                ResultString = WS_NAME+"MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
            } catch (MsgWSAppTechnicalException e) {
                ResultString = WS_NAME+"MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
            } catch (MsgWSAppException e) {
                ResultString = WS_NAME+"MsgWSAppException: " + e.getFaultInfo().getErrorString();
            } catch (MsgWSAccessException e) {
                ResultString = WS_NAME+"MsgWSAccessException: " + e.getFaultInfo().getErrorString();
            } catch (MsgWSVerificationException e) {
                ResultString = WS_NAME+"MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
            } catch (Exception e) {
                ResultString = WS_NAME+"Exception: " + e.getMessage();
            }
            return ResultString;
        } catch (Exception var11) {
            return "ErrorConnect WSCustomerBaseInfo12 " + var11.getMessage();
        }
    }

    public static String getCustomerBaseInfo(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String pCUS) throws RemoteException, MalformedURLException {
        try {
        String ResultString = "";
        WSCommonParms commonParms = new WSCommonParms();
            commonParms.setUserID(userID);
            commonParms.setBranchNumber(branchNumber);
            commonParms.setExternalSystemCode(externalSystemCode);
            commonParms.setExternalUserCode(externalUserCode);

            WSCustomerBaseInfoGetInParms inParms = new WSCustomerBaseInfoGetInParms();
//            try {
                inParms.setCus(pCUS);
        WSCustomerBaseInfoGetResponseType response;
        try {
            response = WebServiceBridge.WSCBI12Port.wsCustomerBaseInfoGet(commonParms, inParms);
        WSCustomerBaseInfoGetOutParms outParms = response.getOutParms();

                String p3R = outParms.getP3R();
                XMLGregorianCalendar cod = outParms.getCod();
                if (p3R != null) {
                    ResultString = ResultString + p3R + ";";
                } else {
                    ResultString = ResultString + ";";
                }

                if (cod != null) {
                    ResultString = ResultString + cod.toString();
                }

        } catch (MsgWSTechnicalException e) {
            ResultString = WS_NAME+"MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAppTechnicalException e) {
            ResultString = WS_NAME+"MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAppException e) {
            ResultString = WS_NAME+"MsgWSAppException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSAccessException e) {
            ResultString = WS_NAME+"MsgWSAccessException: " + e.getFaultInfo().getErrorString();
        } catch (MsgWSVerificationException e) {
            ResultString = WS_NAME+"MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
        } catch (Exception e) {
            ResultString = WS_NAME+"Exception: " + e.getMessage();
        }
                return ResultString;
//            }
// catch (Exception var10) {
//                Calendar calNowDate = Calendar.getInstance();
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                WebServiceBridge.trace("***" + sdf.format(calNowDate.getTime()) + "*** Error in WSCustomerBaseInfo12>>>" + var10.toString() + "\n\r<<<Request>>>\n\r"
//                        + WebServiceBridge.formatRequestParams(userID, branchNumber, externalSystemCode, externalUserCode) + "\n\r<<<Parametres>>>\n\r" + "CUS:" + inParms.getCus() + "\n\r");
//                return "ErrorParams WSCustomerBaseInfo12 " + var10.getMessage() + var10.getClass()+ " ";
//            }
        } catch (Exception var11) {
            return "ErrorConnect WSCustomerBaseInfo12 " + var11.getMessage();
        }
    }
}
