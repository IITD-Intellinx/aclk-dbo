package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.*;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.*;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.List;

public class WSCustomerExtendedInfoCL10 {

    static final String WS_NAME = "WSCustomerExtendedInfoCL10 ";
    public static String getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String pCUS) throws RemoteException, MalformedURLException {
        try {
            WSCommonParms commonParms = new WSCommonParms();
            commonParms.setUserID(userID);
            commonParms.setBranchNumber(branchNumber);
            commonParms.setExternalSystemCode(externalSystemCode);
            commonParms.setExternalUserCode(externalUserCode);
            WSCustomerExtendedInfoCLGetInParms inParms = new WSCustomerExtendedInfoCLGetInParms();

            String ResultString = "";
            StringBuilder sbOut= new StringBuilder();
            try
            {
                inParms.setCus(pCUS);
                List resInfo = WebServiceBridge.WSCEI10Port.wsCustomerExtendedInfoCLGet(commonParms, inParms).getOutParms().getResultSet().getResultSetRow();
                String strDatePhoneCh = "";
                String strTimePhoneCh = "";

                for (Object aResInfo : resInfo) {
                    WSTypeAdditionalInfo ItemRow = (WSTypeAdditionalInfo) aResInfo;
                    if (ItemRow.getAnm().trim().contains("PHCL")) {
                        WSTypeAdditionalInfoAlphanumeric resNumericTime = (WSTypeAdditionalInfoAlphanumeric) ItemRow;
                        if (resNumericTime.getAdt() != null)
                            sbOut.append("PHCL==").append(resNumericTime.getAdt()).append(";");
                        else
                            sbOut.append("PHCL==;");
                    }

                    if (ItemRow.getAnm().trim().contains("DACL")) {
                        WSTypeAdditionalInfoDate typeAdInfoDate = (WSTypeAdditionalInfoDate) ItemRow;
                        if (typeAdInfoDate.getAdt() != null)
                            strDatePhoneCh = typeAdInfoDate.getAdt().toString();
                    }

                    if (ItemRow.getAnm().trim().contains("TMCL"))
                    {
                        WSTypeAdditionalInfoNumeric typeAddInfoNum = (WSTypeAdditionalInfoNumeric) ItemRow;
                        if (typeAddInfoNum.getAdt() != null) {
                            String tmpNumericTime = typeAddInfoNum.getAdt().toString();
                            if (tmpNumericTime.length() < 6)
                            {
                                int tmpTimeChars = 6 - tmpNumericTime.length();
                                String strTimePrefix = "";
                                for (int ii = 0; ii < tmpTimeChars; ++ii)
                                    strTimePrefix = strTimePrefix + "0";
                                tmpNumericTime = strTimePrefix + tmpNumericTime;
                            }

                            char[] tmpCharArray = tmpNumericTime.toCharArray();
                            strTimePhoneCh = Character.toString(tmpCharArray[0]) + Character.toString(tmpCharArray[1]) + ":" + Character.toString(tmpCharArray[2]) + Character.toString(tmpCharArray[3]) + ":" + Character.toString(tmpCharArray[4]) + Character.toString(tmpCharArray[5]);
                        }
                    }
                }

                if (!strTimePhoneCh.contentEquals("") && !strDatePhoneCh.contentEquals(""))
                    sbOut.append("DTACL==").append(strDatePhoneCh).append(" ").append(strTimePhoneCh);

                else
                    sbOut.append(ResultString).append("DTACL==");

                return sbOut.toString();
            }
            catch (MsgWSTechnicalException e)
            {
                ResultString = WS_NAME+"MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppTechnicalException e)
            {
                ResultString = WS_NAME+"MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppException e)
            {
                ResultString = WS_NAME+"MsgWSAppException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAccessException e)
            {
                ResultString = WS_NAME+"MsgWSAccessException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSVerificationException e)
            {
                ResultString = WS_NAME+"MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
            }
            catch (Exception e)
            {
                ResultString = WS_NAME+"Exception: " + e.getMessage();
            }

            return ResultString;
        } catch (Exception var21) {
            var21.printStackTrace();
            return "ErrorConnect WSCustomerExtendedInfoCL10";
        }
    }
}
