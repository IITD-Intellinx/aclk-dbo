package com.ws_bridgebeta;

import com.intellinx.flow.*;
import com.intellinx.flow.bom.BomField;
import com.intellinx.flow.*;
import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPayment10PortType;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPayment10Service;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBA13PortType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBA13Service;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfo12.WSCustomerBaseInfo12PortType;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfo12.WSCustomerBaseInfo12Service;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCL10PortType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCL10Service;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingList11PortType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingList11Service;

import javax.xml.namespace.QName;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

public class WebServiceBridge extends GXFunctionExtension {

    private static final String LOG_PATH = "ws_bridge.log";
    private static final String wscaName = "WSAccountClickPayment10";
    private static final String wspbName = "WSCustomerBaseInfo12";
    private static final String wspcName = "WSPaymentBindingList11";
    private static final String wspdName = "WSAccountListGBA13";

    private static boolean writeLog = true;
    public static volatile boolean IsBegin = false;
    public static WSAccountClickPayment10PortType WSACP10Port;
    public static WSCustomerExtendedInfoCL10PortType WSCEI10Port;
    public static WSCustomerBaseInfo12PortType WSCBI12Port;
    public static WSPaymentBindingList11PortType WSPBL11Port;
    public static WSAccountClickPayment10Service WSACP10Service;
    public static WSCustomerExtendedInfoCL10Service WSCEI10Service;
    public static WSCustomerBaseInfo12Service WSCBI12Service;
    public static WSPaymentBindingList11Service WSPBL11Service;
    public static WSCustomerAccountListGBA13PortType WSCALGBA13Port;
    public static WSCustomerAccountListGBA13Service WSCALGBA13Service;
    public final static String EXCEPTION = "Exception! ";

    public static void trace(String message) throws IOException {

                FileWriter e = new FileWriter(LOG_PATH, true);
                BufferedWriter br = new BufferedWriter(e);
                br.write(message + "\n");
                br.close();

        }



    public void invoke(String functionName, GXArguments arguments, GXResult result) throws Exception {




        String userID  = "", branchNumber = "",
               externalSystemCode = "", externalUserCode = "";
        String serviceName = "";
        String traceLog = "";
        String serviceUrl = "";
        String PayRef = "";
        String Cus = "";
        String Sts = "";
        String Account = "";
        String Purse = "";
        String strChannelID = "";
        String strGRP = "";
        String strSQL = "";
        String strLNM = "";

        try {
            serviceUrl = arguments.getString("service_url");
            externalSystemCode = arguments.getString("externalSystemCode");
            externalUserCode = arguments.getString("externalUserCode");
            userID = arguments.getString("userID");
            branchNumber = arguments.getString("branchNumber");
            serviceName = arguments.getString("service_name");
            traceLog = arguments.getString("trace_log");
            if (serviceName.equals("WSCustomerBaseInfo12")) {//functionName=="getCustomerBaseInfo") {
                Cus = arguments.getString("Cus");
            } else {
                PayRef = arguments.getString("Pay_Ref");
                Cus = arguments.getString("Cus");
                Account = arguments.getString("Account");
               // Purse = arguments.getString("Purse");        /// not used in current version
                Sts = arguments.getString("Sts");
                strChannelID = arguments.getString("ChannellID");
                strGRP = arguments.getString("Grp");
                strSQL = arguments.getString("SQL");
                strLNM = arguments.getString("LNM");
            }
            if (!IsBegin)
            {
                ///Начальная загрузка WSDL-структуры для всех веб-сервисов
                String[] resString = serviceUrl.split(";");
         //       WebServiceBridge.trace(serviceUrl);
        //        WebServiceBridge.trace(serviceName);
         //       for(String aa:resString)
         //           WebServiceBridge.trace(aa);
                WSACP10Service = new WSAccountClickPayment10Service(new URL(resString[0]), new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Service"));
                WSACP10Port = WSACP10Service.getWSAccountClickPayment10Port();
            //    WebServiceBridge.trace("ACP ok");
                WSCBI12Service = new WSCustomerBaseInfo12Service(new URL(resString[1]), new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfo12Service"));
                WSCBI12Port = WSCBI12Service.getWSCustomerBaseInfo12Port();
           //     WebServiceBridge.trace("CBI ok");
                WSCEI10Service = new WSCustomerExtendedInfoCL10Service(new URL(resString[2]), new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Service"));
                WSCEI10Port = WSCEI10Service.getWSCustomerExtendedInfoCL10Port();
          //      WebServiceBridge.trace("CEI ok");
                WSPBL11Service = new WSPaymentBindingList11Service(new URL(resString[3]), new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Service"));
                WSPBL11Port = WSPBL11Service.getWSPaymentBindingList11Port();
           //     WebServiceBridge.trace("PBL ok");
                WSCALGBA13Service = new WSCustomerAccountListGBA13Service(new URL(resString[4]), new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Service"));
                WSCALGBA13Port = WSCALGBA13Service.getWSCustomerAccountListGBA13Port();
           //     WebServiceBridge.trace("ALG ok");
                IsBegin = true;
            }
        } catch (Exception var20) {
            result.setString(var20.getMessage() + " " + var20.getLocalizedMessage() + var20.toString());
            return;
        }

        StringBuilder sbRes = new StringBuilder();

        try
        {
            if (serviceName.equals("WSPaymentBindingList11"))
                sbRes.append(WSPaymentBindingList11Connector.GetPurses(userID, branchNumber,externalSystemCode,externalUserCode, Cus, Sts));
            else if (serviceName.equals("WSAccountListGBA13"))
                sbRes.append(WSBindingListGBA13Connector.getResponse(userID ,branchNumber,externalSystemCode,externalUserCode, Cus, strChannelID, strGRP, strSQL, strLNM));
            else if (serviceName.equals("WSCustomerBaseInfo12"))
                sbRes.append(WSCustomerBaseInfo12Connector.getCustomerBaseInfo(userID, branchNumber,externalSystemCode,externalUserCode, Cus));
            else
            {
                OuterType ex = new OuterType();
                sbRes.append(WSAccountClickPayment10Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, ex, PayRef))
                        .append(";");
                if (!Cus.contentEquals(""))
                {
                    ex.m_strCUS[0] = Cus;
                }

                sbRes.append(WSCustomerBaseInfo12Connector.getResponse(userID, branchNumber,externalSystemCode,externalUserCode, ex.m_strCUS[0],ex.m_strCUSDest[0]))
                        .append(";")
                        .append(WSCustomerExtendedInfoCL10.getResponse(userID ,branchNumber,externalSystemCode,externalUserCode, ex.m_strCUS[0]));
            }
        } catch (Exception var19) {
            result.setString("Error " + Arrays.toString(var19.getStackTrace()) + var19.getMessage());
            return;
        }
        result.setString(sbRes.toString());
    }

}
