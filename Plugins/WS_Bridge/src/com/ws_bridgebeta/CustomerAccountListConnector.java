package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.*;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetInParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSet;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSetRow;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

import java.util.List;


public class CustomerAccountListConnector {

    private static WSCustomerAccountListGBAGetOutResultSet getResponseRS(String userId, String externalUserCode, String BranchNumber, String ExternalSystemCode, String Client) throws MsgWSTechnicalException, MsgWSAppTechnicalException, MsgWSAppException, MsgWSAccessException, MsgWSVerificationException {
        WSCommonParms inCommonParms = new WSCommonParms();
        WSCustomerAccountListGBAGetInParms inParms = new WSCustomerAccountListGBAGetInParms();

        inCommonParms.setExternalUserCode(externalUserCode);
        inCommonParms.setBranchNumber(BranchNumber);
        inCommonParms.setExternalSystemCode(ExternalSystemCode);
        inCommonParms.setUserID(userId);

        inParms.setCus(Client);

        WSCustomerAccountListGBAGetResponseType response = WebServiceBridge.WSCALGBA13Port.wsCustomerAccountListGBAGet(inCommonParms, inParms);
        WSCustomerAccountListGBAGetOutParms outParms = response.getOutParms();

        WSCustomerAccountListGBAGetOutResultSet resultSet = outParms.getResultSet();

        return resultSet;
    }

    public static String[] getResponse(String userId, String externalUserCode, String BranchNumber, String ExternalSystemCode, String Client) {

        try {
            WSCustomerAccountListGBAGetOutResultSet resultSet = getResponseRS(userId, externalUserCode, BranchNumber, ExternalSystemCode, Client);
            List<WSCustomerAccountListGBAGetOutResultSetRow> resultSetRow = resultSet.getResultSetRow();

            int listSize = resultSetRow.size();

            String[] result = new String[listSize];

            for (int i = 0; i < listSize; i++) {
                WSCustomerAccountListGBAGetOutResultSetRow row = resultSetRow.get(i);
                result[i] = getAllInfo(row);
//                char[] chars = result[i].toCharArray();
//                int delimCounter=0;
//                for(char c: chars )
//                    if(c==';')
//                        delimCounter++;
//                if (delimCounter > 38) {
//                    String[] strings = new String[1];
//                    strings[0] = WebServiceBridge.EXCEPTION + "Delimeter in data!";
//                    return strings;
//                }
            }
            return result;

        } catch (MsgWSTechnicalException e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getFaultInfo().getErrorTrace();
            return strings;

        } catch (MsgWSAppTechnicalException e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getFaultInfo().getErrorTrace();
            return strings;
        } catch (MsgWSAppException e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getFaultInfo().getErrorTrace();
            return strings;
        } catch (MsgWSAccessException e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getFaultInfo().getErrorTrace();
            return strings;
        } catch (MsgWSVerificationException e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getFaultInfo().getErrorTrace();
            return strings;

        } catch (Exception e) {
            String[] strings = new String[1];
            strings[0] = WebServiceBridge.EXCEPTION + e.getMessage();
            return strings;
        }
    }

    private static String getAllInfo(WSCustomerAccountListGBAGetOutResultSetRow row) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(row.getAb());//0
        stringBuilder.append(";");

        stringBuilder.append(row.getAcln());//1
        stringBuilder.append(";");

        stringBuilder.append(row.getAcnm());//2
        stringBuilder.append(";");

        stringBuilder.append(row.getAcsn());//3
        stringBuilder.append(";");

        stringBuilder.append(row.getAct());//4
        stringBuilder.append(";");

        stringBuilder.append(row.getActd());//5
        stringBuilder.append(";");

        stringBuilder.append(row.getAi11());//6
        stringBuilder.append(";");

        stringBuilder.append(row.getAi12());//7
        stringBuilder.append(";");

        stringBuilder.append(row.getAi14());//8
        stringBuilder.append(";");

        stringBuilder.append(row.getAi17());//9
        stringBuilder.append(";");

        stringBuilder.append(row.getAi20());//10
        stringBuilder.append(";");

        stringBuilder.append(row.getAi30());//11
        stringBuilder.append(";");

        stringBuilder.append(row.getAi47());//12
        stringBuilder.append(";");

        stringBuilder.append(row.getAi82());//13
        stringBuilder.append(";");

        stringBuilder.append(row.getAi83());//14
        stringBuilder.append(";");

        stringBuilder.append(row.getAi87());//15
        stringBuilder.append(";");

        stringBuilder.append(row.getAn());//16
        stringBuilder.append(";");

        stringBuilder.append(row.getAs());//17
        stringBuilder.append(";");

        stringBuilder.append(row.getBrg());//18
        stringBuilder.append(";");

        stringBuilder.append(row.getCcy());//19
        stringBuilder.append(";");

        stringBuilder.append(row.getCcyd());//20
        stringBuilder.append(";");

        stringBuilder.append(row.getCcym());//21
        stringBuilder.append(";");

        stringBuilder.append(row.getEan());//21
        stringBuilder.append(";");

        stringBuilder.append(row.getExp());//23
        stringBuilder.append(";");

        stringBuilder.append(row.getGh());//24
        stringBuilder.append(";");

        stringBuilder.append(row.getP3R());//25
        stringBuilder.append(";");

        stringBuilder.append(row.getP3Rd());//26
        stringBuilder.append(";");

        stringBuilder.append(row.getP5P());//27
        stringBuilder.append(";");

        stringBuilder.append(row.getPrd());//28
        stringBuilder.append(";");

        stringBuilder.append(row.getSts());//29
        stringBuilder.append(";");

        stringBuilder.append(row.getToe());//30
        stringBuilder.append(";");

        stringBuilder.append(row.getTrz());//31
        stringBuilder.append(";");

        stringBuilder.append(row.getZzz());//32
        stringBuilder.append(";");

        stringBuilder.append(row.getAma());//33
        stringBuilder.append(";");

        stringBuilder.append(row.getHolds());//34
        stringBuilder.append(";");

        stringBuilder.append(row.getOad());//35
        stringBuilder.append(";");

        stringBuilder.append(row.getPwd());//36
        stringBuilder.append(";");

        stringBuilder.append(row.getTotal());//37

        return stringBuilder.toString();
    }
}
