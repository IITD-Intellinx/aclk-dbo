package com.ws_bridgebeta;

import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.*;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetInParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSet;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSetRow;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;

public class WSAccountClickPayment10Connector {

    static final String WS_NAME = "WSAccountClickPayment10 ";
    static final String C_PURSETYPE="E04";
    ///Get payment data
    public static String getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, OuterType OutEQID, String PayRef) throws RemoteException, MalformedURLException {
        try {
            WSCommonParms commonParms = new WSCommonParms();
            commonParms.setUserID(userID);
            commonParms.setBranchNumber(branchNumber);
            commonParms.setExternalSystemCode(externalSystemCode);
            commonParms.setExternalUserCode(externalUserCode);

            WSAccountClickPaymentGetInParms inParms = new WSAccountClickPaymentGetInParms();

            String ResultString;
            try {
                inParms.setMod("U");
                inParms.setRef(PayRef);
                WSAccountClickPaymentGetResponseType e = WebServiceBridge.WSACP10Port.wsAccountClickPaymentGet(commonParms, inParms);
                WSAccountClickPaymentGetOutParms outParms = e.getOutParms();
                WSAccountClickPaymentGetOutResultSet resultSet = outParms.getResultSet();
                List resultSetRowList = resultSet.getResultSetRow();
                StringBuilder sbOut = new StringBuilder();
                WSAccountClickPaymentGetOutResultSetRow resultSetRow;
                ///Extracting Payment Data
                for (Iterator var12 = resultSetRowList.iterator(); var12.hasNext(); sbOut.append(resultSetRow.getField().trim()).append("==").append( resultSetRow.getValue().trim()).append(";") )
                    resultSetRow = (WSAccountClickPaymentGetOutResultSetRow) var12.next();


                if (outParms.getTyp().trim().contentEquals(""))
                    sbOut.append("TYP==;");
                else
                    sbOut.append("TYP==").append(outParms.getTyp().trim()).append(";");


                if (outParms.getAnc().trim().contentEquals(""))
                {
                    sbOut.append("ANC==;");
                }
                else
                {
                    sbOut.append("ANC==").append(outParms.getAnc().trim()).append(";");
                    if(!outParms.getTyp().trim().equals(C_PURSETYPE))
                    {
                        OutEQID.m_strCUSDest[0] = outParms.getAnc().trim();
                    }
                }

                if (outParms.getRcp().trim().contentEquals(""))
                    sbOut.append("RCP==;");
                else
                    sbOut.append("RCP==").append(outParms.getRcp().trim()).append(";");


                if (outParms.getTyp().trim().contentEquals(""))
                    sbOut.append("AMA==;");
                else
                    sbOut.append("AMA==").append(outParms.getTyp().trim()).append(";");


                if (outParms.getCcy().trim().contentEquals(""))
                    sbOut.append("CCY==;");
                else
                    sbOut.append("CCY==").append(outParms.getCcy().trim()).append( ";");

                if (outParms.getCus().trim().contentEquals(""))
                {
                    sbOut.append("CUS==;");
                    OutEQID.m_strCUS[0] = "";
                }
                else
                {
                    sbOut.append("CUS==").append(outParms.getCus().trim()).append( ";");
                    OutEQID.m_strCUS[0] = outParms.getCus().trim();
                }

                if (outParms.getClc().trim().contentEquals(""))
                {
                    sbOut.append("CLC==;");
                    OutEQID.m_strCLC[0] = "";
                }
                else
                {
                    sbOut.append("CLC==").append(outParms.getClc().trim()).append(";");
                    OutEQID.m_strCLC[0] = outParms.getClc().trim();
                }

                if (outParms.getClk() != null)
                {
                    XMLGregorianCalendar dtTimestamp1 = outParms.getClk();
                    sbOut.append("CLK==").append(dtTimestamp1.toString()).append(";");
                }
                else
                    sbOut.append("CLK==;");

                if (outParms.getXm().contentEquals(""))
                    sbOut.append("XM==;");
                else
                    sbOut.append("XM==").append(outParms.getXm().trim()).append(";");

                if (outParms.getSts().contentEquals(""))
                    sbOut.append("STS==;");
                else
                    sbOut.append("STS==").append(outParms.getSts().trim()).append(";");
                if (outParms.getGrp().contentEquals(""))
                    sbOut.append("GRP==;");
                else
                    sbOut.append("GRP==").append(outParms.getGrp().trim()).append(";");

                if (outParms.getTic().contentEquals(""))
                    sbOut.append("TIC==;");
                else
                    sbOut.append("TIC==").append(outParms.getTic().trim()).append(";");

                if (outParms.getSor().contentEquals(""))
                    sbOut.append("SOR==");
                else
                    sbOut.append("SOR==").append(outParms.getSor().trim());

                return sbOut.toString();
            }

            catch (MsgWSTechnicalException e)
            {
                ResultString = WS_NAME+"MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppTechnicalException e)
            {
                ResultString = WS_NAME+"MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAppException e)
            {
                ResultString = WS_NAME+"MsgWSAppException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSAccessException e)
            {
                ResultString = WS_NAME+"MsgWSAccessException: " + e.getFaultInfo().getErrorString();
            }
            catch (MsgWSVerificationException e)
            {
                ResultString = WS_NAME+"MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
            }
            catch (Exception e)
            {
                ResultString = WS_NAME+"Exception: " + e.getMessage();
            }
        return ResultString;
        } catch (Exception var14) {
            return "ErrorConnect WSAccountClickPayment10";
        }
    }
}
