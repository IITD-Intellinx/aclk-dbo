package com.ws_bridgebeta;

import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPayment10Service;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBA13Service;
import ru.alfabank.ws.cs.eq.wscustomerbaseinfo12.WSCustomerBaseInfo12Service;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCL10Service;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingList11Service;

import javax.xml.namespace.QName;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

public class WSChangeMainEndPoints extends GXFunctionExtension {
   /// На случай, если изменились End-Point'ы у веб-сервисов
   public void invoke(String functionName, GXArguments arguments, GXResult result) throws Exception {
      try {
         String urlACP10;
         String urlCBI12;
         String urlPBL11;
         String urlCEI10;
         String urlCALGBA13;

         urlACP10 = arguments.getString("UrlACP10");
         urlCBI12 = arguments.getString("UrlCBI12");
         urlPBL11 = arguments.getString("UrlPBL11");
         urlCEI10 = arguments.getString("UrlCEI10");
         urlCALGBA13 = arguments.getString("UrlCALGBA13");
         if(!urlACP10.isEmpty()) {
            WebServiceBridge.WSACP10Service = new WSAccountClickPayment10Service(new URL(urlACP10), new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Service"));
            WebServiceBridge.WSACP10Port = WebServiceBridge.WSACP10Service.getWSAccountClickPayment10Port();
         }

         if(!urlCBI12.isEmpty()) {
            WebServiceBridge.WSCBI12Service = new WSCustomerBaseInfo12Service(new URL(urlCBI12), new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfo12Service"));
            WebServiceBridge.WSCBI12Port = WebServiceBridge.WSCBI12Service.getWSCustomerBaseInfo12Port();
         }

         if(!urlPBL11.isEmpty()) {
            WebServiceBridge.WSPBL11Service = new WSPaymentBindingList11Service(new URL(urlPBL11), new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Service"));
            WebServiceBridge.WSPBL11Port = WebServiceBridge.WSPBL11Service.getWSPaymentBindingList11Port();
         }

         if(!urlCEI10.isEmpty()) {
            WebServiceBridge.WSCEI10Service = new WSCustomerExtendedInfoCL10Service(new URL(urlCEI10), new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Service"));
            WebServiceBridge.WSCEI10Port = WebServiceBridge.WSCEI10Service.getWSCustomerExtendedInfoCL10Port();
         }

         if(!urlCALGBA13.isEmpty()) {
            WebServiceBridge.WSCALGBA13Service = new WSCustomerAccountListGBA13Service(new URL(urlCALGBA13), new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Service"));
            WebServiceBridge.WSCALGBA13Port = WebServiceBridge.WSCALGBA13Service.getWSCustomerAccountListGBA13Port();
         }

         WebServiceBridge.IsBegin = true;
         result.setString("S");
      } catch (Exception var9) {
          StringWriter stringWriter = new StringWriter();
          PrintWriter printWriter = new PrintWriter(stringWriter);
          var9.printStackTrace(printWriter);
          String traceString = stringWriter.getBuffer().toString();
         result.setString("Error " + var9.getMessage() + traceString);

      }
   }
}
