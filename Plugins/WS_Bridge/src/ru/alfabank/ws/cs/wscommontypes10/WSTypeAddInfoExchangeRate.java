
package ru.alfabank.ws.cs.wscommontypes10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSTypeAddInfoExchangeRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSTypeAddInfoExchangeRate">
 *   &lt;complexContent>
 *     &lt;extension base="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeAddInfo">
 *       &lt;sequence>
 *         &lt;element name="adt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal159"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSTypeAddInfoExchangeRate", propOrder = {
    "adt"
})
public class WSTypeAddInfoExchangeRate
    extends WSTypeAddInfo
{

    @XmlElement(required = true)
    protected BigDecimal adt;

    /**
     * Gets the value of the adt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAdt() {
        return adt;
    }

    /**
     * Sets the value of the adt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAdt(BigDecimal value) {
        this.adt = value;
    }

}
