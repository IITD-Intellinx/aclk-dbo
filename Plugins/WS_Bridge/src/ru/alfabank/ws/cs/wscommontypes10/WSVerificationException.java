
package ru.alfabank.ws.cs.wscommontypes10;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorString" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorTrace" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorExt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSExtention" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "errorCode",
    "errorString",
    "errorTrace",
    "errorExt"
})
@XmlRootElement(name = "WSVerificationException")
public class WSVerificationException {

    @XmlElement(required = true)
    protected String errorCode;
    @XmlElement(required = true)
    protected String errorString;
    @XmlElement(required = true)
    protected String errorTrace;
    @XmlElement(required = true)
    protected List<WSExtention> errorExt;

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorString() {
        return errorString;
    }

    /**
     * Sets the value of the errorString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorString(String value) {
        this.errorString = value;
    }

    /**
     * Gets the value of the errorTrace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorTrace() {
        return errorTrace;
    }

    /**
     * Sets the value of the errorTrace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorTrace(String value) {
        this.errorTrace = value;
    }

    /**
     * Gets the value of the errorExt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorExt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorExt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WSExtention }
     * 
     * 
     */
    public List<WSExtention> getErrorExt() {
        if (errorExt == null) {
            errorExt = new ArrayList<WSExtention>();
        }
        return this.errorExt;
    }

}
