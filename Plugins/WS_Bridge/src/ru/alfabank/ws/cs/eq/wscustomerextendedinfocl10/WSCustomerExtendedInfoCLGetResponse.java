package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGetResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLGetResponse",
   propOrder = {"response"}
)
public class WSCustomerExtendedInfoCLGetResponse {

   @XmlElement(
      required = true
   )
   protected WSCustomerExtendedInfoCLGetResponseType response;


   public WSCustomerExtendedInfoCLGetResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSCustomerExtendedInfoCLGetResponseType value) {
      this.response = value;
   }
}
