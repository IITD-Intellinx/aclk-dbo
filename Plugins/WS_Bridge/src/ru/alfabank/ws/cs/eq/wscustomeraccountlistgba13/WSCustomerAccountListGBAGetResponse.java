package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGetResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetResponse",
   propOrder = {"response"}
)
public class WSCustomerAccountListGBAGetResponse {

   @XmlElement(
      required = true
   )
   protected WSCustomerAccountListGBAGetResponseType response;


   public WSCustomerAccountListGBAGetResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSCustomerAccountListGBAGetResponseType value) {
      this.response = value;
   }
}
