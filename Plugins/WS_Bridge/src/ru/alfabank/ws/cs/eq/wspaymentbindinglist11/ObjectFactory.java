package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGet;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGetResponse;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGetResponseType;

@XmlRegistry
public class ObjectFactory {

   private static final QName _WSPaymentBindingListGetResponse_QNAME = new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingListGetResponse");
   private static final QName _WSPaymentBindingListGet_QNAME = new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingListGet");


   public WSPaymentBindingListGet createWSPaymentBindingListGet() {
      return new WSPaymentBindingListGet();
   }

   public WSPaymentBindingListGetResponseType createWSPaymentBindingListGetResponseType() {
      return new WSPaymentBindingListGetResponseType();
   }

   public WSPaymentBindingListGetResponse createWSPaymentBindingListGetResponse() {
      return new WSPaymentBindingListGetResponse();
   }

   @XmlElementDecl(
      namespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru",
      name = "WSPaymentBindingListGetResponse"
   )
   public JAXBElement<WSPaymentBindingListGetResponse> createWSPaymentBindingListGetResponse(WSPaymentBindingListGetResponse value) {
      return new JAXBElement(_WSPaymentBindingListGetResponse_QNAME, WSPaymentBindingListGetResponse.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru",
      name = "WSPaymentBindingListGet"
   )
   public JAXBElement<WSPaymentBindingListGet> createWSPaymentBindingListGet(WSPaymentBindingListGet value) {
      return new JAXBElement(_WSPaymentBindingListGet_QNAME, WSPaymentBindingListGet.class, (Class)null, value);
   }
}
