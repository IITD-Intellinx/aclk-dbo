package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSet;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGetOutParms",
   propOrder = {"typ", "ref", "rte", "rteact", "rtests", "rtexm", "rtevxm", "rtefls", "rteprs", "sts", "inp", "xm", "xmdsc", "ps", "psdsc", "prc", "prcshn", "prcdsc", "prcprm", "prcppc", "prcpsc", "prcreg", "rcp", "abd", "and", "asd", "end", "abc", "anc", "asc", "enc", "ama", "fee", "ccy", "pod", "brn", "pbr", "psq", "hno", "aco", "hrc", "dts", "dte", "nar", "hdd", "cus", "clc", "rfe", "doc", "tic", "ticnam", "bnd", "sor", "mac", "clk", "tin", "tms", "job", "err", "er0", "er1", "er2", "errtxt", "grp", "grpdsc", "grppth", "stsdsc", "prmshn", "prmdsc", "pwd", "grpcbx", "prccbx", "amw", "resultSet"}
)
public class WSAccountClickPaymentGetOutParms {

   protected String typ;
   protected String ref;
   protected String rte;
   protected String rteact;
   protected String rtests;
   protected String rtexm;
   protected String rtevxm;
   protected String rtefls;
   protected String rteprs;
   protected String sts;
   protected XMLGregorianCalendar inp;
   protected String xm;
   protected String xmdsc;
   protected String ps;
   protected String psdsc;
   protected String prc;
   protected String prcshn;
   protected String prcdsc;
   protected String prcprm;
   protected String prcppc;
   protected String prcpsc;
   protected String prcreg;
   protected String rcp;
   protected String abd;
   protected String and;
   protected String asd;
   protected String end;
   protected String abc;
   protected String anc;
   protected String asc;
   protected String enc;
   protected BigDecimal ama;
   protected BigDecimal fee;
   protected String ccy;
   protected XMLGregorianCalendar pod;
   protected String brn;
   protected String pbr;
   protected BigDecimal psq;
   protected BigDecimal hno;
   protected String aco;
   protected String hrc;
   protected XMLGregorianCalendar dts;
   protected XMLGregorianCalendar dte;
   protected String nar;
   protected String hdd;
   protected String cus;
   protected String clc;
   protected String rfe;
   protected String doc;
   protected String tic;
   protected String ticnam;
   protected String bnd;
   protected String sor;
   protected String mac;
   protected XMLGregorianCalendar clk;
   protected XMLGregorianCalendar tin;
   protected XMLGregorianCalendar tms;
   protected String job;
   protected String err;
   protected String er0;
   protected String er1;
   protected String er2;
   protected String errtxt;
   protected String grp;
   protected String grpdsc;
   protected String grppth;
   protected String stsdsc;
   protected String prmshn;
   protected String prmdsc;
   protected BigDecimal pwd;
   protected String grpcbx;
   protected String prccbx;
   protected String amw;
   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentGetOutResultSet resultSet;


   public String getTyp() {
      return this.typ;
   }

   public void setTyp(String value) {
      this.typ = value;
   }

   public String getRef() {
      return this.ref;
   }

   public void setRef(String value) {
      this.ref = value;
   }

   public String getRte() {
      return this.rte;
   }

   public void setRte(String value) {
      this.rte = value;
   }

   public String getRteact() {
      return this.rteact;
   }

   public void setRteact(String value) {
      this.rteact = value;
   }

   public String getRtests() {
      return this.rtests;
   }

   public void setRtests(String value) {
      this.rtests = value;
   }

   public String getRtexm() {
      return this.rtexm;
   }

   public void setRtexm(String value) {
      this.rtexm = value;
   }

   public String getRtevxm() {
      return this.rtevxm;
   }

   public void setRtevxm(String value) {
      this.rtevxm = value;
   }

   public String getRtefls() {
      return this.rtefls;
   }

   public void setRtefls(String value) {
      this.rtefls = value;
   }

   public String getRteprs() {
      return this.rteprs;
   }

   public void setRteprs(String value) {
      this.rteprs = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public XMLGregorianCalendar getInp() {
      return this.inp;
   }

   public void setInp(XMLGregorianCalendar value) {
      this.inp = value;
   }

   public String getXm() {
      return this.xm;
   }

   public void setXm(String value) {
      this.xm = value;
   }

   public String getXmdsc() {
      return this.xmdsc;
   }

   public void setXmdsc(String value) {
      this.xmdsc = value;
   }

   public String getPs() {
      return this.ps;
   }

   public void setPs(String value) {
      this.ps = value;
   }

   public String getPsdsc() {
      return this.psdsc;
   }

   public void setPsdsc(String value) {
      this.psdsc = value;
   }

   public String getPrc() {
      return this.prc;
   }

   public void setPrc(String value) {
      this.prc = value;
   }

   public String getPrcshn() {
      return this.prcshn;
   }

   public void setPrcshn(String value) {
      this.prcshn = value;
   }

   public String getPrcdsc() {
      return this.prcdsc;
   }

   public void setPrcdsc(String value) {
      this.prcdsc = value;
   }

   public String getPrcprm() {
      return this.prcprm;
   }

   public void setPrcprm(String value) {
      this.prcprm = value;
   }

   public String getPrcppc() {
      return this.prcppc;
   }

   public void setPrcppc(String value) {
      this.prcppc = value;
   }

   public String getPrcpsc() {
      return this.prcpsc;
   }

   public void setPrcpsc(String value) {
      this.prcpsc = value;
   }

   public String getPrcreg() {
      return this.prcreg;
   }

   public void setPrcreg(String value) {
      this.prcreg = value;
   }

   public String getRcp() {
      return this.rcp;
   }

   public void setRcp(String value) {
      this.rcp = value;
   }

   public String getAbd() {
      return this.abd;
   }

   public void setAbd(String value) {
      this.abd = value;
   }

   public String getAnd() {
      return this.and;
   }

   public void setAnd(String value) {
      this.and = value;
   }

   public String getAsd() {
      return this.asd;
   }

   public void setAsd(String value) {
      this.asd = value;
   }

   public String getEnd() {
      return this.end;
   }

   public void setEnd(String value) {
      this.end = value;
   }

   public String getAbc() {
      return this.abc;
   }

   public void setAbc(String value) {
      this.abc = value;
   }

   public String getAnc() {
      return this.anc;
   }

   public void setAnc(String value) {
      this.anc = value;
   }

   public String getAsc() {
      return this.asc;
   }

   public void setAsc(String value) {
      this.asc = value;
   }

   public String getEnc() {
      return this.enc;
   }

   public void setEnc(String value) {
      this.enc = value;
   }

   public BigDecimal getAma() {
      return this.ama;
   }

   public void setAma(BigDecimal value) {
      this.ama = value;
   }

   public BigDecimal getFee() {
      return this.fee;
   }

   public void setFee(BigDecimal value) {
      this.fee = value;
   }

   public String getCcy() {
      return this.ccy;
   }

   public void setCcy(String value) {
      this.ccy = value;
   }

   public XMLGregorianCalendar getPod() {
      return this.pod;
   }

   public void setPod(XMLGregorianCalendar value) {
      this.pod = value;
   }

   public String getBrn() {
      return this.brn;
   }

   public void setBrn(String value) {
      this.brn = value;
   }

   public String getPbr() {
      return this.pbr;
   }

   public void setPbr(String value) {
      this.pbr = value;
   }

   public BigDecimal getPsq() {
      return this.psq;
   }

   public void setPsq(BigDecimal value) {
      this.psq = value;
   }

   public BigDecimal getHno() {
      return this.hno;
   }

   public void setHno(BigDecimal value) {
      this.hno = value;
   }

   public String getAco() {
      return this.aco;
   }

   public void setAco(String value) {
      this.aco = value;
   }

   public String getHrc() {
      return this.hrc;
   }

   public void setHrc(String value) {
      this.hrc = value;
   }

   public XMLGregorianCalendar getDts() {
      return this.dts;
   }

   public void setDts(XMLGregorianCalendar value) {
      this.dts = value;
   }

   public XMLGregorianCalendar getDte() {
      return this.dte;
   }

   public void setDte(XMLGregorianCalendar value) {
      this.dte = value;
   }

   public String getNar() {
      return this.nar;
   }

   public void setNar(String value) {
      this.nar = value;
   }

   public String getHdd() {
      return this.hdd;
   }

   public void setHdd(String value) {
      this.hdd = value;
   }

   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getRfe() {
      return this.rfe;
   }

   public void setRfe(String value) {
      this.rfe = value;
   }

   public String getDoc() {
      return this.doc;
   }

   public void setDoc(String value) {
      this.doc = value;
   }

   public String getTic() {
      return this.tic;
   }

   public void setTic(String value) {
      this.tic = value;
   }

   public String getTicnam() {
      return this.ticnam;
   }

   public void setTicnam(String value) {
      this.ticnam = value;
   }

   public String getBnd() {
      return this.bnd;
   }

   public void setBnd(String value) {
      this.bnd = value;
   }

   public String getSor() {
      return this.sor;
   }

   public void setSor(String value) {
      this.sor = value;
   }

   public String getMac() {
      return this.mac;
   }

   public void setMac(String value) {
      this.mac = value;
   }

   public XMLGregorianCalendar getClk() {
      return this.clk;
   }

   public void setClk(XMLGregorianCalendar value) {
      this.clk = value;
   }

   public XMLGregorianCalendar getTin() {
      return this.tin;
   }

   public void setTin(XMLGregorianCalendar value) {
      this.tin = value;
   }

   public XMLGregorianCalendar getTms() {
      return this.tms;
   }

   public void setTms(XMLGregorianCalendar value) {
      this.tms = value;
   }

   public String getJob() {
      return this.job;
   }

   public void setJob(String value) {
      this.job = value;
   }

   public String getErr() {
      return this.err;
   }

   public void setErr(String value) {
      this.err = value;
   }

   public String getEr0() {
      return this.er0;
   }

   public void setEr0(String value) {
      this.er0 = value;
   }

   public String getEr1() {
      return this.er1;
   }

   public void setEr1(String value) {
      this.er1 = value;
   }

   public String getEr2() {
      return this.er2;
   }

   public void setEr2(String value) {
      this.er2 = value;
   }

   public String getErrtxt() {
      return this.errtxt;
   }

   public void setErrtxt(String value) {
      this.errtxt = value;
   }

   public String getGrp() {
      return this.grp;
   }

   public void setGrp(String value) {
      this.grp = value;
   }

   public String getGrpdsc() {
      return this.grpdsc;
   }

   public void setGrpdsc(String value) {
      this.grpdsc = value;
   }

   public String getGrppth() {
      return this.grppth;
   }

   public void setGrppth(String value) {
      this.grppth = value;
   }

   public String getStsdsc() {
      return this.stsdsc;
   }

   public void setStsdsc(String value) {
      this.stsdsc = value;
   }

   public String getPrmshn() {
      return this.prmshn;
   }

   public void setPrmshn(String value) {
      this.prmshn = value;
   }

   public String getPrmdsc() {
      return this.prmdsc;
   }

   public void setPrmdsc(String value) {
      this.prmdsc = value;
   }

   public BigDecimal getPwd() {
      return this.pwd;
   }

   public void setPwd(BigDecimal value) {
      this.pwd = value;
   }

   public String getGrpcbx() {
      return this.grpcbx;
   }

   public void setGrpcbx(String value) {
      this.grpcbx = value;
   }

   public String getPrccbx() {
      return this.prccbx;
   }

   public void setPrccbx(String value) {
      this.prccbx = value;
   }

   public String getAmw() {
      return this.amw;
   }

   public void setAmw(String value) {
      this.amw = value;
   }

   public WSAccountClickPaymentGetOutResultSet getResultSet() {
      return this.resultSet;
   }

   public void setResultSet(WSAccountClickPaymentGetOutResultSet value) {
      this.resultSet = value;
   }
}
