package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSTypeAdditionalInfoCode",
   propOrder = {"adt"}
)
public class WSTypeAdditionalInfoCode extends WSTypeAdditionalInfo {

   @XmlElement(
      required = true
   )
   protected String adt;


   public String getAdt() {
      return this.adt;
   }

   public void setAdt(String value) {
      this.adt = value;
   }
}
