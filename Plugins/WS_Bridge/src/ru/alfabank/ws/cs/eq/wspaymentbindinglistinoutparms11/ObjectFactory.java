package ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11;

import javax.xml.bind.annotation.XmlRegistry;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetInParms;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutParms;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutResultSet;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutResultSetRow;

@XmlRegistry
public class ObjectFactory {

   public WSPaymentBindingListGetOutResultSet createWSPaymentBindingListGetOutResultSet() {
      return new WSPaymentBindingListGetOutResultSet();
   }

   public WSPaymentBindingListGetOutParms createWSPaymentBindingListGetOutParms() {
      return new WSPaymentBindingListGetOutParms();
   }

   public WSPaymentBindingListGetOutResultSetRow createWSPaymentBindingListGetOutResultSetRow() {
      return new WSPaymentBindingListGetOutResultSetRow();
   }

   public WSPaymentBindingListGetInParms createWSPaymentBindingListGetInParms() {
      return new WSPaymentBindingListGetInParms();
   }
}
