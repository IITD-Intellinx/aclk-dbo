package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPayment10PortType;

@WebServiceClient(
   name = "WSAccountClickPayment10Service",
   targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
   wsdlLocation = "file:/I:/Projects/Workspace/WS_Test/src_test/WSAccountClickPayment10/EQ/WSAccountClickPayment10.wsdl"
)
public class WSAccountClickPayment10Service extends Service {

   private static final URL WSACCOUNTCLICKPAYMENT10SERVICE_WSDL_LOCATION;
   private static final Logger logger = Logger.getLogger(WSAccountClickPayment10Service.class.getName());


   static {
      URL url = null;

      try {
         URL e = WSAccountClickPayment10Service.class.getResource(".");
         url = new URL(e, "file:/E:/SharedData/2012-04-25_eclipse/Workspace/WS_Test/src_test/WSAccountClickPayment10/EQ/WSAccountClickPayment10.wsdl");
      } catch (MalformedURLException var2) {
         logger.warning("Failed to create URL for the wsdl Location: \'file:E:/SharedData/2012-04-25_eclipse/Workspace/WS_Test/src_test/WSAccountClickPayment10/EQ/WSAccountClickPayment10.wsdl");
         logger.warning(var2.getMessage());
      }

      WSACCOUNTCLICKPAYMENT10SERVICE_WSDL_LOCATION = url;
   }

   public WSAccountClickPayment10Service(URL wsdlLocation, QName serviceName) {
      super(wsdlLocation, serviceName);
   }

   public WSAccountClickPayment10Service() {
      super(WSACCOUNTCLICKPAYMENT10SERVICE_WSDL_LOCATION, new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Service"));
   }

   @WebEndpoint(
      name = "WSAccountClickPayment10Port"
   )
   public WSAccountClickPayment10PortType getWSAccountClickPayment10Port() {
      return (WSAccountClickPayment10PortType)super.getPort(new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Port"), WSAccountClickPayment10PortType.class);
   }

   @WebEndpoint(
      name = "WSAccountClickPayment10Port"
   )
   public WSAccountClickPayment10PortType getWSAccountClickPayment10Port(WebServiceFeature ... features) {
      return (WSAccountClickPayment10PortType)super.getPort(new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPayment10Port"), WSAccountClickPayment10PortType.class, features);
   }
}
