package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAdd;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAddResponse;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAddResponseType;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGet;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGetResponse;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGetResponseType;

@XmlRegistry
public class ObjectFactory {

   private static final QName _WSAccountClickPaymentAdd_QNAME = new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPaymentAdd");
   private static final QName _WSAccountClickPaymentGet_QNAME = new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPaymentGet");
   private static final QName _WSAccountClickPaymentAddResponse_QNAME = new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPaymentAddResponse");
   private static final QName _WSAccountClickPaymentGetResponse_QNAME = new QName("http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru", "WSAccountClickPaymentGetResponse");


   public WSAccountClickPaymentGetResponse createWSAccountClickPaymentGetResponse() {
      return new WSAccountClickPaymentGetResponse();
   }

   public WSAccountClickPaymentGet createWSAccountClickPaymentGet() {
      return new WSAccountClickPaymentGet();
   }

   public WSAccountClickPaymentAddResponseType createWSAccountClickPaymentAddResponseType() {
      return new WSAccountClickPaymentAddResponseType();
   }

   public WSAccountClickPaymentAddResponse createWSAccountClickPaymentAddResponse() {
      return new WSAccountClickPaymentAddResponse();
   }

   public WSAccountClickPaymentGetResponseType createWSAccountClickPaymentGetResponseType() {
      return new WSAccountClickPaymentGetResponseType();
   }

   public WSAccountClickPaymentAdd createWSAccountClickPaymentAdd() {
      return new WSAccountClickPaymentAdd();
   }

   @XmlElementDecl(
      namespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      name = "WSAccountClickPaymentAdd"
   )
   public JAXBElement<WSAccountClickPaymentAdd> createWSAccountClickPaymentAdd(WSAccountClickPaymentAdd value) {
      return new JAXBElement(_WSAccountClickPaymentAdd_QNAME, WSAccountClickPaymentAdd.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      name = "WSAccountClickPaymentGet"
   )
   public JAXBElement<WSAccountClickPaymentGet> createWSAccountClickPaymentGet(WSAccountClickPaymentGet value) {
      return new JAXBElement(_WSAccountClickPaymentGet_QNAME, WSAccountClickPaymentGet.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      name = "WSAccountClickPaymentAddResponse"
   )
   public JAXBElement<WSAccountClickPaymentAddResponse> createWSAccountClickPaymentAddResponse(WSAccountClickPaymentAddResponse value) {
      return new JAXBElement(_WSAccountClickPaymentAddResponse_QNAME, WSAccountClickPaymentAddResponse.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      name = "WSAccountClickPaymentGetResponse"
   )
   public JAXBElement<WSAccountClickPaymentGetResponse> createWSAccountClickPaymentGetResponse(WSAccountClickPaymentGetResponse value) {
      return new JAXBElement(_WSAccountClickPaymentGetResponse_QNAME, WSAccountClickPaymentGetResponse.class, (Class)null, value);
   }
}
