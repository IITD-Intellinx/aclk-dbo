package ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutResultSet;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetOutParms",
   propOrder = {"resultSet"}
)
public class WSPaymentBindingListGetOutParms {

   @XmlElement(
      required = true
   )
   protected WSPaymentBindingListGetOutResultSet resultSet;


   public WSPaymentBindingListGetOutResultSet getResultSet() {
      return this.resultSet;
   }

   public void setResultSet(WSPaymentBindingListGetOutResultSet value) {
      this.resultSet = value;
   }
}
