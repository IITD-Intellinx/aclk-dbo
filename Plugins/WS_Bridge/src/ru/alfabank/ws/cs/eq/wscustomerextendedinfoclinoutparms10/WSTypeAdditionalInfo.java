package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoAlphanumeric;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoAmount;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoCode;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoDate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoExchangeRate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoInterestRate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoNumeric;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSTypeAdditionalInfo",
   propOrder = {"anm", "dsc"}
)
@XmlSeeAlso({WSTypeAdditionalInfoInterestRate.class, WSTypeAdditionalInfoNumeric.class, WSTypeAdditionalInfoExchangeRate.class, WSTypeAdditionalInfoDate.class, WSTypeAdditionalInfoCode.class, WSTypeAdditionalInfoAlphanumeric.class, WSTypeAdditionalInfoAmount.class})
public abstract class WSTypeAdditionalInfo {

   @XmlElement(
      required = true
   )
   protected String anm;
   @XmlElement(
      required = true
   )
   protected String dsc;


   public String getAnm() {
      return this.anm;
   }

   public void setAnm(String value) {
      this.anm = value;
   }

   public String getDsc() {
      return this.dsc;
   }

   public void setDsc(String value) {
      this.dsc = value;
   }
}
