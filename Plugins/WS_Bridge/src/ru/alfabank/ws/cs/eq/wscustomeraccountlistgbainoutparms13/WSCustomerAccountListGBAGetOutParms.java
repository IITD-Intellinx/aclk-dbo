package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSet;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetOutParms",
   propOrder = {"cus", "clc", "cnt", "resultSet"}
)
public class WSCustomerAccountListGBAGetOutParms {

   protected String cus;
   protected String clc;
   protected String cnt;
   @XmlElement(
      required = true
   )
   protected WSCustomerAccountListGBAGetOutResultSet resultSet;


   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getCnt() {
      return this.cnt;
   }

   public void setCnt(String value) {
      this.cnt = value;
   }

   public WSCustomerAccountListGBAGetOutResultSet getResultSet() {
      return this.resultSet;
   }

   public void setResultSet(WSCustomerAccountListGBAGetOutResultSet value) {
      this.resultSet = value;
   }
}
