package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentAddInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentAdd",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSAccountClickPaymentAdd {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentAddInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSAccountClickPaymentAddInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSAccountClickPaymentAddInParms value) {
      this.inParms = value;
   }
}
