package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetOutResultSetRow",
   propOrder = {"ean", "ccy", "ama", "brg", "act", "pwd", "prd", "exp", "sts", "toe", "zzz", "p5P", "ab", "an", "as", "ai12", "ai11", "ai14", "ai17", "ai20", "ai30", "ai47", "ai82", "p3R", "gh", "trz", "ccym", "ccyd", "actd", "p3Rd", "acsn", "acln", "total", "holds", "oad", "ai83", "ai87", "acnm"}
)
public class WSCustomerAccountListGBAGetOutResultSetRow {

   protected String ean;
   protected String ccy;
   protected BigDecimal ama;
   protected String brg;
   protected String act;
   protected BigDecimal pwd;
   protected String prd;
   protected String exp;
   protected String sts;
   protected String toe;
   protected String zzz;
   @XmlElement(
      name = "p5p"
   )
   protected String p5P;
   protected String ab;
   protected String an;
   protected String as;
   protected String ai12;
   protected String ai11;
   protected String ai14;
   protected String ai17;
   protected String ai20;
   protected String ai30;
   protected String ai47;
   protected String ai82;
   @XmlElement(
      name = "p3r"
   )
   protected String p3R;
   protected String gh;
   protected String trz;
   protected String ccym;
   protected String ccyd;
   protected String actd;
   @XmlElement(
      name = "p3rd"
   )
   protected String p3Rd;
   protected String acsn;
   protected String acln;
   protected BigDecimal total;
   protected BigDecimal holds;
   protected XMLGregorianCalendar oad;
   protected String ai83;
   protected String ai87;
   protected String acnm;


   public String getEan() {
      return this.ean;
   }

   public void setEan(String value) {
      this.ean = value;
   }

   public String getCcy() {
      return this.ccy;
   }

   public void setCcy(String value) {
      this.ccy = value;
   }

   public BigDecimal getAma() {
      return this.ama;
   }

   public void setAma(BigDecimal value) {
      this.ama = value;
   }

   public String getBrg() {
      return this.brg;
   }

   public void setBrg(String value) {
      this.brg = value;
   }

   public String getAct() {
      return this.act;
   }

   public void setAct(String value) {
      this.act = value;
   }

   public BigDecimal getPwd() {
      return this.pwd;
   }

   public void setPwd(BigDecimal value) {
      this.pwd = value;
   }

   public String getPrd() {
      return this.prd;
   }

   public void setPrd(String value) {
      this.prd = value;
   }

   public String getExp() {
      return this.exp;
   }

   public void setExp(String value) {
      this.exp = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public String getToe() {
      return this.toe;
   }

   public void setToe(String value) {
      this.toe = value;
   }

   public String getZzz() {
      return this.zzz;
   }

   public void setZzz(String value) {
      this.zzz = value;
   }

   public String getP5P() {
      return this.p5P;
   }

   public void setP5P(String value) {
      this.p5P = value;
   }

   public String getAb() {
      return this.ab;
   }

   public void setAb(String value) {
      this.ab = value;
   }

   public String getAn() {
      return this.an;
   }

   public void setAn(String value) {
      this.an = value;
   }

   public String getAs() {
      return this.as;
   }

   public void setAs(String value) {
      this.as = value;
   }

   public String getAi12() {
      return this.ai12;
   }

   public void setAi12(String value) {
      this.ai12 = value;
   }

   public String getAi11() {
      return this.ai11;
   }

   public void setAi11(String value) {
      this.ai11 = value;
   }

   public String getAi14() {
      return this.ai14;
   }

   public void setAi14(String value) {
      this.ai14 = value;
   }

   public String getAi17() {
      return this.ai17;
   }

   public void setAi17(String value) {
      this.ai17 = value;
   }

   public String getAi20() {
      return this.ai20;
   }

   public void setAi20(String value) {
      this.ai20 = value;
   }

   public String getAi30() {
      return this.ai30;
   }

   public void setAi30(String value) {
      this.ai30 = value;
   }

   public String getAi47() {
      return this.ai47;
   }

   public void setAi47(String value) {
      this.ai47 = value;
   }

   public String getAi82() {
      return this.ai82;
   }

   public void setAi82(String value) {
      this.ai82 = value;
   }

   public String getP3R() {
      return this.p3R;
   }

   public void setP3R(String value) {
      this.p3R = value;
   }

   public String getGh() {
      return this.gh;
   }

   public void setGh(String value) {
      this.gh = value;
   }

   public String getTrz() {
      return this.trz;
   }

   public void setTrz(String value) {
      this.trz = value;
   }

   public String getCcym() {
      return this.ccym;
   }

   public void setCcym(String value) {
      this.ccym = value;
   }

   public String getCcyd() {
      return this.ccyd;
   }

   public void setCcyd(String value) {
      this.ccyd = value;
   }

   public String getActd() {
      return this.actd;
   }

   public void setActd(String value) {
      this.actd = value;
   }

   public String getP3Rd() {
      return this.p3Rd;
   }

   public void setP3Rd(String value) {
      this.p3Rd = value;
   }

   public String getAcsn() {
      return this.acsn;
   }

   public void setAcsn(String value) {
      this.acsn = value;
   }

   public String getAcln() {
      return this.acln;
   }

   public void setAcln(String value) {
      this.acln = value;
   }

   public BigDecimal getTotal() {
      return this.total;
   }

   public void setTotal(BigDecimal value) {
      this.total = value;
   }

   public BigDecimal getHolds() {
      return this.holds;
   }

   public void setHolds(BigDecimal value) {
      this.holds = value;
   }

   public XMLGregorianCalendar getOad() {
      return this.oad;
   }

   public void setOad(XMLGregorianCalendar value) {
      this.oad = value;
   }

   public String getAi83() {
      return this.ai83;
   }

   public void setAi83(String value) {
      this.ai83 = value;
   }

   public String getAi87() {
      return this.ai87;
   }

   public void setAi87(String value) {
      this.ai87 = value;
   }

   public String getAcnm() {
      return this.acnm;
   }

   public void setAcnm(String value) {
      this.acnm = value;
   }
}
