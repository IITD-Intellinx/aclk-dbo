package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentAddOutParms",
   propOrder = {"ref", "sts", "pod", "brn", "pbr", "psq", "hno", "doc", "job", "err", "er0", "er1", "er2"}
)
public class WSAccountClickPaymentAddOutParms {

   protected String ref;
   protected String sts;
   protected XMLGregorianCalendar pod;
   protected String brn;
   protected String pbr;
   protected BigDecimal psq;
   protected BigDecimal hno;
   protected String doc;
   protected String job;
   protected String err;
   protected String er0;
   protected String er1;
   protected String er2;


   public String getRef() {
      return this.ref;
   }

   public void setRef(String value) {
      this.ref = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public XMLGregorianCalendar getPod() {
      return this.pod;
   }

   public void setPod(XMLGregorianCalendar value) {
      this.pod = value;
   }

   public String getBrn() {
      return this.brn;
   }

   public void setBrn(String value) {
      this.brn = value;
   }

   public String getPbr() {
      return this.pbr;
   }

   public void setPbr(String value) {
      this.pbr = value;
   }

   public BigDecimal getPsq() {
      return this.psq;
   }

   public void setPsq(BigDecimal value) {
      this.psq = value;
   }

   public BigDecimal getHno() {
      return this.hno;
   }

   public void setHno(BigDecimal value) {
      this.hno = value;
   }

   public String getDoc() {
      return this.doc;
   }

   public void setDoc(String value) {
      this.doc = value;
   }

   public String getJob() {
      return this.job;
   }

   public void setJob(String value) {
      this.job = value;
   }

   public String getErr() {
      return this.err;
   }

   public void setErr(String value) {
      this.err = value;
   }

   public String getEr0() {
      return this.er0;
   }

   public void setEr0(String value) {
      this.er0 = value;
   }

   public String getEr1() {
      return this.er1;
   }

   public void setEr1(String value) {
      this.er1 = value;
   }

   public String getEr2() {
      return this.er2;
   }

   public void setEr2(String value) {
      this.er2 = value;
   }
}
