package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLGet",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSCustomerExtendedInfoCLGet {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSCustomerExtendedInfoCLGetInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSCustomerExtendedInfoCLGetInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSCustomerExtendedInfoCLGetInParms value) {
      this.inParms = value;
   }
}
