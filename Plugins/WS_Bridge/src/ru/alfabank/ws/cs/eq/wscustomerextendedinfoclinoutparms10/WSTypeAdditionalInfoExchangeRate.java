package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSTypeAdditionalInfoExchangeRate",
   propOrder = {"adt"}
)
public class WSTypeAdditionalInfoExchangeRate extends WSTypeAdditionalInfo {

   @XmlElement(
      required = true
   )
   protected BigDecimal adt;


   public BigDecimal getAdt() {
      return this.adt;
   }

   public void setAdt(BigDecimal value) {
      this.adt = value;
   }
}
