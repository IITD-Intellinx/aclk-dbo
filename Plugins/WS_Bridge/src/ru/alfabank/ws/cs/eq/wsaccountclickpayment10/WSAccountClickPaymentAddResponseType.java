package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentAddOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentAddResponseType",
   propOrder = {"outCommonParms", "outParms"}
)
public class WSAccountClickPaymentAddResponseType {

   @XmlElement(
      required = true
   )
   protected WSCommonOutParms outCommonParms;
   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentAddOutParms outParms;


   public WSCommonOutParms getOutCommonParms() {
      return this.outCommonParms;
   }

   public void setOutCommonParms(WSCommonOutParms value) {
      this.outCommonParms = value;
   }

   public WSAccountClickPaymentAddOutParms getOutParms() {
      return this.outParms;
   }

   public void setOutParms(WSAccountClickPaymentAddOutParms value) {
      this.outParms = value;
   }
}
