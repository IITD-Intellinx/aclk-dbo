package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGet",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSPaymentBindingListGet {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSPaymentBindingListGetInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSPaymentBindingListGetInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSPaymentBindingListGetInParms value) {
      this.inParms = value;
   }
}
