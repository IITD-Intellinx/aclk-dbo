package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSVerificationException;

@WebFault(
   name = "WSVerificationException",
   targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru"
)
public class MsgWSVerificationException extends Exception {

   private WSVerificationException faultInfo;


   public MsgWSVerificationException(String message, WSVerificationException faultInfo) {
      super(message);
      this.faultInfo = faultInfo;
   }

   public MsgWSVerificationException(String message, WSVerificationException faultInfo, Throwable cause) {
      super(message, cause);
      this.faultInfo = faultInfo;
   }

   public WSVerificationException getFaultInfo() {
      return this.faultInfo;
   }
}
