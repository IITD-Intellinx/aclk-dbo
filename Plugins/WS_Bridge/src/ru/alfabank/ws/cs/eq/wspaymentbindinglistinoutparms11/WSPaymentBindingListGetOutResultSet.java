package ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutResultSetRow;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetOutResultSet",
   propOrder = {"resultSetRow"}
)
public class WSPaymentBindingListGetOutResultSet {

   protected List<WSPaymentBindingListGetOutResultSetRow> resultSetRow;


   public List<WSPaymentBindingListGetOutResultSetRow> getResultSetRow() {
      if(this.resultSetRow == null) {
         this.resultSetRow = new ArrayList();
      }

      return this.resultSetRow;
   }
}
