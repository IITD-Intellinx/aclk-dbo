package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.MsgWSAccessException;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.MsgWSAppException;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.MsgWSAppTechnicalException;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.MsgWSTechnicalException;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.MsgWSVerificationException;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.ObjectFactory;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGetResponseType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@WebService(
   name = "WSCustomerAccountListGBA13PortType",
   targetNamespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru"
)
@XmlSeeAlso({ru.alfabank.ws.cs.wscommontypes10.ObjectFactory.class, ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.ObjectFactory.class, ObjectFactory.class})
public interface WSCustomerAccountListGBA13PortType {

   @WebMethod(
      operationName = "WSCustomerAccountListGBAGet",
      action = "/CS/EQ/WSCustomerAccountListGBA13#Get"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSCustomerAccountListGBAGet",
      targetNamespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGet"
   )
   @ResponseWrapper(
      localName = "WSCustomerAccountListGBAGetResponse",
      targetNamespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGetResponse"
   )
   WSCustomerAccountListGBAGetResponseType wsCustomerAccountListGBAGet(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSCustomerAccountListGBAGetInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;
}
