package ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetOutResultSetRow",
   propOrder = {"wid", "ean", "sts", "drf", "acb", "ccy", "act", "ab", "an", "as", "dte", "tme", "ps", "acnm", "wnm"}
)
public class WSPaymentBindingListGetOutResultSetRow {

   protected String wid;
   protected String ean;
   protected String sts;
   protected String drf;
   protected BigDecimal acb;
   protected String ccy;
   protected String act;
   protected String ab;
   protected String an;
   protected String as;
   protected XMLGregorianCalendar dte;
   protected String tme;
   protected String ps;
   protected String acnm;
   protected String wnm;


   public String getWid() {
      return this.wid;
   }

   public void setWid(String value) {
      this.wid = value;
   }

   public String getEan() {
      return this.ean;
   }

   public void setEan(String value) {
      this.ean = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public String getDrf() {
      return this.drf;
   }

   public void setDrf(String value) {
      this.drf = value;
   }

   public BigDecimal getAcb() {
      return this.acb;
   }

   public void setAcb(BigDecimal value) {
      this.acb = value;
   }

   public String getCcy() {
      return this.ccy;
   }

   public void setCcy(String value) {
      this.ccy = value;
   }

   public String getAct() {
      return this.act;
   }

   public void setAct(String value) {
      this.act = value;
   }

   public String getAb() {
      return this.ab;
   }

   public void setAb(String value) {
      this.ab = value;
   }

   public String getAn() {
      return this.an;
   }

   public void setAn(String value) {
      this.an = value;
   }

   public String getAs() {
      return this.as;
   }

   public void setAs(String value) {
      this.as = value;
   }

   public XMLGregorianCalendar getDte() {
      return this.dte;
   }

   public void setDte(XMLGregorianCalendar value) {
      this.dte = value;
   }

   public String getTme() {
      return this.tme;
   }

   public void setTme(String value) {
      this.tme = value;
   }

   public String getPs() {
      return this.ps;
   }

   public void setPs(String value) {
      this.ps = value;
   }

   public String getAcnm() {
      return this.acnm;
   }

   public void setAcnm(String value) {
      this.acnm = value;
   }

   public String getWnm() {
      return this.wnm;
   }

   public void setWnm(String value) {
      this.wnm = value;
   }
}
