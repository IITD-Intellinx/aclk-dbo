package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetResponseType",
   propOrder = {"outCommonParms", "outParms"}
)
public class WSPaymentBindingListGetResponseType {

   @XmlElement(
      required = true
   )
   protected WSCommonOutParms outCommonParms;
   @XmlElement(
      required = true
   )
   protected WSPaymentBindingListGetOutParms outParms;


   public WSCommonOutParms getOutCommonParms() {
      return this.outCommonParms;
   }

   public void setOutCommonParms(WSCommonOutParms value) {
      this.outCommonParms = value;
   }

   public WSPaymentBindingListGetOutParms getOutParms() {
      return this.outParms;
   }

   public void setOutParms(WSPaymentBindingListGetOutParms value) {
      this.outParms = value;
   }
}
