package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLResponse",
   propOrder = {"response"}
)
public class WSCustomerExtendedInfoCLResponse {

   @XmlElement(
      required = true
   )
   protected WSCustomerExtendedInfoCLResponseType response;


   public WSCustomerExtendedInfoCLResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSCustomerExtendedInfoCLResponseType value) {
      this.response = value;
   }
}
