package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSTypeAdditionalInfoResultSet",
   propOrder = {"resultSetRow"}
)
public class WSTypeAdditionalInfoResultSet {

   protected List<WSTypeAdditionalInfo> resultSetRow;


   public List<WSTypeAdditionalInfo> getResultSetRow() {
      if(this.resultSetRow == null) {
         this.resultSetRow = new ArrayList();
      }

      return this.resultSetRow;
   }
}
