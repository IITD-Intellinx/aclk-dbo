package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAddResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentAddResponse",
   propOrder = {"response"}
)
public class WSAccountClickPaymentAddResponse {

   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentAddResponseType response;


   public WSAccountClickPaymentAddResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSAccountClickPaymentAddResponseType value) {
      this.response = value;
   }
}
