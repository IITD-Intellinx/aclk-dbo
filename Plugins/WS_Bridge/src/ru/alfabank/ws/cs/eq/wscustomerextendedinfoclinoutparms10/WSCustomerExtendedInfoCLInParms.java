package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLInParms",
   propOrder = {"cus", "clc", "phcl", "opcl", "dacl", "tmcl", "uicl", "imcl", "daim", "tmim", "eml", "daem", "tmem", "dsgn"}
)
public class WSCustomerExtendedInfoCLInParms {

   protected String cus;
   protected String clc;
   protected String phcl;
   protected String opcl;
   protected XMLGregorianCalendar dacl;
   protected String tmcl;
   protected String uicl;
   protected String imcl;
   protected XMLGregorianCalendar daim;
   protected String tmim;
   protected String eml;
   protected XMLGregorianCalendar daem;
   protected String tmem;
   protected String dsgn;


   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getPhcl() {
      return this.phcl;
   }

   public void setPhcl(String value) {
      this.phcl = value;
   }

   public String getOpcl() {
      return this.opcl;
   }

   public void setOpcl(String value) {
      this.opcl = value;
   }

   public XMLGregorianCalendar getDacl() {
      return this.dacl;
   }

   public void setDacl(XMLGregorianCalendar value) {
      this.dacl = value;
   }

   public String getTmcl() {
      return this.tmcl;
   }

   public void setTmcl(String value) {
      this.tmcl = value;
   }

   public String getUicl() {
      return this.uicl;
   }

   public void setUicl(String value) {
      this.uicl = value;
   }

   public String getImcl() {
      return this.imcl;
   }

   public void setImcl(String value) {
      this.imcl = value;
   }

   public XMLGregorianCalendar getDaim() {
      return this.daim;
   }

   public void setDaim(XMLGregorianCalendar value) {
      this.daim = value;
   }

   public String getTmim() {
      return this.tmim;
   }

   public void setTmim(String value) {
      this.tmim = value;
   }

   public String getEml() {
      return this.eml;
   }

   public void setEml(String value) {
      this.eml = value;
   }

   public XMLGregorianCalendar getDaem() {
      return this.daem;
   }

   public void setDaem(XMLGregorianCalendar value) {
      this.daem = value;
   }

   public String getTmem() {
      return this.tmem;
   }

   public void setTmem(String value) {
      this.tmem = value;
   }

   public String getDsgn() {
      return this.dsgn;
   }

   public void setDsgn(String value) {
      this.dsgn = value;
   }
}
