package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLAdd;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLDelete;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGet;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGetResponse;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGetResponseType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLMaintain;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponse;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponseType;

@XmlRegistry
public class ObjectFactory {

   private static final QName _WSCustomerExtendedInfoCLDelete_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLDelete");
   private static final QName _WSCustomerExtendedInfoCLMaintain_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLMaintain");
   private static final QName _WSCustomerExtendedInfoCLGet_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLGet");
   private static final QName _WSCustomerExtendedInfoCLGetResponse_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLGetResponse");
   private static final QName _WSCustomerExtendedInfoCLResponse_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLResponse");
   private static final QName _WSCustomerExtendedInfoCLAdd_QNAME = new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCLAdd");


   public WSCustomerExtendedInfoCLGet createWSCustomerExtendedInfoCLGet() {
      return new WSCustomerExtendedInfoCLGet();
   }

   public WSCustomerExtendedInfoCLAdd createWSCustomerExtendedInfoCLAdd() {
      return new WSCustomerExtendedInfoCLAdd();
   }

   public WSCustomerExtendedInfoCLDelete createWSCustomerExtendedInfoCLDelete() {
      return new WSCustomerExtendedInfoCLDelete();
   }

   public WSCustomerExtendedInfoCLGetResponse createWSCustomerExtendedInfoCLGetResponse() {
      return new WSCustomerExtendedInfoCLGetResponse();
   }

   public WSCustomerExtendedInfoCLResponse createWSCustomerExtendedInfoCLResponse() {
      return new WSCustomerExtendedInfoCLResponse();
   }

   public WSCustomerExtendedInfoCLMaintain createWSCustomerExtendedInfoCLMaintain() {
      return new WSCustomerExtendedInfoCLMaintain();
   }

   public WSCustomerExtendedInfoCLGetResponseType createWSCustomerExtendedInfoCLGetResponseType() {
      return new WSCustomerExtendedInfoCLGetResponseType();
   }

   public WSCustomerExtendedInfoCLResponseType createWSCustomerExtendedInfoCLResponseType() {
      return new WSCustomerExtendedInfoCLResponseType();
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLDelete"
   )
   public JAXBElement<WSCustomerExtendedInfoCLDelete> createWSCustomerExtendedInfoCLDelete(WSCustomerExtendedInfoCLDelete value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLDelete_QNAME, WSCustomerExtendedInfoCLDelete.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLMaintain"
   )
   public JAXBElement<WSCustomerExtendedInfoCLMaintain> createWSCustomerExtendedInfoCLMaintain(WSCustomerExtendedInfoCLMaintain value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLMaintain_QNAME, WSCustomerExtendedInfoCLMaintain.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLGet"
   )
   public JAXBElement<WSCustomerExtendedInfoCLGet> createWSCustomerExtendedInfoCLGet(WSCustomerExtendedInfoCLGet value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLGet_QNAME, WSCustomerExtendedInfoCLGet.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLGetResponse"
   )
   public JAXBElement<WSCustomerExtendedInfoCLGetResponse> createWSCustomerExtendedInfoCLGetResponse(WSCustomerExtendedInfoCLGetResponse value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLGetResponse_QNAME, WSCustomerExtendedInfoCLGetResponse.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLResponse"
   )
   public JAXBElement<WSCustomerExtendedInfoCLResponse> createWSCustomerExtendedInfoCLResponse(WSCustomerExtendedInfoCLResponse value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLResponse_QNAME, WSCustomerExtendedInfoCLResponse.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerExtendedInfoCLAdd"
   )
   public JAXBElement<WSCustomerExtendedInfoCLAdd> createWSCustomerExtendedInfoCLAdd(WSCustomerExtendedInfoCLAdd value) {
      return new JAXBElement(_WSCustomerExtendedInfoCLAdd_QNAME, WSCustomerExtendedInfoCLAdd.class, (Class)null, value);
   }
}
