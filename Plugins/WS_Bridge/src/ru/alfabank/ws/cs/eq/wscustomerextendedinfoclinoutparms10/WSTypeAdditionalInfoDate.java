package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfo;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSTypeAdditionalInfoDate",
   propOrder = {"adt"}
)
public class WSTypeAdditionalInfoDate extends WSTypeAdditionalInfo {

   @XmlElement(
      required = true
   )
   protected XMLGregorianCalendar adt;


   public XMLGregorianCalendar getAdt() {
      return this.adt;
   }

   public void setAdt(XMLGregorianCalendar value) {
      this.adt = value;
   }
}
