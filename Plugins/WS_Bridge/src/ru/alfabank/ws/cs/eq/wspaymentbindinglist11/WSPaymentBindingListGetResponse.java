package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGetResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetResponse",
   propOrder = {"response"}
)
public class WSPaymentBindingListGetResponse {

   @XmlElement(
      required = true
   )
   protected WSPaymentBindingListGetResponseType response;


   public WSPaymentBindingListGetResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSPaymentBindingListGetResponseType value) {
      this.response = value;
   }
}
