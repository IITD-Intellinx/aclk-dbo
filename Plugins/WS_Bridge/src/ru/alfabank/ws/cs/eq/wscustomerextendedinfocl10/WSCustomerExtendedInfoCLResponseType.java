package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLResponseType",
   propOrder = {"outCommonParms"}
)
public class WSCustomerExtendedInfoCLResponseType {

   @XmlElement(
      required = true
   )
   protected WSCommonOutParms outCommonParms;


   public WSCommonOutParms getOutCommonParms() {
      return this.outCommonParms;
   }

   public void setOutCommonParms(WSCommonOutParms value) {
      this.outCommonParms = value;
   }
}
