package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSAppException;

@WebFault(
   name = "WSAppException",
   targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru"
)
public class MsgWSAppException extends Exception {

   private WSAppException faultInfo;


   public MsgWSAppException(String message, WSAppException faultInfo) {
      super(message);
      this.faultInfo = faultInfo;
   }

   public MsgWSAppException(String message, WSAppException faultInfo, Throwable cause) {
      super(message, cause);
      this.faultInfo = faultInfo;
   }

   public WSAppException getFaultInfo() {
      return this.faultInfo;
   }
}
