package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoResultSet;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLGetOutParms",
   propOrder = {"aig", "cus", "clc", "cun", "cpnc", "das", "c1R", "c2R", "c3R", "c4R", "c5R", "p1R", "p2R", "p3R", "p4R", "p5R", "ctp", "cub", "cuc", "cud", "cuz", "sac", "aco", "crf", "lnm", "ca2", "cnap", "cnar", "cnal", "cod", "dcc", "dlm", "itrt", "brnm", "crb1", "crb2", "adj", "ercp", "ercc", "drc", "grps", "cuna", "dasa", "cunm", "cnai", "grp", "mtb", "etx", "yfon", "dfrq", "fon", "fol", "del", "stmp", "cref", "oatp", "occl", "hdd", "dded", "rddh", "ytri", "yret", "ypla", "yopi", "ynet", "yri1", "yri2", "yri3", "yri4", "pcus", "pclc", "cs", "cfrq", "fcyc", "cssa", "pstm", "nstm", "stn", "ocid", "clsf", "clst", "cltv", "cltp", "drty", "mrty", "seid", "cst", "auno", "resultSet"}
)
public class WSCustomerExtendedInfoCLGetOutParms {

   protected String aig;
   protected String cus;
   protected String clc;
   protected String cun;
   protected String cpnc;
   protected String das;
   @XmlElement(
      name = "c1r"
   )
   protected String c1R;
   @XmlElement(
      name = "c2r"
   )
   protected String c2R;
   @XmlElement(
      name = "c3r"
   )
   protected String c3R;
   @XmlElement(
      name = "c4r"
   )
   protected String c4R;
   @XmlElement(
      name = "c5r"
   )
   protected String c5R;
   @XmlElement(
      name = "p1r"
   )
   protected String p1R;
   @XmlElement(
      name = "p2r"
   )
   protected String p2R;
   @XmlElement(
      name = "p3r"
   )
   protected String p3R;
   @XmlElement(
      name = "p4r"
   )
   protected String p4R;
   @XmlElement(
      name = "p5r"
   )
   protected String p5R;
   protected String ctp;
   protected String cub;
   protected String cuc;
   protected String cud;
   protected String cuz;
   protected String sac;
   protected String aco;
   protected String crf;
   protected String lnm;
   protected String ca2;
   protected String cnap;
   protected String cnar;
   protected String cnal;
   protected XMLGregorianCalendar cod;
   protected XMLGregorianCalendar dcc;
   protected XMLGregorianCalendar dlm;
   protected BigDecimal itrt;
   protected String brnm;
   protected String crb1;
   protected String crb2;
   protected BigDecimal adj;
   protected String ercp;
   protected String ercc;
   protected String drc;
   protected String grps;
   protected String cuna;
   protected String dasa;
   protected String cunm;
   protected String cnai;
   protected String grp;
   protected String mtb;
   protected String etx;
   protected String yfon;
   protected BigDecimal dfrq;
   protected String fon;
   protected String fol;
   protected String del;
   protected String stmp;
   protected String cref;
   protected String oatp;
   protected String occl;
   protected String hdd;
   protected XMLGregorianCalendar dded;
   protected String rddh;
   protected String ytri;
   protected String yret;
   protected String ypla;
   protected String yopi;
   protected String ynet;
   protected String yri1;
   protected String yri2;
   protected String yri3;
   protected String yri4;
   protected String pcus;
   protected String pclc;
   protected String cs;
   protected String cfrq;
   protected String fcyc;
   protected String cssa;
   protected XMLGregorianCalendar pstm;
   protected XMLGregorianCalendar nstm;
   protected BigDecimal stn;
   protected String ocid;
   protected XMLGregorianCalendar clsf;
   protected XMLGregorianCalendar clst;
   protected String cltv;
   protected String cltp;
   protected String drty;
   protected String mrty;
   protected String seid;
   protected String cst;
   protected BigDecimal auno;
   protected WSTypeAdditionalInfoResultSet resultSet;


   public String getAig() {
      return this.aig;
   }

   public void setAig(String value) {
      this.aig = value;
   }

   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getCun() {
      return this.cun;
   }

   public void setCun(String value) {
      this.cun = value;
   }

   public String getCpnc() {
      return this.cpnc;
   }

   public void setCpnc(String value) {
      this.cpnc = value;
   }

   public String getDas() {
      return this.das;
   }

   public void setDas(String value) {
      this.das = value;
   }

   public String getC1R() {
      return this.c1R;
   }

   public void setC1R(String value) {
      this.c1R = value;
   }

   public String getC2R() {
      return this.c2R;
   }

   public void setC2R(String value) {
      this.c2R = value;
   }

   public String getC3R() {
      return this.c3R;
   }

   public void setC3R(String value) {
      this.c3R = value;
   }

   public String getC4R() {
      return this.c4R;
   }

   public void setC4R(String value) {
      this.c4R = value;
   }

   public String getC5R() {
      return this.c5R;
   }

   public void setC5R(String value) {
      this.c5R = value;
   }

   public String getP1R() {
      return this.p1R;
   }

   public void setP1R(String value) {
      this.p1R = value;
   }

   public String getP2R() {
      return this.p2R;
   }

   public void setP2R(String value) {
      this.p2R = value;
   }

   public String getP3R() {
      return this.p3R;
   }

   public void setP3R(String value) {
      this.p3R = value;
   }

   public String getP4R() {
      return this.p4R;
   }

   public void setP4R(String value) {
      this.p4R = value;
   }

   public String getP5R() {
      return this.p5R;
   }

   public void setP5R(String value) {
      this.p5R = value;
   }

   public String getCtp() {
      return this.ctp;
   }

   public void setCtp(String value) {
      this.ctp = value;
   }

   public String getCub() {
      return this.cub;
   }

   public void setCub(String value) {
      this.cub = value;
   }

   public String getCuc() {
      return this.cuc;
   }

   public void setCuc(String value) {
      this.cuc = value;
   }

   public String getCud() {
      return this.cud;
   }

   public void setCud(String value) {
      this.cud = value;
   }

   public String getCuz() {
      return this.cuz;
   }

   public void setCuz(String value) {
      this.cuz = value;
   }

   public String getSac() {
      return this.sac;
   }

   public void setSac(String value) {
      this.sac = value;
   }

   public String getAco() {
      return this.aco;
   }

   public void setAco(String value) {
      this.aco = value;
   }

   public String getCrf() {
      return this.crf;
   }

   public void setCrf(String value) {
      this.crf = value;
   }

   public String getLnm() {
      return this.lnm;
   }

   public void setLnm(String value) {
      this.lnm = value;
   }

   public String getCa2() {
      return this.ca2;
   }

   public void setCa2(String value) {
      this.ca2 = value;
   }

   public String getCnap() {
      return this.cnap;
   }

   public void setCnap(String value) {
      this.cnap = value;
   }

   public String getCnar() {
      return this.cnar;
   }

   public void setCnar(String value) {
      this.cnar = value;
   }

   public String getCnal() {
      return this.cnal;
   }

   public void setCnal(String value) {
      this.cnal = value;
   }

   public XMLGregorianCalendar getCod() {
      return this.cod;
   }

   public void setCod(XMLGregorianCalendar value) {
      this.cod = value;
   }

   public XMLGregorianCalendar getDcc() {
      return this.dcc;
   }

   public void setDcc(XMLGregorianCalendar value) {
      this.dcc = value;
   }

   public XMLGregorianCalendar getDlm() {
      return this.dlm;
   }

   public void setDlm(XMLGregorianCalendar value) {
      this.dlm = value;
   }

   public BigDecimal getItrt() {
      return this.itrt;
   }

   public void setItrt(BigDecimal value) {
      this.itrt = value;
   }

   public String getBrnm() {
      return this.brnm;
   }

   public void setBrnm(String value) {
      this.brnm = value;
   }

   public String getCrb1() {
      return this.crb1;
   }

   public void setCrb1(String value) {
      this.crb1 = value;
   }

   public String getCrb2() {
      return this.crb2;
   }

   public void setCrb2(String value) {
      this.crb2 = value;
   }

   public BigDecimal getAdj() {
      return this.adj;
   }

   public void setAdj(BigDecimal value) {
      this.adj = value;
   }

   public String getErcp() {
      return this.ercp;
   }

   public void setErcp(String value) {
      this.ercp = value;
   }

   public String getErcc() {
      return this.ercc;
   }

   public void setErcc(String value) {
      this.ercc = value;
   }

   public String getDrc() {
      return this.drc;
   }

   public void setDrc(String value) {
      this.drc = value;
   }

   public String getGrps() {
      return this.grps;
   }

   public void setGrps(String value) {
      this.grps = value;
   }

   public String getCuna() {
      return this.cuna;
   }

   public void setCuna(String value) {
      this.cuna = value;
   }

   public String getDasa() {
      return this.dasa;
   }

   public void setDasa(String value) {
      this.dasa = value;
   }

   public String getCunm() {
      return this.cunm;
   }

   public void setCunm(String value) {
      this.cunm = value;
   }

   public String getCnai() {
      return this.cnai;
   }

   public void setCnai(String value) {
      this.cnai = value;
   }

   public String getGrp() {
      return this.grp;
   }

   public void setGrp(String value) {
      this.grp = value;
   }

   public String getMtb() {
      return this.mtb;
   }

   public void setMtb(String value) {
      this.mtb = value;
   }

   public String getEtx() {
      return this.etx;
   }

   public void setEtx(String value) {
      this.etx = value;
   }

   public String getYfon() {
      return this.yfon;
   }

   public void setYfon(String value) {
      this.yfon = value;
   }

   public BigDecimal getDfrq() {
      return this.dfrq;
   }

   public void setDfrq(BigDecimal value) {
      this.dfrq = value;
   }

   public String getFon() {
      return this.fon;
   }

   public void setFon(String value) {
      this.fon = value;
   }

   public String getFol() {
      return this.fol;
   }

   public void setFol(String value) {
      this.fol = value;
   }

   public String getDel() {
      return this.del;
   }

   public void setDel(String value) {
      this.del = value;
   }

   public String getStmp() {
      return this.stmp;
   }

   public void setStmp(String value) {
      this.stmp = value;
   }

   public String getCref() {
      return this.cref;
   }

   public void setCref(String value) {
      this.cref = value;
   }

   public String getOatp() {
      return this.oatp;
   }

   public void setOatp(String value) {
      this.oatp = value;
   }

   public String getOccl() {
      return this.occl;
   }

   public void setOccl(String value) {
      this.occl = value;
   }

   public String getHdd() {
      return this.hdd;
   }

   public void setHdd(String value) {
      this.hdd = value;
   }

   public XMLGregorianCalendar getDded() {
      return this.dded;
   }

   public void setDded(XMLGregorianCalendar value) {
      this.dded = value;
   }

   public String getRddh() {
      return this.rddh;
   }

   public void setRddh(String value) {
      this.rddh = value;
   }

   public String getYtri() {
      return this.ytri;
   }

   public void setYtri(String value) {
      this.ytri = value;
   }

   public String getYret() {
      return this.yret;
   }

   public void setYret(String value) {
      this.yret = value;
   }

   public String getYpla() {
      return this.ypla;
   }

   public void setYpla(String value) {
      this.ypla = value;
   }

   public String getYopi() {
      return this.yopi;
   }

   public void setYopi(String value) {
      this.yopi = value;
   }

   public String getYnet() {
      return this.ynet;
   }

   public void setYnet(String value) {
      this.ynet = value;
   }

   public String getYri1() {
      return this.yri1;
   }

   public void setYri1(String value) {
      this.yri1 = value;
   }

   public String getYri2() {
      return this.yri2;
   }

   public void setYri2(String value) {
      this.yri2 = value;
   }

   public String getYri3() {
      return this.yri3;
   }

   public void setYri3(String value) {
      this.yri3 = value;
   }

   public String getYri4() {
      return this.yri4;
   }

   public void setYri4(String value) {
      this.yri4 = value;
   }

   public String getPcus() {
      return this.pcus;
   }

   public void setPcus(String value) {
      this.pcus = value;
   }

   public String getPclc() {
      return this.pclc;
   }

   public void setPclc(String value) {
      this.pclc = value;
   }

   public String getCs() {
      return this.cs;
   }

   public void setCs(String value) {
      this.cs = value;
   }

   public String getCfrq() {
      return this.cfrq;
   }

   public void setCfrq(String value) {
      this.cfrq = value;
   }

   public String getFcyc() {
      return this.fcyc;
   }

   public void setFcyc(String value) {
      this.fcyc = value;
   }

   public String getCssa() {
      return this.cssa;
   }

   public void setCssa(String value) {
      this.cssa = value;
   }

   public XMLGregorianCalendar getPstm() {
      return this.pstm;
   }

   public void setPstm(XMLGregorianCalendar value) {
      this.pstm = value;
   }

   public XMLGregorianCalendar getNstm() {
      return this.nstm;
   }

   public void setNstm(XMLGregorianCalendar value) {
      this.nstm = value;
   }

   public BigDecimal getStn() {
      return this.stn;
   }

   public void setStn(BigDecimal value) {
      this.stn = value;
   }

   public String getOcid() {
      return this.ocid;
   }

   public void setOcid(String value) {
      this.ocid = value;
   }

   public XMLGregorianCalendar getClsf() {
      return this.clsf;
   }

   public void setClsf(XMLGregorianCalendar value) {
      this.clsf = value;
   }

   public XMLGregorianCalendar getClst() {
      return this.clst;
   }

   public void setClst(XMLGregorianCalendar value) {
      this.clst = value;
   }

   public String getCltv() {
      return this.cltv;
   }

   public void setCltv(String value) {
      this.cltv = value;
   }

   public String getCltp() {
      return this.cltp;
   }

   public void setCltp(String value) {
      this.cltp = value;
   }

   public String getDrty() {
      return this.drty;
   }

   public void setDrty(String value) {
      this.drty = value;
   }

   public String getMrty() {
      return this.mrty;
   }

   public void setMrty(String value) {
      this.mrty = value;
   }

   public String getSeid() {
      return this.seid;
   }

   public void setSeid(String value) {
      this.seid = value;
   }

   public String getCst() {
      return this.cst;
   }

   public void setCst(String value) {
      this.cst = value;
   }

   public BigDecimal getAuno() {
      return this.auno;
   }

   public void setAuno(BigDecimal value) {
      this.auno = value;
   }

   public WSTypeAdditionalInfoResultSet getResultSet() {
      return this.resultSet;
   }

   public void setResultSet(WSTypeAdditionalInfoResultSet value) {
      this.resultSet = value;
   }
}
