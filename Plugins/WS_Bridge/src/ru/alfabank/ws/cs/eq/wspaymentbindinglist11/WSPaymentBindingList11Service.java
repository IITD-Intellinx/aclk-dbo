package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingList11PortType;

@WebServiceClient(
   name = "WSPaymentBindingList11Service",
   targetNamespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru",
   wsdlLocation = "file:/E:/SharedData/2012-05-18/WSPaymentBindingList11/EQ/WSPaymentBindingList11.wsdl"
)
public class WSPaymentBindingList11Service extends Service {

   private static final URL WSPAYMENTBINDINGLIST11SERVICE_WSDL_LOCATION;
   private static final Logger logger = Logger.getLogger(WSPaymentBindingList11Service.class.getName());


   static {
      URL url = null;

      try {
         URL e = WSPaymentBindingList11Service.class.getResource(".");
         url = new URL(e, "file:/E:/SharedData/2012-05-18/WSPaymentBindingList11/EQ/WSPaymentBindingList11.wsdl");
      } catch (MalformedURLException var2) {
         logger.warning("Failed to create URL for the wsdl Location: \'file:/E:/SharedData/2012-05-18/WSPaymentBindingList11/EQ/WSPaymentBindingList11.wsdl\', retrying as a local file");
         logger.warning(var2.getMessage());
      }

      WSPAYMENTBINDINGLIST11SERVICE_WSDL_LOCATION = url;
   }

   public WSPaymentBindingList11Service(URL wsdlLocation, QName serviceName) {
      super(wsdlLocation, serviceName);
   }

   public WSPaymentBindingList11Service() {
      super(WSPAYMENTBINDINGLIST11SERVICE_WSDL_LOCATION, new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Service"));
   }

   @WebEndpoint(
      name = "WSPaymentBindingList11Port"
   )
   public WSPaymentBindingList11PortType getWSPaymentBindingList11Port() {
      return (WSPaymentBindingList11PortType)super.getPort(new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Port"), WSPaymentBindingList11PortType.class);
   }

   @WebEndpoint(
      name = "WSPaymentBindingList11Port"
   )
   public WSPaymentBindingList11PortType getWSPaymentBindingList11Port(WebServiceFeature ... features) {
      return (WSPaymentBindingList11PortType)super.getPort(new QName("http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru", "WSPaymentBindingList11Port"), WSPaymentBindingList11PortType.class, features);
   }
}
