package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetInParms",
   propOrder = {"cus", "clc", "ext", "typ", "sql", "lnm"}
)
public class WSCustomerAccountListGBAGetInParms {

   protected String cus;
   protected String clc;
   protected String ext;
   protected String typ;
   protected String sql;
   protected String lnm;


   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getExt() {
      return this.ext;
   }

   public void setExt(String value) {
      this.ext = value;
   }

   public String getTyp() {
      return this.typ;
   }

   public void setTyp(String value) {
      this.typ = value;
   }

   public String getSql() {
      return this.sql;
   }

   public void setSql(String value) {
      this.sql = value;
   }

   public String getLnm() {
      return this.lnm;
   }

   public void setLnm(String value) {
      this.lnm = value;
   }
}
