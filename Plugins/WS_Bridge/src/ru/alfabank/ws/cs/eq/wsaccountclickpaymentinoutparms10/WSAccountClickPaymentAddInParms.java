package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ru.alfabank.ws.cs.wscommontypes10.WSTypeAddInfoList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentAddInParms",
   propOrder = {"ref", "typ", "rte", "sts", "inp", "xm", "ps", "rcp", "abd", "and", "asd", "end", "d17", "abc", "anc", "asc", "enc", "c17", "ama", "fee", "ccy", "pod", "brn", "pbr", "psq", "hno", "aco", "hrc", "dts", "dte", "tcd", "tcc", "nar", "hdd", "cus", "clc", "rfe", "doc", "tic", "bnd", "sor", "dlm", "ulm", "tms", "mac", "clk", "tin", "job", "err", "er0", "er1", "er2", "additionalInfo"}
)
public class WSAccountClickPaymentAddInParms {

   protected String ref;
   protected String typ;
   protected String rte;
   protected String sts;
   protected XMLGregorianCalendar inp;
   protected String xm;
   protected String ps;
   protected String rcp;
   protected String abd;
   protected String and;
   protected String asd;
   protected String end;
   protected String d17;
   protected String abc;
   protected String anc;
   protected String asc;
   protected String enc;
   protected String c17;
   protected BigDecimal ama;
   protected BigDecimal fee;
   protected String ccy;
   protected XMLGregorianCalendar pod;
   protected String brn;
   protected String pbr;
   protected BigDecimal psq;
   protected BigDecimal hno;
   protected String aco;
   protected String hrc;
   protected XMLGregorianCalendar dts;
   protected XMLGregorianCalendar dte;
   protected String tcd;
   protected String tcc;
   protected String nar;
   protected String hdd;
   protected String cus;
   protected String clc;
   protected String rfe;
   protected String doc;
   protected String tic;
   protected String bnd;
   protected String sor;
   protected XMLGregorianCalendar dlm;
   protected String ulm;
   protected XMLGregorianCalendar tms;
   protected String mac;
   protected XMLGregorianCalendar clk;
   protected XMLGregorianCalendar tin;
   protected String job;
   protected String err;
   protected String er0;
   protected String er1;
   protected String er2;
   @XmlElement(
      required = true
   )
   protected WSTypeAddInfoList additionalInfo;


   public String getRef() {
      return this.ref;
   }

   public void setRef(String value) {
      this.ref = value;
   }

   public String getTyp() {
      return this.typ;
   }

   public void setTyp(String value) {
      this.typ = value;
   }

   public String getRte() {
      return this.rte;
   }

   public void setRte(String value) {
      this.rte = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public XMLGregorianCalendar getInp() {
      return this.inp;
   }

   public void setInp(XMLGregorianCalendar value) {
      this.inp = value;
   }

   public String getXm() {
      return this.xm;
   }

   public void setXm(String value) {
      this.xm = value;
   }

   public String getPs() {
      return this.ps;
   }

   public void setPs(String value) {
      this.ps = value;
   }

   public String getRcp() {
      return this.rcp;
   }

   public void setRcp(String value) {
      this.rcp = value;
   }

   public String getAbd() {
      return this.abd;
   }

   public void setAbd(String value) {
      this.abd = value;
   }

   public String getAnd() {
      return this.and;
   }

   public void setAnd(String value) {
      this.and = value;
   }

   public String getAsd() {
      return this.asd;
   }

   public void setAsd(String value) {
      this.asd = value;
   }

   public String getEnd() {
      return this.end;
   }

   public void setEnd(String value) {
      this.end = value;
   }

   public String getD17() {
      return this.d17;
   }

   public void setD17(String value) {
      this.d17 = value;
   }

   public String getAbc() {
      return this.abc;
   }

   public void setAbc(String value) {
      this.abc = value;
   }

   public String getAnc() {
      return this.anc;
   }

   public void setAnc(String value) {
      this.anc = value;
   }

   public String getAsc() {
      return this.asc;
   }

   public void setAsc(String value) {
      this.asc = value;
   }

   public String getEnc() {
      return this.enc;
   }

   public void setEnc(String value) {
      this.enc = value;
   }

   public String getC17() {
      return this.c17;
   }

   public void setC17(String value) {
      this.c17 = value;
   }

   public BigDecimal getAma() {
      return this.ama;
   }

   public void setAma(BigDecimal value) {
      this.ama = value;
   }

   public BigDecimal getFee() {
      return this.fee;
   }

   public void setFee(BigDecimal value) {
      this.fee = value;
   }

   public String getCcy() {
      return this.ccy;
   }

   public void setCcy(String value) {
      this.ccy = value;
   }

   public XMLGregorianCalendar getPod() {
      return this.pod;
   }

   public void setPod(XMLGregorianCalendar value) {
      this.pod = value;
   }

   public String getBrn() {
      return this.brn;
   }

   public void setBrn(String value) {
      this.brn = value;
   }

   public String getPbr() {
      return this.pbr;
   }

   public void setPbr(String value) {
      this.pbr = value;
   }

   public BigDecimal getPsq() {
      return this.psq;
   }

   public void setPsq(BigDecimal value) {
      this.psq = value;
   }

   public BigDecimal getHno() {
      return this.hno;
   }

   public void setHno(BigDecimal value) {
      this.hno = value;
   }

   public String getAco() {
      return this.aco;
   }

   public void setAco(String value) {
      this.aco = value;
   }

   public String getHrc() {
      return this.hrc;
   }

   public void setHrc(String value) {
      this.hrc = value;
   }

   public XMLGregorianCalendar getDts() {
      return this.dts;
   }

   public void setDts(XMLGregorianCalendar value) {
      this.dts = value;
   }

   public XMLGregorianCalendar getDte() {
      return this.dte;
   }

   public void setDte(XMLGregorianCalendar value) {
      this.dte = value;
   }

   public String getTcd() {
      return this.tcd;
   }

   public void setTcd(String value) {
      this.tcd = value;
   }

   public String getTcc() {
      return this.tcc;
   }

   public void setTcc(String value) {
      this.tcc = value;
   }

   public String getNar() {
      return this.nar;
   }

   public void setNar(String value) {
      this.nar = value;
   }

   public String getHdd() {
      return this.hdd;
   }

   public void setHdd(String value) {
      this.hdd = value;
   }

   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getRfe() {
      return this.rfe;
   }

   public void setRfe(String value) {
      this.rfe = value;
   }

   public String getDoc() {
      return this.doc;
   }

   public void setDoc(String value) {
      this.doc = value;
   }

   public String getTic() {
      return this.tic;
   }

   public void setTic(String value) {
      this.tic = value;
   }

   public String getBnd() {
      return this.bnd;
   }

   public void setBnd(String value) {
      this.bnd = value;
   }

   public String getSor() {
      return this.sor;
   }

   public void setSor(String value) {
      this.sor = value;
   }

   public XMLGregorianCalendar getDlm() {
      return this.dlm;
   }

   public void setDlm(XMLGregorianCalendar value) {
      this.dlm = value;
   }

   public String getUlm() {
      return this.ulm;
   }

   public void setUlm(String value) {
      this.ulm = value;
   }

   public XMLGregorianCalendar getTms() {
      return this.tms;
   }

   public void setTms(XMLGregorianCalendar value) {
      this.tms = value;
   }

   public String getMac() {
      return this.mac;
   }

   public void setMac(String value) {
      this.mac = value;
   }

   public XMLGregorianCalendar getClk() {
      return this.clk;
   }

   public void setClk(XMLGregorianCalendar value) {
      this.clk = value;
   }

   public XMLGregorianCalendar getTin() {
      return this.tin;
   }

   public void setTin(XMLGregorianCalendar value) {
      this.tin = value;
   }

   public String getJob() {
      return this.job;
   }

   public void setJob(String value) {
      this.job = value;
   }

   public String getErr() {
      return this.err;
   }

   public void setErr(String value) {
      this.err = value;
   }

   public String getEr0() {
      return this.er0;
   }

   public void setEr0(String value) {
      this.er0 = value;
   }

   public String getEr1() {
      return this.er1;
   }

   public void setEr1(String value) {
      this.er1 = value;
   }

   public String getEr2() {
      return this.er2;
   }

   public void setEr2(String value) {
      this.er2 = value;
   }

   public WSTypeAddInfoList getAdditionalInfo() {
      return this.additionalInfo;
   }

   public void setAdditionalInfo(WSTypeAddInfoList value) {
      this.additionalInfo = value;
   }
}
