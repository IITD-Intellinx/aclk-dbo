package ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSPaymentBindingListGetInParms",
   propOrder = {"cus", "clc", "sts", "ps", "ean"}
)
public class WSPaymentBindingListGetInParms {

   protected String cus;
   protected String clc;
   protected String sts;
   protected String ps;
   protected String ean;


   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }

   public String getSts() {
      return this.sts;
   }

   public void setSts(String value) {
      this.sts = value;
   }

   public String getPs() {
      return this.ps;
   }

   public void setPs(String value) {
      this.ps = value;
   }

   public String getEan() {
      return this.ean;
   }

   public void setEan(String value) {
      this.ean = value;
   }
}
