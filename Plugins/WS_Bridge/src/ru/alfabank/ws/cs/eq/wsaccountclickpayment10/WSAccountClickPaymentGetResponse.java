package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGetResponseType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGetResponse",
   propOrder = {"response"}
)
public class WSAccountClickPaymentGetResponse {

   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentGetResponseType response;


   public WSAccountClickPaymentGetResponseType getResponse() {
      return this.response;
   }

   public void setResponse(WSAccountClickPaymentGetResponseType value) {
      this.response = value;
   }
}
