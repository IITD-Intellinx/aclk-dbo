package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGet;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGetResponse;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBAGetResponseType;

@XmlRegistry
public class ObjectFactory {

   private static final QName _WSCustomerAccountListGBAGetResponse_QNAME = new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBAGetResponse");
   private static final QName _WSCustomerAccountListGBAGet_QNAME = new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBAGet");


   public WSCustomerAccountListGBAGetResponse createWSCustomerAccountListGBAGetResponse() {
      return new WSCustomerAccountListGBAGetResponse();
   }

   public WSCustomerAccountListGBAGetResponseType createWSCustomerAccountListGBAGetResponseType() {
      return new WSCustomerAccountListGBAGetResponseType();
   }

   public WSCustomerAccountListGBAGet createWSCustomerAccountListGBAGet() {
      return new WSCustomerAccountListGBAGet();
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerAccountListGBAGetResponse"
   )
   public JAXBElement<WSCustomerAccountListGBAGetResponse> createWSCustomerAccountListGBAGetResponse(WSCustomerAccountListGBAGetResponse value) {
      return new JAXBElement(_WSCustomerAccountListGBAGetResponse_QNAME, WSCustomerAccountListGBAGetResponse.class, (Class)null, value);
   }

   @XmlElementDecl(
      namespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru",
      name = "WSCustomerAccountListGBAGet"
   )
   public JAXBElement<WSCustomerAccountListGBAGet> createWSCustomerAccountListGBAGet(WSCustomerAccountListGBAGet value) {
      return new JAXBElement(_WSCustomerAccountListGBAGet_QNAME, WSCustomerAccountListGBAGet.class, (Class)null, value);
   }
}
