
package ru.alfabank.ws.cs.eq.wscustomerbaseinfoinoutparms12;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import ru.alfabank.ws.cs.wscommontypes10.WSTypeAddInfoList;


/**
 * <p>Java class for WSCustomerBaseInfoGetOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerBaseInfoGetOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aig" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar5" minOccurs="0"/>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="cun" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cpnc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="das" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar15" minOccurs="0"/>
 *         &lt;element name="c1r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="c2r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="c3r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="c4r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="c5r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="p1r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="p2r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="p3r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="p4r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="p5r" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ctp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cub" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cuc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cud" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cuz" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="sac" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="aco" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="crf" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="lnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="ca2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnap" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnar" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cnal" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="cod" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="dcc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="dlm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="itrt" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal117" minOccurs="0"/>
 *         &lt;element name="brnm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="crb1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="crb2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="adj" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal117" minOccurs="0"/>
 *         &lt;element name="ercp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="ercc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="drc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="grps" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cuna" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="dasa" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar15" minOccurs="0"/>
 *         &lt;element name="cunm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="cnai" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="grp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="mtb" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4" minOccurs="0"/>
 *         &lt;element name="etx" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yfon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="dfrq" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal30" minOccurs="0"/>
 *         &lt;element name="fon" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar15" minOccurs="0"/>
 *         &lt;element name="fol" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="del" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="stmp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar16" minOccurs="0"/>
 *         &lt;element name="cref" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar20" minOccurs="0"/>
 *         &lt;element name="oatp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="occl" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="hdd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="dded" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="rddh" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="ytri" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yret" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ypla" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yopi" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="ynet" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yri1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yri2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yri3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="yri4" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="pcus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="pclc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="cs" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cfrq" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="fcyc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cssa" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="pstm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="nstm" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="stn" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal70" minOccurs="0"/>
 *         &lt;element name="ocid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10" minOccurs="0"/>
 *         &lt;element name="clsf" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="clst" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate" minOccurs="0"/>
 *         &lt;element name="cltv" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="cltp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1" minOccurs="0"/>
 *         &lt;element name="drty" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3" minOccurs="0"/>
 *         &lt;element name="mrty" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="seid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6" minOccurs="0"/>
 *         &lt;element name="cst" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar2" minOccurs="0"/>
 *         &lt;element name="auno" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal40" minOccurs="0"/>
 *         &lt;element name="tfnm1" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tfnm2" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="tfnm3" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35" minOccurs="0"/>
 *         &lt;element name="additionalInfo" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeAddInfoList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerBaseInfoGetOutParms", propOrder = {
    "aig",
    "cus",
    "clc",
    "cun",
    "cpnc",
    "das",
    "c1R",
    "c2R",
    "c3R",
    "c4R",
    "c5R",
    "p1R",
    "p2R",
    "p3R",
    "p4R",
    "p5R",
    "ctp",
    "cub",
    "cuc",
    "cud",
    "cuz",
    "sac",
    "aco",
    "crf",
    "lnm",
    "ca2",
    "cnap",
    "cnar",
    "cnal",
    "cod",
    "dcc",
    "dlm",
    "itrt",
    "brnm",
    "crb1",
    "crb2",
    "adj",
    "ercp",
    "ercc",
    "drc",
    "grps",
    "cuna",
    "dasa",
    "cunm",
    "cnai",
    "grp",
    "mtb",
    "etx",
    "yfon",
    "dfrq",
    "fon",
    "fol",
    "del",
    "stmp",
    "cref",
    "oatp",
    "occl",
    "hdd",
    "dded",
    "rddh",
    "ytri",
    "yret",
    "ypla",
    "yopi",
    "ynet",
    "yri1",
    "yri2",
    "yri3",
    "yri4",
    "pcus",
    "pclc",
    "cs",
    "cfrq",
    "fcyc",
    "cssa",
    "pstm",
    "nstm",
    "stn",
    "ocid",
    "clsf",
    "clst",
    "cltv",
    "cltp",
    "drty",
    "mrty",
    "seid",
    "cst",
    "auno",
    "tfnm1",
    "tfnm2",
    "tfnm3",
    "additionalInfo"
})
public class WSCustomerBaseInfoGetOutParms {

    protected String aig;
    protected String cus;
    protected String clc;
    protected String cun;
    protected String cpnc;
    protected String das;
    @XmlElement(name = "c1r")
    protected String c1R;
    @XmlElement(name = "c2r")
    protected String c2R;
    @XmlElement(name = "c3r")
    protected String c3R;
    @XmlElement(name = "c4r")
    protected String c4R;
    @XmlElement(name = "c5r")
    protected String c5R;
    @XmlElement(name = "p1r")
    protected String p1R;
    @XmlElement(name = "p2r")
    protected String p2R;
    @XmlElement(name = "p3r")
    protected String p3R;
    @XmlElement(name = "p4r")
    protected String p4R;
    @XmlElement(name = "p5r")
    protected String p5R;
    protected String ctp;
    protected String cub;
    protected String cuc;
    protected String cud;
    protected String cuz;
    protected String sac;
    protected String aco;
    protected String crf;
    protected String lnm;
    protected String ca2;
    protected String cnap;
    protected String cnar;
    protected String cnal;
    protected XMLGregorianCalendar cod;
    protected XMLGregorianCalendar dcc;
    protected XMLGregorianCalendar dlm;
    protected BigDecimal itrt;
    protected String brnm;
    protected String crb1;
    protected String crb2;
    protected BigDecimal adj;
    protected String ercp;
    protected String ercc;
    protected String drc;
    protected String grps;
    protected String cuna;
    protected String dasa;
    protected String cunm;
    protected String cnai;
    protected String grp;
    protected String mtb;
    protected String etx;
    protected String yfon;
    protected BigDecimal dfrq;
    protected String fon;
    protected String fol;
    protected String del;
    protected String stmp;
    protected String cref;
    protected String oatp;
    protected String occl;
    protected String hdd;
    protected XMLGregorianCalendar dded;
    protected String rddh;
    protected String ytri;
    protected String yret;
    protected String ypla;
    protected String yopi;
    protected String ynet;
    protected String yri1;
    protected String yri2;
    protected String yri3;
    protected String yri4;
    protected String pcus;
    protected String pclc;
    protected String cs;
    protected String cfrq;
    protected String fcyc;
    protected String cssa;
    protected XMLGregorianCalendar pstm;
    protected XMLGregorianCalendar nstm;
    protected BigDecimal stn;
    protected String ocid;
    protected XMLGregorianCalendar clsf;
    protected XMLGregorianCalendar clst;
    protected String cltv;
    protected String cltp;
    protected String drty;
    protected String mrty;
    protected String seid;
    protected String cst;
    protected BigDecimal auno;
    protected String tfnm1;
    protected String tfnm2;
    protected String tfnm3;
    @XmlElement(required = true)
    protected WSTypeAddInfoList additionalInfo;

    /**
     * Gets the value of the aig property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAig() {
        return aig;
    }

    /**
     * Sets the value of the aig property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAig(String value) {
        this.aig = value;
    }

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the cun property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCun() {
        return cun;
    }

    /**
     * Sets the value of the cun property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCun(String value) {
        this.cun = value;
    }

    /**
     * Gets the value of the cpnc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpnc() {
        return cpnc;
    }

    /**
     * Sets the value of the cpnc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpnc(String value) {
        this.cpnc = value;
    }

    /**
     * Gets the value of the das property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDas() {
        return das;
    }

    /**
     * Sets the value of the das property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDas(String value) {
        this.das = value;
    }

    /**
     * Gets the value of the c1R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getC1R() {
        return c1R;
    }

    /**
     * Sets the value of the c1R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setC1R(String value) {
        this.c1R = value;
    }

    /**
     * Gets the value of the c2R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getC2R() {
        return c2R;
    }

    /**
     * Sets the value of the c2R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setC2R(String value) {
        this.c2R = value;
    }

    /**
     * Gets the value of the c3R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getC3R() {
        return c3R;
    }

    /**
     * Sets the value of the c3R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setC3R(String value) {
        this.c3R = value;
    }

    /**
     * Gets the value of the c4R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getC4R() {
        return c4R;
    }

    /**
     * Sets the value of the c4R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setC4R(String value) {
        this.c4R = value;
    }

    /**
     * Gets the value of the c5R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getC5R() {
        return c5R;
    }

    /**
     * Sets the value of the c5R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setC5R(String value) {
        this.c5R = value;
    }

    /**
     * Gets the value of the p1R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP1R() {
        return p1R;
    }

    /**
     * Sets the value of the p1R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP1R(String value) {
        this.p1R = value;
    }

    /**
     * Gets the value of the p2R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP2R() {
        return p2R;
    }

    /**
     * Sets the value of the p2R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP2R(String value) {
        this.p2R = value;
    }

    /**
     * Gets the value of the p3R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP3R() {
        return p3R;
    }

    /**
     * Sets the value of the p3R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP3R(String value) {
        this.p3R = value;
    }

    /**
     * Gets the value of the p4R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP4R() {
        return p4R;
    }

    /**
     * Sets the value of the p4R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP4R(String value) {
        this.p4R = value;
    }

    /**
     * Gets the value of the p5R property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getP5R() {
        return p5R;
    }

    /**
     * Sets the value of the p5R property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setP5R(String value) {
        this.p5R = value;
    }

    /**
     * Gets the value of the ctp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCtp() {
        return ctp;
    }

    /**
     * Sets the value of the ctp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCtp(String value) {
        this.ctp = value;
    }

    /**
     * Gets the value of the cub property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCub() {
        return cub;
    }

    /**
     * Sets the value of the cub property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCub(String value) {
        this.cub = value;
    }

    /**
     * Gets the value of the cuc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuc() {
        return cuc;
    }

    /**
     * Sets the value of the cuc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuc(String value) {
        this.cuc = value;
    }

    /**
     * Gets the value of the cud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCud() {
        return cud;
    }

    /**
     * Sets the value of the cud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCud(String value) {
        this.cud = value;
    }

    /**
     * Gets the value of the cuz property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuz() {
        return cuz;
    }

    /**
     * Sets the value of the cuz property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuz(String value) {
        this.cuz = value;
    }

    /**
     * Gets the value of the sac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSac() {
        return sac;
    }

    /**
     * Sets the value of the sac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSac(String value) {
        this.sac = value;
    }

    /**
     * Gets the value of the aco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAco() {
        return aco;
    }

    /**
     * Sets the value of the aco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAco(String value) {
        this.aco = value;
    }

    /**
     * Gets the value of the crf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrf() {
        return crf;
    }

    /**
     * Sets the value of the crf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrf(String value) {
        this.crf = value;
    }

    /**
     * Gets the value of the lnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLnm() {
        return lnm;
    }

    /**
     * Sets the value of the lnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLnm(String value) {
        this.lnm = value;
    }

    /**
     * Gets the value of the ca2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCa2() {
        return ca2;
    }

    /**
     * Sets the value of the ca2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCa2(String value) {
        this.ca2 = value;
    }

    /**
     * Gets the value of the cnap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnap() {
        return cnap;
    }

    /**
     * Sets the value of the cnap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnap(String value) {
        this.cnap = value;
    }

    /**
     * Gets the value of the cnar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnar() {
        return cnar;
    }

    /**
     * Sets the value of the cnar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnar(String value) {
        this.cnar = value;
    }

    /**
     * Gets the value of the cnal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnal() {
        return cnal;
    }

    /**
     * Sets the value of the cnal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnal(String value) {
        this.cnal = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCod() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCod(XMLGregorianCalendar value) {
        this.cod = value;
    }

    /**
     * Gets the value of the dcc property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDcc() {
        return dcc;
    }

    /**
     * Sets the value of the dcc property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDcc(XMLGregorianCalendar value) {
        this.dcc = value;
    }

    /**
     * Gets the value of the dlm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDlm() {
        return dlm;
    }

    /**
     * Sets the value of the dlm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDlm(XMLGregorianCalendar value) {
        this.dlm = value;
    }

    /**
     * Gets the value of the itrt property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getItrt() {
        return itrt;
    }

    /**
     * Sets the value of the itrt property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setItrt(BigDecimal value) {
        this.itrt = value;
    }

    /**
     * Gets the value of the brnm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrnm() {
        return brnm;
    }

    /**
     * Sets the value of the brnm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrnm(String value) {
        this.brnm = value;
    }

    /**
     * Gets the value of the crb1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrb1() {
        return crb1;
    }

    /**
     * Sets the value of the crb1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrb1(String value) {
        this.crb1 = value;
    }

    /**
     * Gets the value of the crb2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrb2() {
        return crb2;
    }

    /**
     * Sets the value of the crb2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrb2(String value) {
        this.crb2 = value;
    }

    /**
     * Gets the value of the adj property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAdj() {
        return adj;
    }

    /**
     * Sets the value of the adj property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAdj(BigDecimal value) {
        this.adj = value;
    }

    /**
     * Gets the value of the ercp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErcp() {
        return ercp;
    }

    /**
     * Sets the value of the ercp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErcp(String value) {
        this.ercp = value;
    }

    /**
     * Gets the value of the ercc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErcc() {
        return ercc;
    }

    /**
     * Sets the value of the ercc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErcc(String value) {
        this.ercc = value;
    }

    /**
     * Gets the value of the drc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrc() {
        return drc;
    }

    /**
     * Sets the value of the drc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrc(String value) {
        this.drc = value;
    }

    /**
     * Gets the value of the grps property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrps() {
        return grps;
    }

    /**
     * Sets the value of the grps property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrps(String value) {
        this.grps = value;
    }

    /**
     * Gets the value of the cuna property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuna() {
        return cuna;
    }

    /**
     * Sets the value of the cuna property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuna(String value) {
        this.cuna = value;
    }

    /**
     * Gets the value of the dasa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDasa() {
        return dasa;
    }

    /**
     * Sets the value of the dasa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDasa(String value) {
        this.dasa = value;
    }

    /**
     * Gets the value of the cunm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCunm() {
        return cunm;
    }

    /**
     * Sets the value of the cunm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCunm(String value) {
        this.cunm = value;
    }

    /**
     * Gets the value of the cnai property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnai() {
        return cnai;
    }

    /**
     * Sets the value of the cnai property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnai(String value) {
        this.cnai = value;
    }

    /**
     * Gets the value of the grp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrp() {
        return grp;
    }

    /**
     * Sets the value of the grp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrp(String value) {
        this.grp = value;
    }

    /**
     * Gets the value of the mtb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMtb() {
        return mtb;
    }

    /**
     * Sets the value of the mtb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMtb(String value) {
        this.mtb = value;
    }

    /**
     * Gets the value of the etx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEtx() {
        return etx;
    }

    /**
     * Sets the value of the etx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEtx(String value) {
        this.etx = value;
    }

    /**
     * Gets the value of the yfon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYfon() {
        return yfon;
    }

    /**
     * Sets the value of the yfon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYfon(String value) {
        this.yfon = value;
    }

    /**
     * Gets the value of the dfrq property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDfrq() {
        return dfrq;
    }

    /**
     * Sets the value of the dfrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDfrq(BigDecimal value) {
        this.dfrq = value;
    }

    /**
     * Gets the value of the fon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFon() {
        return fon;
    }

    /**
     * Sets the value of the fon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFon(String value) {
        this.fon = value;
    }

    /**
     * Gets the value of the fol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFol() {
        return fol;
    }

    /**
     * Sets the value of the fol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFol(String value) {
        this.fol = value;
    }

    /**
     * Gets the value of the del property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDel() {
        return del;
    }

    /**
     * Sets the value of the del property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDel(String value) {
        this.del = value;
    }

    /**
     * Gets the value of the stmp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStmp() {
        return stmp;
    }

    /**
     * Sets the value of the stmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStmp(String value) {
        this.stmp = value;
    }

    /**
     * Gets the value of the cref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCref() {
        return cref;
    }

    /**
     * Sets the value of the cref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCref(String value) {
        this.cref = value;
    }

    /**
     * Gets the value of the oatp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOatp() {
        return oatp;
    }

    /**
     * Sets the value of the oatp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOatp(String value) {
        this.oatp = value;
    }

    /**
     * Gets the value of the occl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccl() {
        return occl;
    }

    /**
     * Sets the value of the occl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccl(String value) {
        this.occl = value;
    }

    /**
     * Gets the value of the hdd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHdd() {
        return hdd;
    }

    /**
     * Sets the value of the hdd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHdd(String value) {
        this.hdd = value;
    }

    /**
     * Gets the value of the dded property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDded() {
        return dded;
    }

    /**
     * Sets the value of the dded property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDded(XMLGregorianCalendar value) {
        this.dded = value;
    }

    /**
     * Gets the value of the rddh property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRddh() {
        return rddh;
    }

    /**
     * Sets the value of the rddh property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRddh(String value) {
        this.rddh = value;
    }

    /**
     * Gets the value of the ytri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYtri() {
        return ytri;
    }

    /**
     * Sets the value of the ytri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYtri(String value) {
        this.ytri = value;
    }

    /**
     * Gets the value of the yret property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYret() {
        return yret;
    }

    /**
     * Sets the value of the yret property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYret(String value) {
        this.yret = value;
    }

    /**
     * Gets the value of the ypla property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYpla() {
        return ypla;
    }

    /**
     * Sets the value of the ypla property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYpla(String value) {
        this.ypla = value;
    }

    /**
     * Gets the value of the yopi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYopi() {
        return yopi;
    }

    /**
     * Sets the value of the yopi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYopi(String value) {
        this.yopi = value;
    }

    /**
     * Gets the value of the ynet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYnet() {
        return ynet;
    }

    /**
     * Sets the value of the ynet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYnet(String value) {
        this.ynet = value;
    }

    /**
     * Gets the value of the yri1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYri1() {
        return yri1;
    }

    /**
     * Sets the value of the yri1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYri1(String value) {
        this.yri1 = value;
    }

    /**
     * Gets the value of the yri2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYri2() {
        return yri2;
    }

    /**
     * Sets the value of the yri2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYri2(String value) {
        this.yri2 = value;
    }

    /**
     * Gets the value of the yri3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYri3() {
        return yri3;
    }

    /**
     * Sets the value of the yri3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYri3(String value) {
        this.yri3 = value;
    }

    /**
     * Gets the value of the yri4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYri4() {
        return yri4;
    }

    /**
     * Sets the value of the yri4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYri4(String value) {
        this.yri4 = value;
    }

    /**
     * Gets the value of the pcus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPcus() {
        return pcus;
    }

    /**
     * Sets the value of the pcus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPcus(String value) {
        this.pcus = value;
    }

    /**
     * Gets the value of the pclc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPclc() {
        return pclc;
    }

    /**
     * Sets the value of the pclc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPclc(String value) {
        this.pclc = value;
    }

    /**
     * Gets the value of the cs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCs() {
        return cs;
    }

    /**
     * Sets the value of the cs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCs(String value) {
        this.cs = value;
    }

    /**
     * Gets the value of the cfrq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCfrq() {
        return cfrq;
    }

    /**
     * Sets the value of the cfrq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCfrq(String value) {
        this.cfrq = value;
    }

    /**
     * Gets the value of the fcyc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFcyc() {
        return fcyc;
    }

    /**
     * Sets the value of the fcyc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFcyc(String value) {
        this.fcyc = value;
    }

    /**
     * Gets the value of the cssa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCssa() {
        return cssa;
    }

    /**
     * Sets the value of the cssa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCssa(String value) {
        this.cssa = value;
    }

    /**
     * Gets the value of the pstm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPstm() {
        return pstm;
    }

    /**
     * Sets the value of the pstm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPstm(XMLGregorianCalendar value) {
        this.pstm = value;
    }

    /**
     * Gets the value of the nstm property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNstm() {
        return nstm;
    }

    /**
     * Sets the value of the nstm property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNstm(XMLGregorianCalendar value) {
        this.nstm = value;
    }

    /**
     * Gets the value of the stn property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStn() {
        return stn;
    }

    /**
     * Sets the value of the stn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStn(BigDecimal value) {
        this.stn = value;
    }

    /**
     * Gets the value of the ocid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOcid() {
        return ocid;
    }

    /**
     * Sets the value of the ocid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOcid(String value) {
        this.ocid = value;
    }

    /**
     * Gets the value of the clsf property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClsf() {
        return clsf;
    }

    /**
     * Sets the value of the clsf property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClsf(XMLGregorianCalendar value) {
        this.clsf = value;
    }

    /**
     * Gets the value of the clst property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClst() {
        return clst;
    }

    /**
     * Sets the value of the clst property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClst(XMLGregorianCalendar value) {
        this.clst = value;
    }

    /**
     * Gets the value of the cltv property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCltv() {
        return cltv;
    }

    /**
     * Sets the value of the cltv property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCltv(String value) {
        this.cltv = value;
    }

    /**
     * Gets the value of the cltp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCltp() {
        return cltp;
    }

    /**
     * Sets the value of the cltp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCltp(String value) {
        this.cltp = value;
    }

    /**
     * Gets the value of the drty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrty() {
        return drty;
    }

    /**
     * Sets the value of the drty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrty(String value) {
        this.drty = value;
    }

    /**
     * Gets the value of the mrty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMrty() {
        return mrty;
    }

    /**
     * Sets the value of the mrty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMrty(String value) {
        this.mrty = value;
    }

    /**
     * Gets the value of the seid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeid() {
        return seid;
    }

    /**
     * Sets the value of the seid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeid(String value) {
        this.seid = value;
    }

    /**
     * Gets the value of the cst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCst() {
        return cst;
    }

    /**
     * Sets the value of the cst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCst(String value) {
        this.cst = value;
    }

    /**
     * Gets the value of the auno property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAuno() {
        return auno;
    }

    /**
     * Sets the value of the auno property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAuno(BigDecimal value) {
        this.auno = value;
    }

    /**
     * Gets the value of the tfnm1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTfnm1() {
        return tfnm1;
    }

    /**
     * Sets the value of the tfnm1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTfnm1(String value) {
        this.tfnm1 = value;
    }

    /**
     * Gets the value of the tfnm2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTfnm2() {
        return tfnm2;
    }

    /**
     * Sets the value of the tfnm2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTfnm2(String value) {
        this.tfnm2 = value;
    }

    /**
     * Gets the value of the tfnm3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTfnm3() {
        return tfnm3;
    }

    /**
     * Sets the value of the tfnm3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTfnm3(String value) {
        this.tfnm3 = value;
    }

    /**
     * Gets the value of the additionalInfo property.
     * 
     * @return
     *     possible object is
     *     {@link WSTypeAddInfoList }
     *     
     */
    public WSTypeAddInfoList getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * Sets the value of the additionalInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSTypeAddInfoList }
     *     
     */
    public void setAdditionalInfo(WSTypeAddInfoList value) {
        this.additionalInfo = value;
    }

}
