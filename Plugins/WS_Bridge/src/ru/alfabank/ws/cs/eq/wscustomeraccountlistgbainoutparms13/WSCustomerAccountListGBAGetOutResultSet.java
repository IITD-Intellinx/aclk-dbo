package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSetRow;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetOutResultSet",
   propOrder = {"resultSetRow"}
)
public class WSCustomerAccountListGBAGetOutResultSet {

   protected List<WSCustomerAccountListGBAGetOutResultSetRow> resultSetRow;


   public List<WSCustomerAccountListGBAGetOutResultSetRow> getResultSetRow() {
      if(this.resultSetRow == null) {
         this.resultSetRow = new ArrayList();
      }

      return this.resultSetRow;
   }
}
