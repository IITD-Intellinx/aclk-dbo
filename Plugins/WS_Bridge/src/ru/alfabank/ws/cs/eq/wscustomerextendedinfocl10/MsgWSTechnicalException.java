package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSTechnicalException;

@WebFault(
   name = "WSTechnicalException",
   targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru"
)
public class MsgWSTechnicalException extends Exception {

   private WSTechnicalException faultInfo;


   public MsgWSTechnicalException(String message, WSTechnicalException faultInfo) {
      super(message);
      this.faultInfo = faultInfo;
   }

   public MsgWSTechnicalException(String message, WSTechnicalException faultInfo, Throwable cause) {
      super(message, cause);
      this.faultInfo = faultInfo;
   }

   public WSTechnicalException getFaultInfo() {
      return this.faultInfo;
   }
}
