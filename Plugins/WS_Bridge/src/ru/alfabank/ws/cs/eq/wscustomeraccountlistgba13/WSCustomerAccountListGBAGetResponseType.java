package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGetResponseType",
   propOrder = {"outCommonParms", "outParms"}
)
public class WSCustomerAccountListGBAGetResponseType {

   @XmlElement(
      required = true
   )
   protected WSCommonOutParms outCommonParms;
   @XmlElement(
      required = true
   )
   protected WSCustomerAccountListGBAGetOutParms outParms;


   public WSCommonOutParms getOutCommonParms() {
      return this.outCommonParms;
   }

   public void setOutCommonParms(WSCommonOutParms value) {
      this.outCommonParms = value;
   }

   public WSCustomerAccountListGBAGetOutParms getOutParms() {
      return this.outParms;
   }

   public void setOutParms(WSCustomerAccountListGBAGetOutParms value) {
      this.outParms = value;
   }
}
