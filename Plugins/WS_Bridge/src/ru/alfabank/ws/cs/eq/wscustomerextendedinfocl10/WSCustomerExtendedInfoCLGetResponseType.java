package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLGetOutParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonOutParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLGetResponseType",
   propOrder = {"outCommonParms", "outParms"}
)
public class WSCustomerExtendedInfoCLGetResponseType {

   @XmlElement(
      required = true
   )
   protected WSCommonOutParms outCommonParms;
   @XmlElement(
      required = true
   )
   protected WSCustomerExtendedInfoCLGetOutParms outParms;


   public WSCommonOutParms getOutCommonParms() {
      return this.outCommonParms;
   }

   public void setOutCommonParms(WSCommonOutParms value) {
      this.outCommonParms = value;
   }

   public WSCustomerExtendedInfoCLGetOutParms getOutParms() {
      return this.outParms;
   }

   public void setOutParms(WSCustomerExtendedInfoCLGetOutParms value) {
      this.outParms = value;
   }
}
