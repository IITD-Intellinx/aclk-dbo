package ru.alfabank.ws.cs.eq.wspaymentbindinglist11;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.MsgWSAccessException;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.MsgWSAppException;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.MsgWSAppTechnicalException;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.MsgWSTechnicalException;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.MsgWSVerificationException;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.ObjectFactory;
import ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGetResponseType;
import ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.WSPaymentBindingListGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@WebService(
   name = "WSPaymentBindingList11PortType",
   targetNamespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru"
)
@XmlSeeAlso({ru.alfabank.ws.cs.eq.wspaymentbindinglistinoutparms11.ObjectFactory.class, ru.alfabank.ws.cs.wscommontypes10.ObjectFactory.class, ObjectFactory.class})
public interface WSPaymentBindingList11PortType {

   @WebMethod(
      operationName = "WSPaymentBindingListGet",
      action = "/CS/EQ/WSPaymentBindingList11#Get"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSPaymentBindingListGet",
      targetNamespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGet"
   )
   @ResponseWrapper(
      localName = "WSPaymentBindingListGetResponse",
      targetNamespace = "http://WSPaymentBindingList11.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wspaymentbindinglist11.WSPaymentBindingListGetResponse"
   )
   WSPaymentBindingListGetResponseType wsPaymentBindingListGet(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSPaymentBindingListGetInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;
}
