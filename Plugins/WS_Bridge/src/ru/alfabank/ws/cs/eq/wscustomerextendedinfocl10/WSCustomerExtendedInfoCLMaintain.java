package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLMaintain",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSCustomerExtendedInfoCLMaintain {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSCustomerExtendedInfoCLInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSCustomerExtendedInfoCLInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSCustomerExtendedInfoCLInParms value) {
      this.inParms = value;
   }
}
