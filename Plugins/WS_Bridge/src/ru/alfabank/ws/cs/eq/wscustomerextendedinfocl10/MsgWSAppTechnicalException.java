package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSAppTechnicalException;

@WebFault(
   name = "WSAppTechnicalException",
   targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru"
)
public class MsgWSAppTechnicalException extends Exception {

   private WSAppTechnicalException faultInfo;


   public MsgWSAppTechnicalException(String message, WSAppTechnicalException faultInfo) {
      super(message);
      this.faultInfo = faultInfo;
   }

   public MsgWSAppTechnicalException(String message, WSAppTechnicalException faultInfo, Throwable cause) {
      super(message, cause);
      this.faultInfo = faultInfo;
   }

   public WSAppTechnicalException getFaultInfo() {
      return this.faultInfo;
   }
}
