package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13.WSCustomerAccountListGBA13PortType;

@WebServiceClient(
   name = "WSCustomerAccountListGBA13Service",
   targetNamespace = "http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru",
   wsdlLocation = "file:/I:/Projects/WSCustomerAccountListGBA13/WSCustomerAccountListGBA13/EQ/WSCustomerAccountListGBA13.wsdl"
)
public class WSCustomerAccountListGBA13Service extends Service {

   private static final URL WSCUSTOMERACCOUNTLISTGBA13SERVICE_WSDL_LOCATION;
   private static final Logger logger = Logger.getLogger(WSCustomerAccountListGBA13Service.class.getName());


   static {
      URL url = null;

      try {
         URL e = WSCustomerAccountListGBA13Service.class.getResource(".");
         url = new URL(e, "file:/I:/Projects/WSCustomerAccountListGBA13/WSCustomerAccountListGBA13/EQ/WSCustomerAccountListGBA13.wsdl");
      } catch (MalformedURLException var2) {
         logger.warning("Failed to create URL for the wsdl Location: \'file:I:/Projects/WSCustomerAccountListGBA13/WSCustomerAccountListGBA13/EQ/WSCustomerAccountListGBA13.wsdl\', retrying as a local file");
         logger.warning(var2.getMessage());
      }

      WSCUSTOMERACCOUNTLISTGBA13SERVICE_WSDL_LOCATION = url;
   }

   public WSCustomerAccountListGBA13Service(URL wsdlLocation, QName serviceName) {
      super(wsdlLocation, serviceName);
   }

   public WSCustomerAccountListGBA13Service() {
      super(WSCUSTOMERACCOUNTLISTGBA13SERVICE_WSDL_LOCATION, new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Service"));
   }

   @WebEndpoint(
      name = "WSCustomerAccountListGBA13Port"
   )
   public WSCustomerAccountListGBA13PortType getWSCustomerAccountListGBA13Port() {
      return (WSCustomerAccountListGBA13PortType)super.getPort(new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Port"), WSCustomerAccountListGBA13PortType.class);
   }

   @WebEndpoint(
      name = "WSCustomerAccountListGBA13Port"
   )
   public WSCustomerAccountListGBA13PortType getWSCustomerAccountListGBA13Port(WebServiceFeature ... features) {
      return (WSCustomerAccountListGBA13PortType)super.getPort(new QName("http://WSCustomerAccountListGBA13.EQ.CS.ws.alfabank.ru", "WSCustomerAccountListGBA13Port"), WSCustomerAccountListGBA13PortType.class, features);
   }
}
