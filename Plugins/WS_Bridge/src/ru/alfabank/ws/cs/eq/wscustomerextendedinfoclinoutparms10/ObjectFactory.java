package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlRegistry;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLGetInParms;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLGetOutParms;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLInParms;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoAlphanumeric;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoAmount;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoCode;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoDate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoExchangeRate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoInterestRate;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoNumeric;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSTypeAdditionalInfoResultSet;

@XmlRegistry
public class ObjectFactory {

   public WSTypeAdditionalInfoDate createWSTypeAdditionalInfoDate() {
      return new WSTypeAdditionalInfoDate();
   }

   public WSTypeAdditionalInfoAlphanumeric createWSTypeAdditionalInfoAlphanumeric() {
      return new WSTypeAdditionalInfoAlphanumeric();
   }

   public WSTypeAdditionalInfoCode createWSTypeAdditionalInfoCode() {
      return new WSTypeAdditionalInfoCode();
   }

   public WSTypeAdditionalInfoNumeric createWSTypeAdditionalInfoNumeric() {
      return new WSTypeAdditionalInfoNumeric();
   }

   public WSTypeAdditionalInfoResultSet createWSTypeAdditionalInfoResultSet() {
      return new WSTypeAdditionalInfoResultSet();
   }

   public WSCustomerExtendedInfoCLInParms createWSCustomerExtendedInfoCLInParms() {
      return new WSCustomerExtendedInfoCLInParms();
   }

   public WSTypeAdditionalInfoExchangeRate createWSTypeAdditionalInfoExchangeRate() {
      return new WSTypeAdditionalInfoExchangeRate();
   }

   public WSCustomerExtendedInfoCLGetOutParms createWSCustomerExtendedInfoCLGetOutParms() {
      return new WSCustomerExtendedInfoCLGetOutParms();
   }

   public WSTypeAdditionalInfoAmount createWSTypeAdditionalInfoAmount() {
      return new WSTypeAdditionalInfoAmount();
   }

   public WSCustomerExtendedInfoCLGetInParms createWSCustomerExtendedInfoCLGetInParms() {
      return new WSCustomerExtendedInfoCLGetInParms();
   }

   public WSTypeAdditionalInfoInterestRate createWSTypeAdditionalInfoInterestRate() {
      return new WSTypeAdditionalInfoInterestRate();
   }
}
