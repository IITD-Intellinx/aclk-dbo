package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGet",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSAccountClickPaymentGet {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSAccountClickPaymentGetInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSAccountClickPaymentGetInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSAccountClickPaymentGetInParms value) {
      this.inParms = value;
   }
}
