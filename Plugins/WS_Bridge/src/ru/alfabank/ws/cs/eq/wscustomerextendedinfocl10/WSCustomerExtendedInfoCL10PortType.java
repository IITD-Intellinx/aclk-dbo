package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.MsgWSAccessException;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.MsgWSAppException;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.MsgWSAppTechnicalException;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.MsgWSTechnicalException;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.MsgWSVerificationException;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.ObjectFactory;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGetResponseType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponseType;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLGetInParms;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.WSCustomerExtendedInfoCLInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@WebService(
   name = "WSCustomerExtendedInfoCL10PortType",
   targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru"
)
@XmlSeeAlso({ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10.ObjectFactory.class, ru.alfabank.ws.cs.wscommontypes10.ObjectFactory.class, ObjectFactory.class})
public interface WSCustomerExtendedInfoCL10PortType {

   @WebMethod(
      operationName = "WSCustomerExtendedInfoCLGet",
      action = "/CS/EQ/WSCustomerExtendedInfoCL10#Get"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSCustomerExtendedInfoCLGet",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGet"
   )
   @ResponseWrapper(
      localName = "WSCustomerExtendedInfoCLGetResponse",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLGetResponse"
   )
   WSCustomerExtendedInfoCLGetResponseType wsCustomerExtendedInfoCLGet(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSCustomerExtendedInfoCLGetInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;

   @WebMethod(
      operationName = "WSCustomerExtendedInfoCLAdd",
      action = "/CS/EQ/WSCustomerExtendedInfoCL10#Add"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSCustomerExtendedInfoCLAdd",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLAdd"
   )
   @ResponseWrapper(
      localName = "WSCustomerExtendedInfoCLResponse",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponse"
   )
   WSCustomerExtendedInfoCLResponseType wsCustomerExtendedInfoCLAdd(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSCustomerExtendedInfoCLInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;

   @WebMethod(
      operationName = "WSCustomerExtendedInfoCLMaintain",
      action = "/CS/EQ/WSCustomerExtendedInfoCL10#Maintain"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSCustomerExtendedInfoCLMaintain",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLMaintain"
   )
   @ResponseWrapper(
      localName = "WSCustomerExtendedInfoCLResponse",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponse"
   )
   WSCustomerExtendedInfoCLResponseType wsCustomerExtendedInfoCLMaintain(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSCustomerExtendedInfoCLInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;

   @WebMethod(
      operationName = "WSCustomerExtendedInfoCLDelete",
      action = "/CS/EQ/WSCustomerExtendedInfoCL10#Delete"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSCustomerExtendedInfoCLDelete",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLDelete"
   )
   @ResponseWrapper(
      localName = "WSCustomerExtendedInfoCLResponse",
      targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCLResponse"
   )
   WSCustomerExtendedInfoCLResponseType wsCustomerExtendedInfoCLDelete(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSCustomerExtendedInfoCLInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;
}
