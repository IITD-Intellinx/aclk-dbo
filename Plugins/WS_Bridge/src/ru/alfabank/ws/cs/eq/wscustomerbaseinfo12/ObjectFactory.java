
package ru.alfabank.ws.cs.eq.wscustomerbaseinfo12;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alfabank.ws.cs.eq.wscustomerbaseinfo12 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSCustomerBaseInfoGet_QNAME = new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfoGet");
    private final static QName _WSCustomerBaseInfoGetResponse_QNAME = new QName("http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", "WSCustomerBaseInfoGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alfabank.ws.cs.eq.wscustomerbaseinfo12
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSCustomerBaseInfoGetResponseType }
     * 
     */
    public WSCustomerBaseInfoGetResponseType createWSCustomerBaseInfoGetResponseType() {
        return new WSCustomerBaseInfoGetResponseType();
    }

    /**
     * Create an instance of {@link WSCustomerBaseInfoGet }
     * 
     */
    public WSCustomerBaseInfoGet createWSCustomerBaseInfoGet() {
        return new WSCustomerBaseInfoGet();
    }

    /**
     * Create an instance of {@link WSCustomerBaseInfoGetResponse }
     * 
     */
    public WSCustomerBaseInfoGetResponse createWSCustomerBaseInfoGetResponse() {
        return new WSCustomerBaseInfoGetResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerBaseInfoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", name = "WSCustomerBaseInfoGet")
    public JAXBElement<WSCustomerBaseInfoGet> createWSCustomerBaseInfoGet(WSCustomerBaseInfoGet value) {
        return new JAXBElement<WSCustomerBaseInfoGet>(_WSCustomerBaseInfoGet_QNAME, WSCustomerBaseInfoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerBaseInfoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerBaseInfo12.EQ.CS.ws.alfabank.ru", name = "WSCustomerBaseInfoGetResponse")
    public JAXBElement<WSCustomerBaseInfoGetResponse> createWSCustomerBaseInfoGetResponse(WSCustomerBaseInfoGetResponse value) {
        return new JAXBElement<WSCustomerBaseInfoGetResponse>(_WSCustomerBaseInfoGetResponse_QNAME, WSCustomerBaseInfoGetResponse.class, null, value);
    }

}
