package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import javax.xml.bind.annotation.XmlRegistry;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentAddInParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentAddOutParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetInParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSet;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSetRow;

@XmlRegistry
public class ObjectFactory {

   public WSAccountClickPaymentGetOutResultSetRow createWSAccountClickPaymentGetOutResultSetRow() {
      return new WSAccountClickPaymentGetOutResultSetRow();
   }

   public WSAccountClickPaymentAddInParms createWSAccountClickPaymentAddInParms() {
      return new WSAccountClickPaymentAddInParms();
   }

   public WSAccountClickPaymentGetOutParms createWSAccountClickPaymentGetOutParms() {
      return new WSAccountClickPaymentGetOutParms();
   }

   public WSAccountClickPaymentAddOutParms createWSAccountClickPaymentAddOutParms() {
      return new WSAccountClickPaymentAddOutParms();
   }

   public WSAccountClickPaymentGetOutResultSet createWSAccountClickPaymentGetOutResultSet() {
      return new WSAccountClickPaymentGetOutResultSet();
   }

   public WSAccountClickPaymentGetInParms createWSAccountClickPaymentGetInParms() {
      return new WSAccountClickPaymentGetInParms();
   }
}
