package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGetOutResultSetRow",
   propOrder = {"field", "value", "description", "type", "screen", "len", "format", "visible", "require", "hint", "link", "bitmask", "valuedescr"}
)
public class WSAccountClickPaymentGetOutResultSetRow {

   protected String field;
   protected String value;
   protected String description;
   protected String type;
   protected String screen;
   protected BigDecimal len;
   protected String format;
   protected String visible;
   protected String require;
   protected String hint;
   protected String link;
   protected String bitmask;
   protected String valuedescr;


   public String getField() {
      return this.field;
   }

   public void setField(String value) {
      this.field = value;
   }

   public String getValue() {
      return this.value;
   }

   public void setValue(String value) {
      this.value = value;
   }

   public String getDescription() {
      return this.description;
   }

   public void setDescription(String value) {
      this.description = value;
   }

   public String getType() {
      return this.type;
   }

   public void setType(String value) {
      this.type = value;
   }

   public String getScreen() {
      return this.screen;
   }

   public void setScreen(String value) {
      this.screen = value;
   }

   public BigDecimal getLen() {
      return this.len;
   }

   public void setLen(BigDecimal value) {
      this.len = value;
   }

   public String getFormat() {
      return this.format;
   }

   public void setFormat(String value) {
      this.format = value;
   }

   public String getVisible() {
      return this.visible;
   }

   public void setVisible(String value) {
      this.visible = value;
   }

   public String getRequire() {
      return this.require;
   }

   public void setRequire(String value) {
      this.require = value;
   }

   public String getHint() {
      return this.hint;
   }

   public void setHint(String value) {
      this.hint = value;
   }

   public String getLink() {
      return this.link;
   }

   public void setLink(String value) {
      this.link = value;
   }

   public String getBitmask() {
      return this.bitmask;
   }

   public void setBitmask(String value) {
      this.bitmask = value;
   }

   public String getValuedescr() {
      return this.valuedescr;
   }

   public void setValuedescr(String value) {
      this.valuedescr = value;
   }
}
