package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGetInParms",
   propOrder = {"ref", "mod"}
)
public class WSAccountClickPaymentGetInParms {

   protected String ref;
   protected String mod;


   public String getRef() {
      return this.ref;
   }

   public void setRef(String value) {
      this.ref = value;
   }

   public String getMod() {
      return this.mod;
   }

   public void setMod(String value) {
      this.mod = value;
   }
}
