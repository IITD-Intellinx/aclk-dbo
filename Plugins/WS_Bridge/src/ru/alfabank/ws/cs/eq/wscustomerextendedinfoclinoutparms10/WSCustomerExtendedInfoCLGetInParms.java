package ru.alfabank.ws.cs.eq.wscustomerextendedinfoclinoutparms10;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerExtendedInfoCLGetInParms",
   propOrder = {"cus", "clc"}
)
public class WSCustomerExtendedInfoCLGetInParms {

   protected String cus;
   protected String clc;


   public String getCus() {
      return this.cus;
   }

   public void setCus(String value) {
      this.cus = value;
   }

   public String getClc() {
      return this.clc;
   }

   public void setClc(String value) {
      this.clc = value;
   }
}
