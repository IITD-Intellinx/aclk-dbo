package ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import ru.alfabank.ws.cs.eq.wscustomerextendedinfocl10.WSCustomerExtendedInfoCL10PortType;

@WebServiceClient(
   name = "WSCustomerExtendedInfoCL10Service",
   targetNamespace = "http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru",
   wsdlLocation = "file:/I:/Projects/Workspace/WS_Test/src_test/WSCustomerExtendedInfoCL10/EQ/WSCustomerExtendedInfoCL10.wsdl"
)
public class WSCustomerExtendedInfoCL10Service extends Service {

   private static final URL WSCUSTOMEREXTENDEDINFOCL10SERVICE_WSDL_LOCATION;
   private static final Logger logger = Logger.getLogger(WSCustomerExtendedInfoCL10Service.class.getName());


   static {
      URL url = null;

      try {
         URL e = WSCustomerExtendedInfoCL10Service.class.getResource(".");
         url = new URL(e, "file:/I:/Projects/Workspace/WS_Test/src_test/WSCustomerExtendedInfoCL10/EQ/WSCustomerExtendedInfoCL10.wsdl");
      } catch (MalformedURLException var2) {
         logger.warning("Failed to create URL for the wsdl Location: \'file:/I:/Projects/Workspace/WS_Test/src_test/WSCustomerExtendedInfoCL10/EQ/WSCustomerExtendedInfoCL10.wsdl\', retrying as a local file");
         logger.warning(var2.getMessage());
      }

      WSCUSTOMEREXTENDEDINFOCL10SERVICE_WSDL_LOCATION = url;
   }

   public WSCustomerExtendedInfoCL10Service(URL wsdlLocation, QName serviceName) {
      super(wsdlLocation, serviceName);
   }

   public WSCustomerExtendedInfoCL10Service() {
      super(WSCUSTOMEREXTENDEDINFOCL10SERVICE_WSDL_LOCATION, new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Service"));
   }

   @WebEndpoint(
      name = "WSCustomerExtendedInfoCL10Port"
   )
   public WSCustomerExtendedInfoCL10PortType getWSCustomerExtendedInfoCL10Port() {
      return (WSCustomerExtendedInfoCL10PortType)super.getPort(new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Port"), WSCustomerExtendedInfoCL10PortType.class);
   }

   @WebEndpoint(
      name = "WSCustomerExtendedInfoCL10Port"
   )
   public WSCustomerExtendedInfoCL10PortType getWSCustomerExtendedInfoCL10Port(WebServiceFeature ... features) {
      return (WSCustomerExtendedInfoCL10PortType)super.getPort(new QName("http://WSCustomerExtendedInfoCL10.EQ.CS.ws.alfabank.ru", "WSCustomerExtendedInfoCL10Port"), WSCustomerExtendedInfoCL10PortType.class, features);
   }
}
