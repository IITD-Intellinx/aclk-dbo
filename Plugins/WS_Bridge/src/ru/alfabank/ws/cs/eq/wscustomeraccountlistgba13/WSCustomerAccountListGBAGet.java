package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSCustomerAccountListGBAGet",
   propOrder = {"inCommonParms", "inParms"}
)
public class WSCustomerAccountListGBAGet {

   @XmlElement(
      required = true
   )
   protected WSCommonParms inCommonParms;
   @XmlElement(
      required = true
   )
   protected WSCustomerAccountListGBAGetInParms inParms;


   public WSCommonParms getInCommonParms() {
      return this.inCommonParms;
   }

   public void setInCommonParms(WSCommonParms value) {
      this.inCommonParms = value;
   }

   public WSCustomerAccountListGBAGetInParms getInParms() {
      return this.inParms;
   }

   public void setInParms(WSCustomerAccountListGBAGetInParms value) {
      this.inParms = value;
   }
}
