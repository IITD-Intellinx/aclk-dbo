package ru.alfabank.ws.cs.eq.wscustomeraccountlistgba13;

import javax.xml.ws.WebFault;
import ru.alfabank.ws.cs.wscommontypes10.WSAccessException;

@WebFault(
   name = "WSAccessException",
   targetNamespace = "http://WSCommonTypes10.CS.ws.alfabank.ru"
)
public class MsgWSAccessException extends Exception {

   private WSAccessException faultInfo;


   public MsgWSAccessException(String message, WSAccessException faultInfo) {
      super(message);
      this.faultInfo = faultInfo;
   }

   public MsgWSAccessException(String message, WSAccessException faultInfo, Throwable cause) {
      super(message, cause);
      this.faultInfo = faultInfo;
   }

   public WSAccessException getFaultInfo() {
      return this.faultInfo;
   }
}
