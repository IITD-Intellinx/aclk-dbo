package ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetOutResultSetRow;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
   name = "WSAccountClickPaymentGetOutResultSet",
   propOrder = {"resultSetRow"}
)
public class WSAccountClickPaymentGetOutResultSet {

   protected List<WSAccountClickPaymentGetOutResultSetRow> resultSetRow;


   public List<WSAccountClickPaymentGetOutResultSetRow> getResultSetRow() {
      if(this.resultSetRow == null) {
         this.resultSetRow = new ArrayList();
      }

      return this.resultSetRow;
   }
}
