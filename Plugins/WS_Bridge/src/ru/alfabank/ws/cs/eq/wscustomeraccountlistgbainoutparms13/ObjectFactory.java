package ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13;

import javax.xml.bind.annotation.XmlRegistry;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetInParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutParms;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSet;
import ru.alfabank.ws.cs.eq.wscustomeraccountlistgbainoutparms13.WSCustomerAccountListGBAGetOutResultSetRow;

@XmlRegistry
public class ObjectFactory {

   public WSCustomerAccountListGBAGetOutResultSet createWSCustomerAccountListGBAGetOutResultSet() {
      return new WSCustomerAccountListGBAGetOutResultSet();
   }

   public WSCustomerAccountListGBAGetOutParms createWSCustomerAccountListGBAGetOutParms() {
      return new WSCustomerAccountListGBAGetOutParms();
   }

   public WSCustomerAccountListGBAGetOutResultSetRow createWSCustomerAccountListGBAGetOutResultSetRow() {
      return new WSCustomerAccountListGBAGetOutResultSetRow();
   }

   public WSCustomerAccountListGBAGetInParms createWSCustomerAccountListGBAGetInParms() {
      return new WSCustomerAccountListGBAGetInParms();
   }
}
