package ru.alfabank.ws.cs.eq.wsaccountclickpayment10;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.MsgWSAccessException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.MsgWSAppException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.MsgWSAppTechnicalException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.MsgWSTechnicalException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.MsgWSVerificationException;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.ObjectFactory;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAddResponseType;
import ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGetResponseType;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentAddInParms;
import ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.WSAccountClickPaymentGetInParms;
import ru.alfabank.ws.cs.wscommontypes10.WSCommonParms;

@WebService(
   name = "WSAccountClickPayment10PortType",
   targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru"
)
@XmlSeeAlso({ObjectFactory.class, ru.alfabank.ws.cs.wscommontypes10.ObjectFactory.class, ru.alfabank.ws.cs.eq.wsaccountclickpaymentinoutparms10.ObjectFactory.class})
public interface WSAccountClickPayment10PortType {

   @WebMethod(
      operationName = "WSAccountClickPaymentGet",
      action = "/CS/EQ/WSAccountClickPayment10#Get"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSAccountClickPaymentGet",
      targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGet"
   )
   @ResponseWrapper(
      localName = "WSAccountClickPaymentGetResponse",
      targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentGetResponse"
   )
   WSAccountClickPaymentGetResponseType wsAccountClickPaymentGet(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSAccountClickPaymentGetInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;

   @WebMethod(
      operationName = "WSAccountClickPaymentAdd",
      action = "/CS/EQ/WSAccountClickPayment10#Add"
   )
   @WebResult(
      name = "response",
      targetNamespace = ""
   )
   @RequestWrapper(
      localName = "WSAccountClickPaymentAdd",
      targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAdd"
   )
   @ResponseWrapper(
      localName = "WSAccountClickPaymentAddResponse",
      targetNamespace = "http://WSAccountClickPayment10.EQ.CS.ws.alfabank.ru",
      className = "ru.alfabank.ws.cs.eq.wsaccountclickpayment10.WSAccountClickPaymentAddResponse"
   )
   WSAccountClickPaymentAddResponseType wsAccountClickPaymentAdd(
      @WebParam(
         name = "inCommonParms",
         targetNamespace = ""
      ) WSCommonParms var1, 
      @WebParam(
         name = "inParms",
         targetNamespace = ""
      ) WSAccountClickPaymentAddInParms var2) throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException;
}
