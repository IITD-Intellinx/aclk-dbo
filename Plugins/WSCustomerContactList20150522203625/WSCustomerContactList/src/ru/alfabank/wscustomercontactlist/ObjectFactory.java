
package ru.alfabank.wscustomercontactlist;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.alfabank.wscustomercontactlist package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WSCustomerContactsListGet_QNAME = new QName("http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", "WSCustomerContactsListGet");
    private final static QName _WSCustomerContactsListGetResponse_QNAME = new QName("http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", "WSCustomerContactsListGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.alfabank.wscustomercontactlist
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link WSVerificationException }
     * 
     */
    public WSVerificationException createWSVerificationException() {
        return new WSVerificationException();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetResponse }
     * 
     */
    public WSCustomerContactsListGetResponse createWSCustomerContactsListGetResponse() {
        return new WSCustomerContactsListGetResponse();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetOutResultSet }
     * 
     */
    public WSCustomerContactsListGetOutResultSet createWSCustomerContactsListGetOutResultSet() {
        return new WSCustomerContactsListGetOutResultSet();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetOutResultSetRow }
     * 
     */
    public WSCustomerContactsListGetOutResultSetRow createWSCustomerContactsListGetOutResultSetRow() {
        return new WSCustomerContactsListGetOutResultSetRow();
    }

    /**
     * Create an instance of {@link WSTechnicalException }
     * 
     */
    public WSTechnicalException createWSTechnicalException() {
        return new WSTechnicalException();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetOutResult }
     * 
     */
    public WSCustomerContactsListGetOutResult createWSCustomerContactsListGetOutResult() {
        return new WSCustomerContactsListGetOutResult();
    }

    /**
     * Create an instance of {@link WSAppException }
     * 
     */
    public WSAppException createWSAppException() {
        return new WSAppException();
    }

    /**
     * Create an instance of {@link WSCommonOutParms }
     * 
     */
    public WSCommonOutParms createWSCommonOutParms() {
        return new WSCommonOutParms();
    }

    /**
     * Create an instance of {@link WSExtention }
     * 
     */
    public WSExtention createWSExtention() {
        return new WSExtention();
    }

    /**
     * Create an instance of {@link WSAppTechnicalException }
     * 
     */
    public WSAppTechnicalException createWSAppTechnicalException() {
        return new WSAppTechnicalException();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGet }
     * 
     */
    public WSCustomerContactsListGet createWSCustomerContactsListGet() {
        return new WSCustomerContactsListGet();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetOutParms }
     * 
     */
    public WSCustomerContactsListGetOutParms createWSCustomerContactsListGetOutParms() {
        return new WSCustomerContactsListGetOutParms();
    }

    /**
     * Create an instance of {@link WSCommonParms }
     * 
     */
    public WSCommonParms createWSCommonParms() {
        return new WSCommonParms();
    }

    /**
     * Create an instance of {@link WSCustomerContactsListGetInParms }
     * 
     */
    public WSCustomerContactsListGetInParms createWSCustomerContactsListGetInParms() {
        return new WSCustomerContactsListGetInParms();
    }

    /**
     * Create an instance of {@link WSAccessException }
     * 
     */
    public WSAccessException createWSAccessException() {
        return new WSAccessException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerContactsListGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", name = "WSCustomerContactsListGet")
    public JAXBElement<WSCustomerContactsListGet> createWSCustomerContactsListGet(WSCustomerContactsListGet value) {
        return new JAXBElement<WSCustomerContactsListGet>(_WSCustomerContactsListGet_QNAME, WSCustomerContactsListGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WSCustomerContactsListGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", name = "WSCustomerContactsListGetResponse")
    public JAXBElement<WSCustomerContactsListGetResponse> createWSCustomerContactsListGetResponse(WSCustomerContactsListGetResponse value) {
        return new JAXBElement<WSCustomerContactsListGetResponse>(_WSCustomerContactsListGetResponse_QNAME, WSCustomerContactsListGetResponse.class, null, value);
    }

}
