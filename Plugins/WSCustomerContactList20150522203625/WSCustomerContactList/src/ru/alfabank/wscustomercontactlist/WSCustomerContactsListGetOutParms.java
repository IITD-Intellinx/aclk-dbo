
package ru.alfabank.wscustomercontactlist;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSCustomerContactsListGetOutParms complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerContactsListGetOutParms">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outCommonParms" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSCommonOutParms"/>
 *         &lt;element name="result" type="{http://WSCustomerContactsListInOutParms10.EQ.CS.ws.alfabank.ru}WSCustomerContactsListGetOutResult"/>
 *         &lt;element name="resultSetCount" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal50"/>
 *         &lt;element name="resultSet" type="{http://WSCustomerContactsListInOutParms10.EQ.CS.ws.alfabank.ru}WSCustomerContactsListGetOutResultSet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerContactsListGetOutParms", namespace = "http://WSCustomerContactsListInOutParms10.EQ.CS.ws.alfabank.ru", propOrder = {
    "outCommonParms",
    "result",
    "resultSetCount",
    "resultSet"
})
public class WSCustomerContactsListGetOutParms {

    @XmlElement(required = true)
    protected WSCommonOutParms outCommonParms;
    @XmlElement(required = true)
    protected WSCustomerContactsListGetOutResult result;
    @XmlElement(required = true)
    protected BigDecimal resultSetCount;
    protected WSCustomerContactsListGetOutResultSet resultSet;

    /**
     * Gets the value of the outCommonParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSCommonOutParms }
     *     
     */
    public WSCommonOutParms getOutCommonParms() {
        return outCommonParms;
    }

    /**
     * Sets the value of the outCommonParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCommonOutParms }
     *     
     */
    public void setOutCommonParms(WSCommonOutParms value) {
        this.outCommonParms = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link WSCustomerContactsListGetOutResult }
     *     
     */
    public WSCustomerContactsListGetOutResult getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCustomerContactsListGetOutResult }
     *     
     */
    public void setResult(WSCustomerContactsListGetOutResult value) {
        this.result = value;
    }

    /**
     * Gets the value of the resultSetCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getResultSetCount() {
        return resultSetCount;
    }

    /**
     * Sets the value of the resultSetCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setResultSetCount(BigDecimal value) {
        this.resultSetCount = value;
    }

    /**
     * Gets the value of the resultSet property.
     * 
     * @return
     *     possible object is
     *     {@link WSCustomerContactsListGetOutResultSet }
     *     
     */
    public WSCustomerContactsListGetOutResultSet getResultSet() {
        return resultSet;
    }

    /**
     * Sets the value of the resultSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCustomerContactsListGetOutResultSet }
     *     
     */
    public void setResultSet(WSCustomerContactsListGetOutResultSet value) {
        this.resultSet = value;
    }

}
