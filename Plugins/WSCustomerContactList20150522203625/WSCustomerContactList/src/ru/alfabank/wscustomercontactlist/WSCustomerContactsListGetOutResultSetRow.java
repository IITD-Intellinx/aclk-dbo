
package ru.alfabank.wscustomercontactlist;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for WSCustomerContactsListGetOutResultSetRow complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerContactsListGetOutResultSetRow">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cus" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar6"/>
 *         &lt;element name="clc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3"/>
 *         &lt;element name="ctyp" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3"/>
 *         &lt;element name="cval" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar256"/>
 *         &lt;element name="opr" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar10"/>
 *         &lt;element name="csts" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar3"/>
 *         &lt;element name="act" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar1"/>
 *         &lt;element name="asys" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4"/>
 *         &lt;element name="adte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate"/>
 *         &lt;element name="auid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4"/>
 *         &lt;element name="msys" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4"/>
 *         &lt;element name="grd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDecimal30"/>
 *         &lt;element name="mdte" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate"/>
 *         &lt;element name="muid" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar4"/>
 *         &lt;element name="lcmd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate"/>
 *         &lt;element name="csmd" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeDate"/>
 *         &lt;element name="tdsc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35"/>
 *         &lt;element name="csdsc" type="{http://WSCommonTypes10.CS.ws.alfabank.ru}WSTypeChar35"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerContactsListGetOutResultSetRow", namespace = "http://WSCustomerContactsListInOutParms10.EQ.CS.ws.alfabank.ru", propOrder = {
    "cus",
    "clc",
    "ctyp",
    "cval",
    "opr",
    "csts",
    "act",
    "asys",
    "adte",
    "auid",
    "msys",
    "grd",
    "mdte",
    "muid",
    "lcmd",
    "csmd",
    "tdsc",
    "csdsc"
})
public class WSCustomerContactsListGetOutResultSetRow {

    @XmlElement(required = true)
    protected String cus;
    @XmlElement(required = true)
    protected String clc;
    @XmlElement(required = true)
    protected String ctyp;
    @XmlElement(required = true)
    protected String cval;
    @XmlElement(required = true)
    protected String opr;
    @XmlElement(required = true)
    protected String csts;
    @XmlElement(required = true)
    protected String act;
    @XmlElement(required = true)
    protected String asys;
    @XmlElement(required = true)
    protected XMLGregorianCalendar adte;
    @XmlElement(required = true)
    protected String auid;
    @XmlElement(required = true)
    protected String msys;
    @XmlElement(required = true)
    protected BigDecimal grd;
    @XmlElement(required = true)
    protected XMLGregorianCalendar mdte;
    @XmlElement(required = true)
    protected String muid;
    @XmlElement(required = true)
    protected XMLGregorianCalendar lcmd;
    @XmlElement(required = true)
    protected XMLGregorianCalendar csmd;
    @XmlElement(required = true)
    protected String tdsc;
    @XmlElement(required = true)
    protected String csdsc;

    /**
     * Gets the value of the cus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCus() {
        return cus;
    }

    /**
     * Sets the value of the cus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCus(String value) {
        this.cus = value;
    }

    /**
     * Gets the value of the clc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClc() {
        return clc;
    }

    /**
     * Sets the value of the clc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClc(String value) {
        this.clc = value;
    }

    /**
     * Gets the value of the ctyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCtyp() {
        return ctyp;
    }

    /**
     * Sets the value of the ctyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCtyp(String value) {
        this.ctyp = value;
    }

    /**
     * Gets the value of the cval property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCval() {
        return cval;
    }

    /**
     * Sets the value of the cval property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCval(String value) {
        this.cval = value;
    }

    /**
     * Gets the value of the opr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpr() {
        return opr;
    }

    /**
     * Sets the value of the opr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpr(String value) {
        this.opr = value;
    }

    /**
     * Gets the value of the csts property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsts() {
        return csts;
    }

    /**
     * Sets the value of the csts property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsts(String value) {
        this.csts = value;
    }

    /**
     * Gets the value of the act property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAct() {
        return act;
    }

    /**
     * Sets the value of the act property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAct(String value) {
        this.act = value;
    }

    /**
     * Gets the value of the asys property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsys() {
        return asys;
    }

    /**
     * Sets the value of the asys property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsys(String value) {
        this.asys = value;
    }

    /**
     * Gets the value of the adte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAdte() {
        return adte;
    }

    /**
     * Sets the value of the adte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAdte(XMLGregorianCalendar value) {
        this.adte = value;
    }

    /**
     * Gets the value of the auid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuid() {
        return auid;
    }

    /**
     * Sets the value of the auid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuid(String value) {
        this.auid = value;
    }

    /**
     * Gets the value of the msys property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsys() {
        return msys;
    }

    /**
     * Sets the value of the msys property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsys(String value) {
        this.msys = value;
    }

    /**
     * Gets the value of the grd property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getGrd() {
        return grd;
    }

    /**
     * Sets the value of the grd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setGrd(BigDecimal value) {
        this.grd = value;
    }

    /**
     * Gets the value of the mdte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMdte() {
        return mdte;
    }

    /**
     * Sets the value of the mdte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMdte(XMLGregorianCalendar value) {
        this.mdte = value;
    }

    /**
     * Gets the value of the muid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMuid() {
        return muid;
    }

    /**
     * Sets the value of the muid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMuid(String value) {
        this.muid = value;
    }

    /**
     * Gets the value of the lcmd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLcmd() {
        return lcmd;
    }

    /**
     * Sets the value of the lcmd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLcmd(XMLGregorianCalendar value) {
        this.lcmd = value;
    }

    /**
     * Gets the value of the csmd property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCsmd() {
        return csmd;
    }

    /**
     * Sets the value of the csmd property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCsmd(XMLGregorianCalendar value) {
        this.csmd = value;
    }

    /**
     * Gets the value of the tdsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTdsc() {
        return tdsc;
    }

    /**
     * Sets the value of the tdsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTdsc(String value) {
        this.tdsc = value;
    }

    /**
     * Gets the value of the csdsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsdsc() {
        return csdsc;
    }

    /**
     * Sets the value of the csdsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsdsc(String value) {
        this.csdsc = value;
    }

}
