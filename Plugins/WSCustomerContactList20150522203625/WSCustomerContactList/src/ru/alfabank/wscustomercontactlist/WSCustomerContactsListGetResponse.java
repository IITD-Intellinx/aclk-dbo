
package ru.alfabank.wscustomercontactlist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSCustomerContactsListGetResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WSCustomerContactsListGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="outParms" type="{http://WSCustomerContactsListInOutParms10.EQ.CS.ws.alfabank.ru}WSCustomerContactsListGetOutParms"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WSCustomerContactsListGetResponse", namespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", propOrder = {
    "outParms"
})
public class WSCustomerContactsListGetResponse {

    @XmlElement(required = true)
    protected WSCustomerContactsListGetOutParms outParms;

    /**
     * Gets the value of the outParms property.
     * 
     * @return
     *     possible object is
     *     {@link WSCustomerContactsListGetOutParms }
     *     
     */
    public WSCustomerContactsListGetOutParms getOutParms() {
        return outParms;
    }

    /**
     * Sets the value of the outParms property.
     * 
     * @param value
     *     allowed object is
     *     {@link WSCustomerContactsListGetOutParms }
     *     
     */
    public void setOutParms(WSCustomerContactsListGetOutParms value) {
        this.outParms = value;
    }

}
