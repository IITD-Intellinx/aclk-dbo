
package ru.alfabank.wscustomercontactlist;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebService(name = "WSCustomerContactsListPortType", targetNamespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface WSCustomerContactsListPortType {


    /**
     * 
     * @param inCommonParms
     * @param inParms
     * @return
     *     returns ru.alfabank.wscustomercontactlist.WSCustomerContactsListGetOutParms
     * @throws MsgWSAppException
     * @throws MsgWSVerificationException
     * @throws MsgWSTechnicalException
     * @throws MsgWSAppTechnicalException
     * @throws MsgWSAccessException
     */
    @WebMethod(operationName = "WSCustomerContactsListGet", action = "/CS/EQ/WSCustomerContactsList10#Get")
    @WebResult(name = "outParms", targetNamespace = "")
    @RequestWrapper(localName = "WSCustomerContactsListGet", targetNamespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", className = "ru.alfabank.wscustomercontactlist.WSCustomerContactsListGet")
    @ResponseWrapper(localName = "WSCustomerContactsListGetResponse", targetNamespace = "http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", className = "ru.alfabank.wscustomercontactlist.WSCustomerContactsListGetResponse")
    public WSCustomerContactsListGetOutParms wsCustomerContactsListGet(
        @WebParam(name = "inCommonParms", targetNamespace = "")
        WSCommonParms inCommonParms,
        @WebParam(name = "inParms", targetNamespace = "")
        WSCustomerContactsListGetInParms inParms)
        throws MsgWSAccessException, MsgWSAppException, MsgWSAppTechnicalException, MsgWSTechnicalException, MsgWSVerificationException
    ;

}
