package ru.iitdgroup.wscustomercontactlist;

import ru.alfabank.wscustomercontactlist.*;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 28.01.14
 * Time: 12:32
 * To change this template use File | Settings | File Templates.
 */
public class WSCustomerContactListConnector {

    public static String[] getResponse(String userID, String branchNumber, String externalSystemCode, String externalUserCode, String clientEQID, String sys) {
        try {
            WSCommonParms inCommonParms = new WSCommonParms();
            WSCustomerContactsListGetInParms inParms = new WSCustomerContactsListGetInParms();

            inCommonParms.setUserID(userID);
            inCommonParms.setBranchNumber(branchNumber);
            inCommonParms.setExternalSystemCode(externalSystemCode);
            inCommonParms.setExternalUserCode(externalUserCode);


            inParms.setCus(clientEQID);
            inParms.setSys(sys);

            List<WSCustomerContactsListGetOutResultSetRow> wsCustomerContactsListGetOutResultSetRows = WSCustomerContactList.wsCustomerContactsListPortType.wsCustomerContactsListGet(inCommonParms, inParms).getResultSet().getRow();
            Iterator<WSCustomerContactsListGetOutResultSetRow> wsCustomerContactsListGetOutResultSetRowIterator = wsCustomerContactsListGetOutResultSetRows.iterator();
            String[] resStrings = new String[wsCustomerContactsListGetOutResultSetRows.size()];
            int i = 0;

            while (wsCustomerContactsListGetOutResultSetRowIterator.hasNext()) {
                StringBuilder stringBuilder = new StringBuilder();
                WSCustomerContactsListGetOutResultSetRow setRow = wsCustomerContactsListGetOutResultSetRowIterator.next();
                stringBuilder.append(setRow.getCus());//0
                stringBuilder.append(";");
                stringBuilder.append(setRow.getClc());//1
                stringBuilder.append(";");
                stringBuilder.append(setRow.getCtyp());//2
                stringBuilder.append(";");
                stringBuilder.append(setRow.getCval());//3
                stringBuilder.append(";");
                stringBuilder.append(setRow.getOpr());//4
                stringBuilder.append(";");
                stringBuilder.append(setRow.getCsts());//5
                stringBuilder.append(";");
                stringBuilder.append(setRow.getAct());//6
                stringBuilder.append(";");
                stringBuilder.append(setRow.getAsys());//7
                stringBuilder.append(";");
                stringBuilder.append(setRow.getAdte());//8
                stringBuilder.append(";");
                stringBuilder.append(setRow.getAuid());//9
                stringBuilder.append(";");
                stringBuilder.append(setRow.getMsys());//10
                stringBuilder.append(";");
                stringBuilder.append(setRow.getGrd());//11
                stringBuilder.append(";");
                stringBuilder.append(setRow.getMdte());//12
                stringBuilder.append(";");
                stringBuilder.append(setRow.getMuid());//13
                stringBuilder.append(";");
                stringBuilder.append(setRow.getLcmd());//14
                stringBuilder.append(";");
                stringBuilder.append(setRow.getCsmd());//15
                stringBuilder.append(";");
                stringBuilder.append(setRow.getTdsc());//16
                stringBuilder.append(";");
                stringBuilder.append(setRow.getCsdsc());//17

                resStrings[i] = stringBuilder.toString();
                i++;
            }

            return resStrings;

        } catch (MsgWSTechnicalException e) {
            String[] strings = new String[1];
            strings[0] = "MsgWSTechnicalException: " + e.getFaultInfo().getErrorString();
            return strings;
        } catch (MsgWSAppTechnicalException e) {
            String[] strings = new String[1];
            strings[0] = "MsgWSAppTechnicalException: " + e.getFaultInfo().getErrorString();
            return strings;
        } catch (MsgWSAppException e) {
            String[] strings = new String[1];
            strings[0] = "MsgWSAppException: " + e.getFaultInfo().getErrorString();
            return strings;
        } catch (MsgWSAccessException e) {
            String[] strings = new String[1];
            strings[0] = "MsgWSAccessException: " + e.getFaultInfo().getErrorString();
            return strings;
        } catch (MsgWSVerificationException e) {
            String[] strings = new String[1];
            strings[0] = "MsgWSVerificationException: " + e.getFaultInfo().getErrorString();
            return strings;

        } catch (Exception e) {
            String[] strings = new String[1];
            strings[0] = WSCustomerContactList.EXCEPTION + e.getMessage();
            return strings;
        }

    }

}
