package ru.iitdgroup.wscustomercontactlist;

import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;
import ru.alfabank.wscustomercontactlist.WSCustomerContactsListService;

import javax.xml.namespace.QName;
import java.net.MalformedURLException;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 28.01.14
 * Time: 13:44
 * To change this template use File | Settings | File Templates.
 */
public class WSChangeEndPoint extends GXFunctionExtension {

    @Override
    public void invoke(String functionName, GXArguments Arguments, GXResult Result) throws Exception {

        try {
            String URL;
            URL = Arguments.getString("URL");
            if (!URL.isEmpty()) {
                WSCustomerContactList.wsCustomerContactsListService = new WSCustomerContactsListService(new java.net.URL(URL), new QName("http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", "WSCustomerContactsListService"));
                WSCustomerContactList.wsCustomerContactsListPortType = WSCustomerContactList.wsCustomerContactsListService.getWSCustomerContactsListPort();
                WSCustomerContactList.IsBegin = true;

                Result.setString("Connected");
            } else Result.setString("URL is empty!");

        } catch (MalformedURLException e) {
            Result.setString("MalformedURLException: " + e.getMessage());
        }
    }
}
