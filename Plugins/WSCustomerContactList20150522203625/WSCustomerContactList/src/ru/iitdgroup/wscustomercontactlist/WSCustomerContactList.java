package ru.iitdgroup.wscustomercontactlist;

import com.intellinx.flow.extensions.GXArguments;
import com.intellinx.flow.extensions.GXFunctionExtension;
import com.intellinx.flow.extensions.GXResult;
import ru.alfabank.wscustomercontactlist.WSCustomerContactsListPortType;
import ru.alfabank.wscustomercontactlist.WSCustomerContactsListService;

import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import  java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 28.01.14
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */
public class WSCustomerContactList extends GXFunctionExtension {
    public static WSCustomerContactsListPortType wsCustomerContactsListPortType;
    public static WSCustomerContactsListService wsCustomerContactsListService;
    public final static String EXCEPTION = "Exception! ";
    public static volatile boolean IsBegin = false;

    @Override
    public void invoke(String functionName, GXArguments Arguments, GXResult Result) throws Exception {
        String userID, branchNumber,
                externalSystemCode, externalUserCode, sys,
                clientEQID, URL;

        try{
            externalSystemCode = Arguments.getString("externalSystemCode");
            externalUserCode = Arguments.getString("externalUserCode");
            userID = Arguments.getString("userID");
            branchNumber = Arguments.getString("branchNumber");
            sys = Arguments.getString("sys");
            clientEQID = Arguments.getString("clientEQID");
            URL = Arguments.getString("URL");

            if(!IsBegin) {
                WSCustomerContactList.wsCustomerContactsListService = new WSCustomerContactsListService(new URL(URL), new QName("http://WSCustomerContactsList10.EQ.CS.ws.alfabank.ru", "WSCustomerContactsListService"));
                WSCustomerContactList.wsCustomerContactsListPortType = WSCustomerContactList.wsCustomerContactsListService.getWSCustomerContactsListPort();
                IsBegin = true;
            }

            String[] resultStrings = WSCustomerContactListConnector.getResponse(userID,branchNumber,externalSystemCode,externalUserCode,clientEQID,sys);
            Result.setArray(resultStrings);

        }catch (MalformedURLException ex){
            String[] strings = new String[1];
            strings[0] = "MalformedURLException: " + ex.getLocalizedMessage();
            Result.setArray(strings);
        }
    }
}
